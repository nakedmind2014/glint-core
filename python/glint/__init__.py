# -*- coding: utf-8 -*-
"""
Glint is a component-based framework for building/writing tools.

    :license: BSD, see LICENSE for more details.
"""
import os
import sys
import logging

########################## GLINT CORE PUBLIC API ###############################
# These are "framework-level" functionality available in glint's core.
from .site import get_glint_root, get_plugin_root, get_tool_root, get_glint_home

from .util import (
    get_current_user, get_hostname, get_env, import_file, register_path
)
from .logging_setup import configure_logger

from .hook import load_all_hooks, run_hook
from .cache import (
    initialize_cache_region, update_cache, get_cache_data, purge_cache_data
)
from .settings import get_setting

from .component import get_main, run, register_component_paths
from .component.tool import ToolBase, load_tool, reload_tool
from .component.plugin import PluginBase, load_plugin, reload_plugin
from .component.engine import EngineBase, load_engine, get_current_engine
from .component.launcher import AppLauncher
from .component.reloader import GlintBase

from .api.commands import register_command
from .api.remote import process_rpc_request, rpc_endpoint, register_rpc_object
from .api.decorator import use_rpc
from .api.errors import GlintError, GlintRPCError, register_exception

from blinker import signal


def register_package(name, package):
    sys.modules['glint.packages.'+name] = package
    setattr(sys.modules['glint.packages'], name, package)


########################## LOGGING DEFAULT #####################################
# Start with a do-nothing logging handler to avoid "No handler found" warnings.
# After loading the core settings, glint logging will be configured. This step
# can be disabled/bypassed if there is a need to set up different logging
# configurations.
try:  # python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass
logger = logging.getLogger(__name__)
logger.addHandler(NullHandler())


########################## INITIALIZE ##########################################
def _initialize():
    # Mainly used during unittests to remove import side effects and
    # facilitate mocking of functions called during initialization.
    if os.environ.get('GLINT_DISABLE_INIT'):
        return

    # Make sure GLINT_ROOT environment variable exists.
    glint_root = os.environ.get('GLINT_ROOT')
    if not glint_root:
        os.environ['GLINT_ROOT'] = get_glint_root()

    # Make sure GLINT_HOME environment variable and home dir exists.
    glint_home = get_glint_home()
    if not glint_home.exists():
        glint_home.mkdir(parents=True)

    # Load core manifest.
    from .component import get_component_manager
    core_meta = get_component_manager().load_core_manifest()

    # Load core settings. Check first if settings file is present.
    from .settings import load_settings, check_settings_file
    check_settings_file()
    load_settings('core', core_meta['settings'])

    # Register the core hooks defined in the core manifest.
    import glint.constants as constants
    from .hook import register_hooks
    from .site import get_core_root
    register_hooks(core_meta, get_core_root()/constants.COMPONENT_HOOK_FOLDER)

    # Configure logging system here since core settings has been loaded.
    from .logging_setup import configure_main_loggers
    configure_main_loggers()

    # Make sure global temp folder exists.
    temp_dir = get_setting('global_temp_dir')
    if not temp_dir.exists():
        temp_dir.mkdir(parents=True)

    # Register default handler for getting the current glint user. This is
    # intended to be overridden by engine components if they have a different
    # way of determining the current user.
    register_command('get_current_user', get_current_user)

_initialize()


########################## NAMESPACES ##########################################
# Commands provide a lot of the functionality available within the glint
# framework. Local commands can be accessed via the glint.commands namespace
# while remote commands are accessible via glint.remote.
from .api.commands import commands
from .api.remote import remote
sys.modules['glint.commands'] = commands
sys.modules['glint.remote'] = remote

# Enable custom exceptions defined by each component to be accessible by
# other components via the glint.errors namespace.
from .api.errors import errors
sys.modules['glint.errors'] = errors

# Provide components a way to expose any third-party modules and libraries,
# that it depends on and is bundled with it, so that it can be used by other
# components without adding its internal paths to sys.path. The API command
# to use is register_package. These exposed modules can be accessed via the
# glint.packages namespace.
from types import ModuleType
class modules(ModuleType):
    pass
sys.modules['glint.modules'] = modules
sys.modules['glint'].modules = modules
sys.modules['glint.packages'] = modules
sys.modules['glint'].packages = modules


########################## CLEANUP #############################################
# Delete functions that should not be accessible outside the core.
del _initialize
