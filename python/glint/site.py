# -*- coding: utf-8 -*-
"""
    glint.site
    ~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import os
import logging
import platform
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from .api.errors import GlintError


# Default paths to search for glint installation.
__default_install_paths = {
    'Windows': ['C:/glint'],
    'Linux': ['/opt/glint', '/usr/local/glint']
}

# caches
__glint_home = None
__glint_root = None
__core_root = None
__plugin_root = None
__tool_root = None

log = logging.getLogger('glint.core.site')


# ------------------------------------------------------------------------------
#   Public functions for this module.
# ------------------------------------------------------------------------------

def get_glint_home():
    """Return the directory where user-specific glint data are saved.
    Also initializes the GLINT_HOME environment variable if not set.
    """
    global __glint_home
    if not __glint_home:
        glint_home = os.environ.get('GLINT_HOME')
        if glint_home is None:
            os_name = platform.system()
            if os_name == 'Windows':
                glint_home = os.path.join(os.environ.get('APPDATA'), 'glint')
            elif os_name == 'Linux':
                glint_home = os.path.join(os.environ.get('HOME'), '.glint')
            else:
                raise GlintError("Unsupported operating system platform")

            log.debug("GLINT_HOME set to %s", glint_home)
            os.environ['GLINT_HOME'] = glint_home

        __glint_home = Path(glint_home)

    return __glint_home


def get_glint_root():
    """Return the root of the glint installation."""
    global __glint_root
    global __core_root
    # Check cache first.
    if __glint_root:
        return __glint_root

    glint_root = os.environ.get('GLINT_ROOT')
    if glint_root is None or not os.path.exists(glint_root):
        if glint_root:
            log_msg = "%s does not exist. " % glint_root
            glint_root = None
        else:
            log_msg = "GLINT_ROOT not set. "
        log_msg += "Checking default locations for glint installation."
        log.debug(log_msg)

        os_name = platform.system()
        if os_name not in __default_install_paths:
            raise GlintError("Unsupported operating system platform")

        for fp in __default_install_paths[os_name]:
            if os.path.exists(fp):
                glint_root = fp
                break

    # Check if glint_root points to a valid installation
    # by checking for core folder.
    if glint_root is not None:
        glint_root = Path(glint_root)
        core_dir = glint_root/'core'
        if core_dir.exists():
            __glint_root = glint_root
            __core_root = core_dir
            log.debug("Found a glint installation here: %s", glint_root)
            return glint_root

    raise GlintError("Unable to determine glint's install location. If "
                     "glint is installed in this machine, set the environ"
                     "ment variable GLINT_ROOT to point to this location.")


def get_core_root():
    """Return the location of glint's core folder."""
    global __core_root
    if not __core_root:
        __core_root = get_glint_root()/'core'
    return __core_root


def get_plugin_root():
    """Return the location where plugins will be loaded from."""
    global __plugin_root
    if not __plugin_root:
        __plugin_root = Path(os.environ.get('GLINT_PLUGIN_ROOT') or
                             get_glint_root()/'plugins')
    return __plugin_root


def get_tool_root():
    """Return the location where an engine will search for tools to load."""
    global __tool_root
    if not __tool_root:
        __tool_root = Path(os.environ.get('GLINT_TOOL_ROOT') or
                           get_glint_root()/'tools')
    return __tool_root
