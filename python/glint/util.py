from builtins import str
import os
import re
import sys
import imp
import uuid
import socket
import logging
import getpass
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path


log = logging.getLogger('glint.core.util')


def get_env(var_name):
    """Return the value of an environment variable with embedded variables
    fully expanded and optionally returned as a Path object.
    """
    value = os.environ.get(var_name)
    if value:
        value = expand_vars(value)
        if 'GLINT_ROOT' == var_name or '_PATH' in var_name or '_DIR' in var_name:
            return Path(value)
        else:
            return value


def get_current_user():
    """This function checks the environment variables LOGNAME, USER, LNAME and
    USERNAME, in order, and returns the value of the first one which is set
    to a non-empty string. If none are set, the login name from the
    password database is returned on systems which support the pwd module;
    otherwise, returns an empty string.
    """
    try:
        # remove underscores
        return getpass.getuser().replace('_', '')
    except getpass.GetPassWarning:
        pass
    return ''


def get_hostname():
    return socket.gethostname()


def import_file(script_path, do_not_cache=False):
    """Import a file with full path specification.

    Allows one to import from anywhere, something __import__ does not do.
    """
    fh, fp, desc = imp.find_module(script_path.stem, [str(script_path.parent)])
    # Make sure module is loaded using a unique name to prevent
    # name collision with an existing loaded module. Useful when
    # loading files which serve as entry points for a plugin or
    # a tool. These files usually are named the same.
    uid = uuid.uuid4().hex
    mod = imp.load_module(uid, fh, fp, desc)

    # A uid is not very descriptive so change the module's name
    # to the file's name.
    mod.__name__ = script_path.stem

    if do_not_cache:
        del sys.modules[uid]
    return mod


def register_path(fp):
    """Add the given path as the first item in python's module search path.

    Based on blurdev.settings.registerPath.
    """
    fp_str = str(fp)
    if fp and fp_str != '.' and fp.exists() and fp_str not in sys.path:
        sys.path.insert(0, fp_str)
        log.debug("Added to python path: %s", fp)
        return True
    return False


def expand_vars(text, other_sources=None, cache=None):
    """Recursively expand the text variables.

    Based on blurdev.osystem.expandvars. Better than os.path.expandvars
    method which only works at one level.

    :param text: Text string to expand.
    :param other_sources (list): A list of dictionaries to be searched first
        for variable values before falling back to the system environment.
        The default is to only search the system environment.
    :param cache (dict): Used internally during recursion to prevent infinite loop.
    :return: Text with variables fully expanded.
    """
    # make sure we have data
    if not text:
        return ''

    # check for circular dependencies
    if cache is None:
        cache = {}

    # if given, search dictionary first before the system environment
    value_sources = [os.environ]
    if other_sources is not None:
        value_sources += other_sources

    # return the cleaned variable
    output = text
    keys = re.findall(r'\$(\w+)|\${(\w+)\}|\%(\w+)\%', text)

    for first, second, third in keys:
        repl = ''
        key = ''
        if first:
            repl = '$%s' % first
            key = first
        elif second:
            repl = '${%s}' % second
            key = second
        elif third:
            repl = '%%%s%%' % third
            key = third
        else:
            continue

        for source in value_sources:
            value = source.get(key)
            if value is not None:
                break

        if value:
            if key not in cache:
                cache[key] = value
                value = expand_vars(value, other_sources, cache)
            else:
                log.warning("%s environ variable contains a circular dependency", key)
                value = cache[key]
        else:
            value = repl
        output = output.replace(repl, value)

    return output


underscore_prefix_re = re.compile('(_+)(.*?)(_+)')
first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')


def camelcase_to_underscore(name):
    s1 = first_cap_re.sub(r'\1_\2', name)
    s1 = all_cap_re.sub(r'\1_\2', s1).lower()

    # This removes the extra underscore added when name is prefixed with
    # underscore/s or when name is underscore-separated.
    s1 = s1.replace('__', '_').strip('_')
    return s1


def whitespace_to_camelcase(text):
    """Convert whitespace-separated text to camelCase and remove invalid characters."""
    text = str(text)
    split = text.replace('_', ' ').split()
    if len(split) > 1:
        split = map(lambda e: e[:1].upper() + e[1:], split)
        text = ''.join(split)
    text = ''.join( x for x in text if x.isalnum() or x == '-' )
    if text.strip():
        text = text[0].lower() + text[1:]
    return text
