# -*- coding: utf-8 -*-
"""
    glint.api.decorator
    ~~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import logging
import functools

from .remote import rpc_stub, rpc_endpoint, rpc_registry


log = logging.getLogger('glint.core.api.decorator')


def use_rpc(decorated=None, **kwargs):

    def decorating_function(decorated):
        rpc_registry[decorated.__name__] = {
            'server': server,
            'command': decorated
        }

        @functools.wraps(decorated)
        def wrapper(*args, **kwargs):
            # We are running in the server so just invoke the wrapped
            # function and return the result.
            if rpc_endpoint():
                return decorated(*args, **kwargs)

            # We are not running in the server so run the command in the
            # server via glint's RPC mechanism.
            else:
                rpc_stub.__name__ = decorated.__name__
                return rpc_stub(*args, **kwargs)

        wrapper.__wrapped__ = decorated
        return wrapper

    # Using the **kwargs notation instead of explicitly defining the valid
    # parameters in the function definition prevents any arguments, positional
    # or otherwise, from being recognize as valid arguments. Currently accepts
    # only one keyword argument.
    if kwargs:
        server = kwargs.pop('server', None)
        if kwargs:
            raise ValueError("Invalid decorator keyword arguments given. The valid "
                             "keyword argument is server only.")
    else:
        server = None

    if server:
        # The decorator is called with arguments. Register the given
        # arguments so the rpc functions can access them when needed.
        # print "Decorator called with valid keyword arguments."
        return decorating_function

    elif decorated is None:
        # The decorator is used as decorator factory with no arguments.
        # Like this: @use_rpc()
        # print "Decorator called as factory with no arguments."
        return decorating_function

    else:
        # print "Used as simple decorator.", serializer, deserializer, decorated
        return decorating_function(decorated)
