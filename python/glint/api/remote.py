# -*- coding: utf-8 -*-
"""
    glint.api.remote
    ~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import re
import os
import sys
import json
import logging
import datetime
import requests
import importlib
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from .errors import GlintError, GlintRPCError
from ..settings import get_setting


log = logging.getLogger('glint.core.api.remote')
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
rpc_url = 'http://%s/runcmd'
rpc_registry = {}
rpc_object_registry = {}


class _Remote(object):
    def __getattr__(self, command_name):
        rpc_stub.__name__ = command_name
        return rpc_stub


remote = _Remote()
remote.__dict__.update({
    # This prevents the error in cherrypy's module re-loading mechanism.
    '__file__': __file__,
    '__doc__': __doc__
})


def _get_exception_name(e):
    exception_type = str(type(e))
    m = re.match("<class '(.*)'>", exception_type)
    if not m:
        m = re.match("<type '(.*)'>", exception_type)
    if not m:
        # Unable to determine exception type. Just return generic.
        return 'exceptions.Exception'
    return m.group(1)


def _get_object_class(object_type):
    parts = object_type.rsplit('.', 1)
    parts_num = len(parts)
    if parts_num == 2:
        module_name, class_name = object_type.rsplit('.', 1)
        mod = importlib.import_module(module_name)
        return getattr(mod, class_name)

    elif parts_num == 1:
        # Python 3 builtin exceptions no longer is accessible via
        # the exceptions namespace (e.g. exceptions.KeyError)
        import builtins
        return getattr(builtins, parts[0])


def _communicate(data, rpc_server):
    log.debug("Executing remote command %s", data['command'])
    url = rpc_url % rpc_server
    response = requests.post(url, data=serialize(data), headers=headers)
    if response.status_code == 200:
        return response.json(object_hook=_object_hook)
    elif response.status_code in (500, 503):
        errmsg = ("RPC service unavailable. Unable to connect to %s." % rpc_server)
        data = {'error': {'msg': errmsg, 'exc': 'glint.GlintRPCError'}}
        return data
    else:
        data = {'error': {'msg': response.text, 'exc': 'glint.GlintRPCError'}}
        return data


def _object_hook(json_dict):
    """ Code adapted from this gist:
        https://gist.github.com/abhinav-upadhyay/5300137
    """
    data = json_dict.get('__data__')
    data_type = json_dict.get('__type__')
    if not data and not data_type:
        return json_dict

    if data_type == 'datetime':
        try:
            v = datetime.datetime.strptime(data, '%Y-%m-%dT%H:%M:%S.%f')
        except ValueError:
            v = datetime.datetime.strptime(data, '%Y-%m-%dT%H:%M:%S')
        return v
    elif data_type == 'date':
        y, m, d = data.split('-')
        return datetime.date(int(y), int(m), int(d))
    elif data_type == 'path':
        return Path(data)
    elif data_type in rpc_object_registry:
        deserializer = rpc_object_registry[data_type][-1]
        if deserializer:
            return deserializer(data)

    return json_dict


def _default_json_encoder(o):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(o, datetime.datetime):
        return {'__type__': 'datetime', '__data__': o.isoformat()}
    elif isinstance(o, datetime.date):
        return {'__type__': 'date', '__data__': o.isoformat()}
    elif isinstance(o, Path):
        return {'__type__': 'path', '__data__': str(o)}

    for n, (cls, serializer, _) in rpc_object_registry.items():
        if isinstance(o, cls):
            return {'__type__': n, '__data__': (serializer(o) if serializer else o)}

    raise TypeError(repr(o) + " is not JSON serializable")


def serialize(data, format='json'):
    return json.dumps(data, separators=(',', ':'), default=_default_json_encoder).encode('utf-8')


def deserialize(data, format='json'):
    return json.loads(data, object_hook=_object_hook)


def register_rpc_object(class_name, class_obj, serializer=None, deserializer=None):
    rpc_object_registry[class_name] = class_obj, serializer, deserializer


def rpc_stub(*args, **kwargs):
    """
    TODO:
    1. Do something when rpc result indicates error condition but no
       exception raised.
    """
    command_name = rpc_stub.__name__
    rpc_info = rpc_registry.get(command_name, {})

    # When rpc_stub is called via glint.remote.command_name, command_name has no
    # entry in the rpc_registry. So the rpc_server used is the default_rpc_server.
    setting_key = rpc_info.get('server') or 'default_rpc_server'
    server = get_setting(setting_key)

    data = {'command': command_name, 'args': args, 'kwargs': kwargs}
    data = _communicate(data, server)

    # If an exception is generated in the server, generate the exception in
    # the client.
    if 'error' in data and data['error']:
        if 'exc' in data['error']:
            exception_class = _get_object_class(data['error']['exc'])
            try:
                raise exception_class(data['error']['msg'])
            except TypeError:
                raise GlintRPCError(
                    "TypeError exception generated when trying to raise "
                    "the exception '%s' generated by remote command '%s'."
                    % (exception_class, command_name)
                )
        else:
            # what to do here?
            pass

    return data['result']


def process_rpc_request(rpc_data):
    """
    Args:
        rpc_data (dict): ...

    Returns:
        ...
    """
    output_data = {}

    # Remote commands can be invoked either via the use_rpc decorator or
    # glint.remote.
    command_name = rpc_data.pop('command')
    try:
        cmd_obj = rpc_registry[command_name]['command']
    except KeyError:
        # The command is called via glint.remote
        commands = sys.modules['glint.commands']
        try:
            cmd_obj = getattr(commands, command_name)
        except GlintError as e:
            log.exception("Command not registered.")
            output_data['error'] = {'msg': str(e), 'exc': 'glint.GlintError'}
            return serialize(output_data)

    # Invoke the command. Any exception raised by the command is
    # captured and sent back to the caller.
    try:
        ret_val = cmd_obj(*rpc_data.get('args', []), **rpc_data.get('kwargs', {}))
    except Exception as e:
        log.exception("Exception raised by command.")
        output_data['error'] = {
            'msg': str(e), 'exc': _get_exception_name(e)
        }
    else:
        output_data['result'] = ret_val

    try:
        return serialize(output_data)
    except TypeError as e:
        log.exception('JSON Decoding Error')
        output_data = {'error': {'msg': str(e), 'exc': 'exceptions.TypeError'}}
        return serialize(output_data)


def rpc_endpoint():
    return 'GLINT_RPC_ENDPOINT' in os.environ
