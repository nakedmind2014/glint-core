# -*- coding: utf-8 -*-
"""
    glint.api.errors
    ~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
from types import ModuleType


class GlintError(Exception):
    """Exception that indicates a general glint-related error has occurred."""
    pass


class GlintRPCError(GlintError):
    """Exception that indicates an rpc-related error has occurred."""
    pass


_error_registry = {'GlintError': GlintError, 'GlintRPCError': GlintRPCError}


class _Errors(ModuleType):
    # Due to this class masquerading as a module, when these module dunder
    # methods are accessed via glint.errors namespace, they are treated by
    # this class as glint exception objects, thereby raising an exception
    # due to not being registered. We add these as class attributes so the
    # normal glint error mechanism is bypassed.
    try:
        __loader__ = __loader__     # present in py3 only
    except NameError:
        pass
    __file__ = __file__
    __path__ = __file__

    def __getattr__(self, exc_name):
        if exc_name not in _error_registry:
            raise GlintError(
                "Custom exception with name '%s' is not registered." % exc_name)

        return _error_registry[exc_name]


errors = _Errors('errors')


def register_exception(exception_object):
    error_name = exception_object.__name__
    if error_name in ('GlintError', 'GlintRPCError'):
        raise GlintError(
            f"Cannot override glint core exception '%s'." % error_name)
    elif error_name in _error_registry:
        raise GlintError(
            "Custom exception with name '%s' is already registered." % error_name)

    _error_registry[error_name] = exception_object
    return True
