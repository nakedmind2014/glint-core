# -*- coding: utf-8 -*-
"""
    glint.api.commands
    ~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import logging
from types import ModuleType

from .errors import GlintError


_command_registry = {}
log = logging.getLogger('glint.core.api.commands')


class _Commands(ModuleType):
    # Due to this class masquerading as a module, when these module dunder
    # methods are accessed via glint.commands namespace, they are treated by
    # this class as glint commands, thereby raising an exception due to not
    # being registered. We add these as class attributes so the normal glint
    # commands mechanism is bypassed.
    try:
        __loader__ = __loader__     # present in py3 only
    except NameError:
        pass
    __file__ = __file__
    __path__ = __file__

    def __getattr__(self, command_name):
        if command_name not in _command_registry:
            raise GlintError("Command name %s is not registered."
                             % command_name)

        handler = _command_registry.get(command_name)
        if handler is None:
            raise GlintError("Command name %s does not have a registered "
                             "handler." % command_name)

        return handler


commands = _Commands('gcmds')


def register_command(command_name, handler=None, force=False):
    if force and command_name in _command_registry:
        log.debug("Register new handler for command %s.", command_name)

    elif not force and command_name in _command_registry:
        raise GlintError("Command name %s is already registered."
                         % command_name)

    _command_registry[command_name] = handler
    return True


def get_command_handler(command_name):
    if command_name not in _command_registry:
        raise GlintError("Command name %s is not registered."
                         % command_name)
    return _command_registry[command_name]
