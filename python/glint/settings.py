# -*- coding: utf-8 -*-
"""
    glint.settings
    ~~~~~~~~~~~~~~

    This module contains the glint settings infrastructure used to customize
    the behavior of the core and the components.

    The settings recognized and used by the core or the components must first
    be defined in the manifest. The definition for a setting must include the
    following info:
        name
            - use as the key in the key-value pairs in the settings dict
        type
            - defines the setting's semantic
            - based on python's data types plus additional types based on
              python objects (e.g. path)
            - can be int, bool, str, list, path
            - optional; if not specified, str is assumed
        default_value
            - used when preference is not specified in settings file
            - optional; if not specified, takes a value based on type
                int: 0, str: "", bool: false, list: [], path: ""
        description
            - explanation on what the setting is for
            - optional; if not specified, defaults to empty string
        label
            - for use in the display interface
            - optional; if not specified, uses name
        private
            - a true/false flag to indicate where setting is going to be stored:
                global dictionary or component's local dictionary
            - optional; if not specified, assumes that setting is global

    Values for settings can contain references to environment variables or
    other settings. References are specified by:
        - prefixing with dollar sign ($name),
        - enclosing in percent signs (%name%),
        - enclosing in dollar and braces (${name})

    Each component including the core can specify different values for the
    defined settings in a settings file (settings.ini) located in the
    component's root. The file format used is the standard Windows INI format.
    Each OS has a separate section and a setting can have different values for
    each OS. Values that are not OS-dependent can be specified in a Common
    section. This means the same settings file can be used in windows or linux,
    for example.

    A global settings dictionary, accessible to all the components, stores the
    core settings and any component settings that are global. Each component
    also maintains a separate local dictionary for its private settings. This
    setup allows for a global setting to be easily applied to all parts of the
    framework by saving in the core settings file, but at the same time have
    the flexibility to override it on the component level. For example, a common
    value for log level for all components can be set in the core settings file.
    This can then be overridden in a component's settings file if a different
    log level is needed for that particular component.

    :license: BSD, see LICENSE for more details.
"""
from builtins import str
import os
import logging
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from configobj import ConfigObj

from .site import get_glint_root
from .util import expand_vars
from .constants import COMPONENT_SETTINGS


__global_settings = {}
# Store setting keys of type path for converting path-related
# settings as Path instances.
__path_keys = []
# Store path to settings file if it exists
__settings_file = None
log = logging.getLogger('glint.core.settings')


def check_settings_file():
    global __settings_file
    fp = Path(os.environ.get('GLINT_SETTINGS_PATH') or
              get_glint_root()/COMPONENT_SETTINGS)
    __settings_file = (fp if fp.exists() else None)


def __register_setting(section, key, **kwargs):
    # Load settings to global dictionary if not included in private_keys.
    settings_def = kwargs['settings_def']
    settings_dict = kwargs['settings_dict']
    private_keys = kwargs['private_keys']
    if key not in private_keys:
        settings_dict = __global_settings

    # Ignore file-specified setting that is not previously defined.
    if key not in settings_dict:
        log.debug("No definition found for setting key '%s'", key)
        return

    value_type = settings_def[key]['type']
    if value_type == 'bool':
        settings_dict[key] = section.as_bool(key)

    elif value_type == 'int':
        settings_dict[key] = section.as_int(key)

    elif value_type == 'float':
        settings_dict[key] = section.as_float(key)

    elif value_type == 'list':
        settings_dict[key] = [v.strip() for v in section[key].split(',') if v]

    elif value_type in ('path', 'str'):
        # Store path values as strings and convert later to Path object when
        # retrieving setting value. Explicitly cast to futurize.builtins.str
        # for py2 & py3 compatibility.
        settings_dict[key] = str(section[key])

    log.debug("Loaded setting '%s' with value %s", key, settings_dict[key])


# ------------------------------------------------------------------------------
#   core functions for internal use
# ------------------------------------------------------------------------------

def load_settings(comp_name, settings_def, settings_dict=None):
    """Load the settings info in the given INI file into a settings dictionary.
    Info on settings to be loaded is retrieved from the settings definition.

    :param comp_name: Name of component whose settings will be loaded.
    :param settings_def (dict): Contains the definition for the settings to
        be loaded.
    :param settings_dict (dict,optional): If given, load the settings into
        the component dictionary. Otherwise, load the settings into the global
        core dictionary.
    """
    private_keys = []
    for k, v in settings_def.items():
        value_type = v.get('type')
        if not value_type:
            # Default to str type if not specified. Also, update definition.
            value_type = settings_def[k]['type'] = 'str'

        if value_type == 'str':
            default_value = v.get('default_value', '')
        elif value_type == 'path':
            __path_keys.append(k)
            default_value = v.get('default_value', '')
        elif value_type == 'int':
            default_value = v.get('default_value', 0)
        elif value_type == 'float':
            default_value = v.get('default_value', 0.0)
        elif value_type == 'list':
            default_value = v.get('default_value', [])
        elif value_type == 'bool':
            default_value = v.get('default_value', False)

        if v.get('private', False):
            private_keys.append(k)
            if settings_dict is not None:
                settings_dict[k] = default_value
        else:
            __global_settings[k] = default_value

    if __settings_file:
        # Override default values with the values specified in settings file.
        # Keys that are not defined are ignored.
        kwargs = {'settings_def': settings_def,
                  'settings_dict': settings_dict,
                  'private_keys': private_keys}

        config = ConfigObj(str(__settings_file), list_values=False)
        data = config.get(comp_name)
        if data:
            log.debug("Loading %s settings from %s", comp_name, __settings_file)
            data.walk(__register_setting, **kwargs)


def get_setting(key, settings_dict=None):
    """Get the value from either the given settings dictionary or the
    global settings dictionary.

    Resolve any references to environment variables or other settings.
    """
    value = None
    source_dicts = [__global_settings]

    # Given settings dictionary has priority over global settings dictionary.
    if settings_dict:
        value = settings_dict.get(key)
        source_dicts.insert(0, settings_dict)
    if value is None:
        value = __global_settings.get(key)

    if value and isinstance(value, str):
        value = expand_vars(value, source_dicts)
    if key in __path_keys:
        value = Path(value) if value else ''
    return value
