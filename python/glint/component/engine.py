# -*- coding: utf-8 -*-
"""
    glint.component.engine
    ~~~~~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import logging
from logging.handlers import RotatingFileHandler
try:
    from os import scandir
except ImportError:
    # Python 2 3rd-party module
    from scandir import scandir
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from . import Component, get_component_manager
from .tool import load_tool
from .plugin import PluginBase, load_plugin
from ..site import get_tool_root
from ..settings import get_setting
from ..constants import COMPONENT_TYPE
from ..logging_setup import configure_logger, BACKUP_COUNT, MAX_LOGFILE_SIZE
from ..api.errors import GlintError


log = logging.getLogger('glint.core.component.engine')
comp_mgr = get_component_manager()


class EngineBase(Component):
    """Base class for a glint engine.

    The engine component connects glint to a host application. This enables
    the functionality of glint to be accessed from inside the application and
    vice versa.
    """
    __launchers = {}

    # --------------------------------------------------------------------------
    #   overridable methods
    # --------------------------------------------------------------------------

    def start(self):
        """Called by the engine boostrap after the engine is loaded."""

    def stop(self):
        """Called when the engine is to be unloaded."""

    def get_host_application(self):
        """Return a reference to the host application the engine is connected to."""

    def has_ui(self):
        """Indicate that the host application connected to the engine has a
        GUI enabled.

        This can always returns False for some engines (such as a commandline-
        based engine) and may vary for some engines, depending on, for example,
        whether the host application is running in batch mode or UI mode. The
        default implementation assumes the engine is not connected to a graphical
        application.

        Returns:
            A boolean value indicating that the host application uses a GUI.
        """
        return False

    # --------------------------------------------------------------------------
    #   public properties
    # --------------------------------------------------------------------------

    # Flag which indicates that tools that support the engine will be loaded
    # after the engine component has been loaded.
    tools_enabled = True

    @property
    def menu_name(self):
        """The name of the main glint menu. This defaults to Glint unless
        overridden in settings.ini.
        """
        try:
            return self.__menu_name
        except AttributeError:
            self.__menu_name = self.get_setting('studio_id')
            return self.__menu_name

    # --------------------------------------------------------------------------
    #   public methods
    # --------------------------------------------------------------------------

    def get_loaded_tools(self):
        return comp_mgr.get_loaded_components(COMPONENT_TYPE.Tool)

    def has_gui(self):
        return self.has_ui()

    def register_launcher(self, name, properties, callback, *args, **kwargs):
        """
        :param name: The name that uniquely identifies the launcher item.
        :param properties: Store configuration related to how the launcher item
            is displayed.
        :param callback: Function to call when the launcher is activated.
        :param args: Positional arguments to pass to the callback.
        :param kwargs: Keyword arguments to pass to the callback.
        """
        entry = {'properties': properties, 'callback': callback,
                 'callback_args': args, 'callback_kwargs': kwargs}
        self.__launchers[name] = entry

    def execute_callback(self, name):
        """
        :param name: The name that uniquely identifies the launcher item.
        """
        callback = self.__launchers[name]['callback']
        args = self.__launchers[name]['callback_args']
        kwargs = self.__launchers[name]['callback_kwargs']
        return callback(*args, **kwargs)

    def get_launchers(self):
        """Return a sorted list of registered launchers.

        The launchers are sorted by groups. Within each group, launchers are
        sorted based on each launcher's specified position within its group.
        """
        # Extract group number and group position information for each launcher.
        temp = {}
        for name in self.__launchers:
            data = self.__launchers[name]
            props = data['properties']

            # All menu items with no specified group are placed at the bottom of
            # the glint menu.
            group = props.get('group')
            # Place the menu item last if position within group is not specified.
            grp_pos = data['properties'].get('position_in_group', 100)
            params = {
                'name': name,
                'label': props.get('label', name),
                'tooltip': props.get('tooltip', ''),
                'icon': props.get('icon'),
                'shortcut': props.get('shortcut'),
            }

            temp.setdefault(group, []).append((grp_pos, params))

        # Using the extracted information, create a sorted list of launchers
        # in display order.
        sorted_list = []
        if temp:
            last_group = max(temp.keys())
            for group_num in range(1, last_group+1):
                if group_num not in temp:
                    continue

                group = []
                for _, params in sorted(temp[group_num]):
                    group.append(params)
                sorted_list.append(group)

            others = temp.get(None)
            if others:
                group = []
                for _, params in sorted(others):
                    group.append(params)
                sorted_list.append(group)

        return sorted_list


# ------------------------------------------------------------------------------
#   public functions
# ------------------------------------------------------------------------------

def load_engine(location, **init_kwargs):
    """Return the initialized engine object."""
    if comp_mgr.get_current_engine():
        raise GlintError("A glint engine is already loaded! Before you can "
                         "load a new engine, please unload the current engine.")

    log.info("Loading the glint engine located in %s...", location)
    c_main = None
    try:
        c_main = comp_mgr.load_component(location, COMPONENT_TYPE.Engine, EngineBase)

    except Exception:
        log.exception("Failed to load the glint engine located in %s.", location)
        raise

    else:
        # If disk logging is enabled, redirect log messages for all components,
        # including core, to an engine-specific log file. This makes it easier
        # to see all the glint log messages emitted while an engine is running.
        if get_setting('log_to_disk'):
            log_file = Path(get_setting('log_file')).parent/('%s.log' % c_main.name)
            handler = RotatingFileHandler(log_file, delay=True,
                                          backupCount=BACKUP_COUNT,
                                          maxBytes=MAX_LOGFILE_SIZE)
            formatter = logging.getLogger('glint').handlers[0].formatter
            configure_logger(handler, formatter)

        # Enable other components to access any 3rd-party packages that
        # the engine uses.
        c_main.register_component_paths()

        # If the engine depends on plugins for proper operation, load
        # them here. Any required plugin that fails to load will unload
        # the engine and any engine plugins previously loaded and abort
        # the engine loading process.
        loaded = []
        for n in c_main.get_meta('requires', {}):
            n = n.strip()
            if n == 'core':
                continue
            try:
                loaded.append(load_plugin(n))
            except Exception:
                for c in loaded:
                    comp_mgr.unload_component(c.name)
                comp_mgr.unload_component(c_main.name)
                raise

        # If additional plugins are specified, either in engine settings
        # or core settings, load them here. Any additional plugin that
        # fails to load will not stop the engine loading process.
        for n in c_main.get_setting('additional_plugins'):
            n = n.strip()
            if n == 'core':
                continue
            try:
                loaded.append(load_plugin(n))
            except Exception:
                pass

        # Call the engine's initializer. If there is an error initializing
        # the engine, the engine is unloaded and all the plugins that have
        # been previously loaded are unloaded and the engine loading process
        # is aborted.
        try:
            comp_mgr.run_initializer(c_main, **init_kwargs)
        except Exception as e:
            log.exception("Error in executing the engine's initializer: %s", e)
            for c in loaded:
                comp_mgr.unload_component(c.name)
            comp_mgr.unload_component(c_main.name)
            raise

        # Finally, load the tools that support the current engine.
        tools_root = get_tool_root()
        if c_main.tools_enabled and tools_root.exists():
            for dir_entry in scandir(str(tools_root)):
                if not dir_entry.is_dir():
                    continue
                load_tool(tools_root/dir_entry.name)

        log.info("Successfully loaded glint engine: %s.", c_main.name)

    return c_main


def get_current_engine():
    return comp_mgr.get_current_engine()
