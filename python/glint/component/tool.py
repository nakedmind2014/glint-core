# -*- coding: utf-8 -*-
"""
    glint.component.tool
    ~~~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import logging

from . import Component, get_component_manager, get_main
from ..constants import COMPONENT_TYPE
from ..api.errors import GlintError


log = logging.getLogger('glint.core.component.tool')
comp_mgr = get_component_manager()


class GlintUnsupportedError(Exception):
    """Exception that indicates a tool does not support an engine."""
    pass


class ToolBase(Component):
    # --------------------------------------------------------------------------
    #   methods that need to be implemented by tools
    # --------------------------------------------------------------------------

    def run(self):
        pass


# ------------------------------------------------------------------------------
#   public functions
# ------------------------------------------------------------------------------

def load_tool(location, load_plugins=True):
    """Return the initialized tool object."""
    engine = comp_mgr.get_current_engine()

    def pre_load(manifest):
        if engine.name not in manifest.get('supports', {}).keys():
            raise GlintUnsupportedError
        # If the tool depends on plugins for proper operation, load them here.
        if load_plugins:
            from .plugin import load_plugin
            for n in manifest.get('requires', {}):
                n = n.strip()
                if n == 'core':
                    continue
                load_plugin(n)

    c_main = None
    tool_name = location.stem
    callbacks = {'pre_load': pre_load}
    try:
        c_main = comp_mgr.load_component(location, COMPONENT_TYPE.Tool,
                                         ToolBase, callbacks)

    except GlintUnsupportedError:
        log.debug("Glint tool '%s' does not support current glint "
                  "engine. Tool will not be loaded.", tool_name)

    except GlintError as e:
        comp_mgr.unload_component(tool_name)
        log.error(e)

    except Exception:
        comp_mgr.unload_component(tool_name)
        log.exception("Failed to load glint tool: %s.", tool_name)

    else:
        try:
            comp_mgr.run_initializer(c_main)
        except Exception as e:
            c_main = None
            comp_mgr.unload_component(tool_name)
            log.exception("Failed to load glint tool %s. Error in executing "
                          "the tool's initializer: %s", tool_name, e)
        else:
            log.info("Successfully loaded glint tool: %s.", c_main.name)

    return c_main


def reload_tool(tool_name):
    c_main = get_main(tool_name)
    if not c_main:
        log.error("Reload failed. Glint tool is not yet loaded: %s.", tool_name)
        return

    try:
        c_main = comp_mgr.reload_component(tool_name, ToolBase)
    except Exception:
        c_main = None
        log.exception("Failed to reload glint tool: %s.", tool_name)
    else:
        comp_mgr.run_initializer(c_main)
        log.info("Successfully reloaded glint tool: %s.", tool_name)
    return c_main
