# -*- coding: utf-8 -*-
"""
    glint.component.plugin
    ~~~~~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import logging

from . import Component, get_component_manager, get_main
from ..site import get_plugin_root
from ..constants import COMPONENT_TYPE
from ..api.errors import GlintError


log = logging.getLogger('glint.core.component.plugin')
comp_mgr = get_component_manager()


class PluginBase(Component):
    """Base class for a glint plugin."""
    pass


# ------------------------------------------------------------------------------
#   public functions
# ------------------------------------------------------------------------------

def load_plugin(name):
    """Return the initialized plugin object."""
    c_main = get_main(name)
    if c_main:
        log.debug("Glint plugin already loaded: %s.", name)
        return c_main

    def pre_load(manifest):
        # If the plugin depends on other plugins for proper operation,
        # load them here.
        for n in manifest.get('requires'):
            n = n.strip()
            if n == 'core':
                continue
            load_plugin(n)

    location = get_plugin_root()/name
    callbacks = {'pre_load': pre_load}
    try:
        c_main = comp_mgr.load_component(location, COMPONENT_TYPE.Plugin,
                                         PluginBase, callbacks)

    except GlintError as e:
        comp_mgr.unload_component(name)
        log.error(e)
        raise

    except Exception:
        comp_mgr.unload_component(name)
        log.exception("Failed to load glint plugin: %s.", name)
        raise

    else:
        try:
            comp_mgr.run_initializer(c_main)
        except Exception as e:
            c_main = None
            comp_mgr.unload_component(name)
            log.exception("Failed to load glint plugin %s. Error in executing "
                          "the plugin's initializer: %s", name, e)
            raise
        else:
            log.info("Successfully loaded glint plugin: %s.", c_main.name)

    return c_main


def reload_plugin(name):
    c_main = get_main(name)
    if not c_main:
        log.error("Reload failed. Glint plugin is not yet loaded: %s.", name)
        return

    try:
        c_main = comp_mgr.reload_component(name, PluginBase)
    except Exception:
        c_main = None
        log.exception("Failed to reload glint plugin: %s.", name)
    else:
        comp_mgr.run_initializer(c_main)
        log.info("Successfully reloaded glint plugin: %s.", name)
    return c_main
