"""
Based on this recipe:
http://code.activestate.com/recipes/160164/
"""
import weakref


_class_refs = {}


class MetaInstanceTracker(type):
    def __new__(cls, name, bases, ns):
        t = super(MetaInstanceTracker, cls).__new__(cls, name, bases, ns)
        t.__instance_refs__ = []
        _class_refs[name] = weakref.ref(t)
        return t

    def __instances__(self):
        instances = [(r, r()) for r in self.__instance_refs__]
        instances = filter(lambda x_y: x_y[1] is not None, instances)
        self.__instance_refs__ = [r for (r, o) in instances]
        return [o for (r, o) in instances]

    def __call__(self, *args, **kw):
        instance = super(MetaInstanceTracker, self).__call__(*args, **kw)
        self.__instance_refs__.append(weakref.ref(instance))
        return instance


class InstanceTracker(object):
    __metaclass__ = MetaInstanceTracker


class MetaAutoReloader(MetaInstanceTracker):
    def __new__(cls, name, bases, ns):
        old_class = None
        if name in _class_refs:
            old_class = _class_refs[name]()

        new_class = super(MetaAutoReloader, cls).__new__(cls, name, bases, ns)

        if old_class:
            for instance in old_class.__instances__():
                instance.change_class(new_class)
                new_class.__instance_refs__.append(
                    weakref.ref(instance))

            for subcls in old_class.__subclasses__():
                newbases = ()
                for base in subcls.__bases__:
                    if base is old_class:
                        newbases += (new_class,)
                    else:
                        newbases += (base,)
                subcls.__bases__ = newbases

            del old_class

        # Original code from recipe.
        # f = inspect.currentframe().f_back
        # for d in [f.f_locals, f.f_globals]:
        #     if d.has_key(name):
        #         old_class = d[name]
        #         for instance in old_class.__instances__():
        #             instance.change_class(new_class)
        #             new_class.__instance_refs__.append(
        #                 weakref.ref(instance))
        #         # this section only works in 2.3
        #         for subcls in old_class.__subclasses__():
        #             newbases = ()
        #             for base in subcls.__bases__:
        #                 if base is old_class:
        #                     newbases += (new_class,)
        #                 else:
        #                     newbases += (base,)
        #             subcls.__bases__ = newbases
        #         break

        return new_class


class GlintBase(object):
    __metaclass__ = MetaAutoReloader

    def change_class(self, new_class):
        self.__class__ = new_class
