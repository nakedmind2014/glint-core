# -*- coding: utf-8 -*-
"""
    glint.component
    ~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import gc
import io
import os
import sys
import imp
import inspect
import logging
import platform
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

import simplejson as json

from ..hook import register_hooks
from ..site import get_glint_root
from ..util import import_file, register_path
from ..settings import get_setting, load_settings
from ..logging_setup import log_levels
from ..constants import (
    COMPONENT_PYTHON_FOLDER, COMPONENT_TYPE, COMPONENT_MANIFEST,
    COMPONENT_MAIN, COMPONENT_HOOK_FOLDER
)
from .reloader import GlintBase
from ..api.errors import GlintError


# Some hard-coded settings definition to minimize boilerplate. This is
# combined with the settings definition in the component's manifest.
builtin_settings = {
    'log_level': {
        'description': "Controls the verbosity of the messages emitted by the component logger.",
        'default_value': None,  # defaults to global value
        'private': True
    },
    'additional_plugins': {
        'type': 'list',
        'description': ("Other plugins that will be loaded by the engine."
                        "These plugins are specified in the settings file."),
        'default_value': [],  # no additional plugins will be loaded
        'private': True
    }
}
log = logging.getLogger('glint.core.component')


class ComponentManager(object):
    """Manages component life cycle, loading and instantiating components."""
    # This will point to a component manager instance. Only one instance is
    # needed to manage components.
    __inst = None

    # This will point to an engine instance. Only one engine can be loaded
    # at a time.
    __loaded_engine = None

    # Enables quick lookup of component data using component location.
    _component_registry_l = {}

    # Enables quick lookup of component data using component name.
    _component_registry_n = {}

    # Enables quick lookup of component data using the component's id.
    _component_registry_i = {}

    # Enables quick lookup of the entry-point modules of components using component name.
    _main_modules = {}

    def __init__(self):
        # Only one instance of the manager class is allowed.
        ComponentManager.__inst = self

    def _load_component_manifest(self, location):
        """Load and validate a component's manifest. Required entries
        must be present.
        """
        with io.open(str(location/COMPONENT_MANIFEST)) as fh:
            try:
                manifest = json.load(fh, object_pairs_hook=json.OrderedDict)
            except ValueError:
                # Make sure info on malformed json data is included in error message.
                raise GlintError("Error reading manifest of component located in %s. "
                                 "Could be invalid json data. It will not be loaded."
                                 % location)
            else:
                if not manifest.get('name'):
                    raise GlintError("Invalid component manifest. Missing entry: name.")
                if not manifest.get('version'):
                    raise GlintError("Invalid component manifest. Missing entry: version.")
            return manifest

    def _load_component_settings(self, location, manifest):
        """Load component settings. Add hard-coded settings definition to
        manifest data.
        """
        settings = {}
        settings_def = manifest.setdefault('settings', {})
        settings_def.update(builtin_settings)
        load_settings(manifest.get('name'), settings_def, settings)
        return settings

    def _override_logging_setup(self, component):
        """Set up component-specific logging configuration.

        Set the logging level specific to the component and its modules.
        The default is to output only error messages.
        """
        channel = 'glint.%s' % component.name
        logger = logging.getLogger(channel)
        setting = component.get_setting('log_level')
        log_level = log_levels.get(setting)
        if log_level:
            logger.setLevel(log_level)
            log.debug("Log level for %s set to %s.", logger.name, setting)
        else:
            log.debug(("Invalid or unknown log level %s specified for %s. "
                       "Using global setting."), setting, logger.name)

    def _core_version_check(self, manifest):
        """Verify that a component's core version requirement is met.

        If required core version is not specified in the manifest, this means
        the component will work on any version of the core.

        :param manifest: Component manifest data.
        """
        version_ok = True
        core_version = _core_manifest["version"]
        required_core_version = manifest.get("required_core_version")
        if required_core_version:
            version_ok = validate_version(core_version, required_core_version)
        if not version_ok:
            c_name = manifest.get("name")
            raise GlintVersionError("The glint component %s requires core version %s. "
                                    "Current core version is %s. Component will not be loaded."
                                    % (c_name,required_core_version, core_version) )

    def _do_version_checking(self):
        pass
        # TODO:
        # Add version checking here. Check core version and plugin version
        # against plugin's required_core_version and required_plugin_version.
        # core_version = glint.get_meta['version']
        # required_core_version = self.__meta['required_core_version']
        # plugin_version = plugin.get_meta['version']
        # required_plugin_version = self.__meta['required_plugin_version']
        # if core_version  < required_core_version:
        #       raise GlintError("The pipeline tool %s requires core version %s. "
        #                        "Tool will not be loaded."
        #                        % (self.name,required_core_version) )
        # if plugin_version  < required_plugin_version:
        #       raise GlintError("The pipeline tool %s requires glint plugin version %s. "
        #                        "Tool will not be loaded."
        #                        % (self.name,required_plugin_version) )
        # TODO:
        # Add version checking here. Check core version against plugin's
        # required_core_version.
        # core_version = glint.get_meta['version']
        # required_core_version = self.__meta['required_core_version']
        # if core_version  < required_core_version:
        #       raise GlintError("The glint plugin %s requires core version %s. "
        #                        "Plugin will not be loaded."
        #                        % (self.name,required_core_version) )

    def load_core_manifest(self):
        """Load and validate the core's manifest. Required entries
        must be present.
        """
        with io.open(str(get_glint_root()/'core'/COMPONENT_MANIFEST)) as fh:
            try:
                manifest = json.load(fh, object_pairs_hook=json.OrderedDict)
            except ValueError:
                # Make sure info on malformed json data is included in error message.
                raise GlintError("Error reading core manifest. Could be invalid "
                                 "json data. It will not be loaded.")
            else:
                if not manifest.get('version'):
                    raise GlintError("Invalid core manifest. Missing entry: version.")
            return manifest

    def load_component(self, location, c_type, baseclass, callbacks={}):
        """Load and initialize a component object.

        :param location: Absolute path to the component's root folder
            containing the python module in which the component entry
            point is defined.
        :param baseclass: The class from which the component entry point
            is derived. Currently, 3 base classes are defined, corresponding
            to the 3 types of components. These are:
                - EngineBase for engines,
                - PluginBase for plugins,
                - ToolBase for tools.
        :param callbacks: Dictionary containing caller-supplied functions
            that will be called at predefined points in the loading process.
            These are:
                - pre_load: before the component entry point is loaded.
            Exceptions in the callback are not handled, effectively aborting
            the loading process.
        :return: :class:`Component <Component>` object
        """
        if not location.exists():
            raise GlintError("Component location %s does not exist." % location)

        # A valid component folder must contain a component entry point
        # and a component manifest.
        if not (location/COMPONENT_MAIN).exists():
            raise GlintError("Entry point was not found for component located "
                             "in %s. It will not be loaded." % location)
        if not (location/COMPONENT_MANIFEST).exists():
            raise GlintError("Manifest was not found for component located "
                             "in %s. It will not be loaded." % location)

        manifest = self._load_component_manifest(location)
        settings = self._load_component_settings(location, manifest)
        self._do_version_checking()

        # Run pre_load callback if supplied.
        cb = callbacks.get('pre_load')
        if cb:
            cb(manifest)

        c_mod = import_file(location/COMPONENT_MAIN)
        c_class = get_main_class(c_mod, baseclass)
        c_main = c_class()

        # Register the hooks defined in the component manifest.
        register_hooks(manifest, location/COMPONENT_HOOK_FOLDER)

        # Update the various component-related registries. Components should use
        # glint.get_main to gain access to another component's entry point object.
        c_name = _normalize_name(manifest.get('name'))
        c_type = (c_type if c_type in COMPONENT_TYPE else COMPONENT_TYPE.Unknown)
        registry_entry = {'manifest': manifest, 'settings': settings,
                          'type': c_type, 'main': c_main}
        self._component_registry_l[location] = registry_entry
        self._component_registry_n[c_name] = registry_entry
        self._component_registry_i[c_main] = registry_entry
        self._main_modules[c_name] = c_mod

        # Perform component-specific logging customization (optional).
        self._override_logging_setup(c_main)

        # Automatically set the current engine if the component
        # is an engine.
        if c_type == COMPONENT_TYPE.Engine:
            self.__loaded_engine = c_main

        return c_main

    def unload_component(self, component_name):
        main = get_main(component_name)
        if main:
            # Delete component entry in the registry.
            loc = main.location
            c_type = main.type
            name = _normalize_name(component_name)
            del self._component_registry_n[name]
            del self._component_registry_l[loc]
            del self._component_registry_i[main]
            del self._main_modules[name]

            # Delete references to component's loaded modules from sys.modules
            loaded_modules = {}
            mod_keys = list(sys.modules.keys())
            for key in mod_keys:
                module = sys.modules[key]
                if not module:
                    continue
                try:
                    module_path = Path(module.__file__)
                except AttributeError:
                    continue

                if loc in module_path.parents:
                    if module_path in [(loc/'main.pyc'), (loc/'main.py')]:
                        del sys.modules[key]
                    else:
                        loaded_modules[key] = module

            for key in loaded_modules:
                del sys.modules[key]
            gc.collect()

            # Automatically reset the current engine if the component
            # is an engine.
            if c_type == COMPONENT_TYPE.Engine:
                self.__loaded_engine = None

            return loaded_modules

    def reload_component(self, component_name, baseclass):
        c_main = get_main(component_name)
        c_type = c_main.type
        loaded_modules = self.unload_component(component_name)

        # Reload the component main instance and component modules.
        c_main = self.load_component(c_main.location, c_type, baseclass)
        c_main.lib  # load the lib modules
        for key in loaded_modules:
            try:
                newmodule = sys.modules[key]
            except KeyError:
                continue

            # Other objects might still be referencing the old modules
            # of the reloaded component. Doing this will allow them to
            # have access to whatever changes in the reloaded component
            # modules.
            oldmodule = loaded_modules[key]
            oldmodule.__dict__.clear()
            oldmodule.__dict__.update(newmodule.__dict__)

        return c_main

    def get_current_engine(self):
        return self.__loaded_engine

    def get_main_module(self, component_name):
        return self._main_modules.get(_normalize_name(component_name))

    def get_loaded_components(self, c_type):
        return [v['main'] for k, v in self._component_registry_n.items()
                if v['main'].type == c_type]

    def run_initializer(self, c_main, **init_kwargs):
        c_mod = self.get_main_module(c_main.name)
        try:
            initializer = c_mod.initializer
        except AttributeError:
            pass
        else:
            python_folder = str(c_main.python_folder)
            sys.path.insert(0, python_folder)
            try:
                initializer(c_main, **init_kwargs)
            finally:
                sys.path.remove(python_folder)

    @classmethod
    def instance(cls):
        if not cls.__inst:
            cls.__inst = ComponentManager()
        return cls.__inst


class Component(GlintBase):
    """The abstract base class for all glint components such as plugins,
    tools, engines, and others yet to be defined.

    This is the entry point to a component. The instanced entry point object
    can be accessed by other modules by using:

    for modules within the component:       glint.get_main()
    for modules not part of the component:  glint.get_main(component_name)
    """
    __lib = None
    __comp_mgr = ComponentManager.instance()

    # --------------------------------------------------------------------------
    #   read-only public properties
    # --------------------------------------------------------------------------

    @property
    def name(self):
        return self.get_meta('name')

    @property
    def display_name(self):
        return self.get_meta('display_name', self.name)

    @property
    def version(self):
        return self.get_meta('version')

    @property
    def description(self):
        return self.get_meta('description', '')

    @property
    def type(self):
        try:
            return self.__type
        except AttributeError:
            self.__type = self.__comp_mgr._component_registry_i[self]['type']
        return self.__type

    @property
    def location(self):
        """The folder on disk where the component's entry point is located."""
        try:
            return self.__location
        except AttributeError:
            main_file = Path(sys.modules[self.__module__].__file__)
            self.__location = main_file.resolve().parent
            return self.__location

    @property
    def logger(self):
        try:
            return self.__logger
        except AttributeError:
            self.__logger = logging.getLogger('glint.%s.main' % self.name)
            return self.__logger

    @property
    def python_folder(self):
        """ The folder containing the component's python modules which may
        include 3rd-party libraries.
        """
        return self.location/COMPONENT_PYTHON_FOLDER

    @property
    def lib(self):
        if self.__lib is None:
            self.__lib = _import_lib(self)
        return self.__lib

    # --------------------------------------------------------------------------
    #   public methods
    # --------------------------------------------------------------------------

    def get_meta(self, key, default=None):
        """Get the value from the component's manifest.

        :param key: Name of metadata.
        :param default: Default value to return if key is not declared.
        :return: manifest data which can be any valid python value.
        """
        try:
            return self.__meta.get(key, default)
        except AttributeError:
            self.__meta = self.__comp_mgr._component_registry_i[self]['manifest']
            return self.__meta.get(key, default)

    def get_setting(self, key):
        """Get the value from either the local component settings or from the
        global settings.

        :param key: Name of setting key.
        :return: setting value which can be any valid python value.
        """
        try:
            s = self.__settings
        except AttributeError:
            s = self.__settings = self.__comp_mgr._component_registry_i[self]['settings']
        return get_setting(key, s)

    def get_logger(self):
        caller_fp = Path(inspect.stack()[1][1])
        try:
            rel_path = caller_fp.relative_to(self.location)

        except ValueError:
            # Caller is not in a component module. Just return the root logger.
            logger = logging.getLogger()

        else:
            # Caller is in the component main file. Note that there is no need
            # to call get_logger from within the entry point object.
            if caller_fp.name == COMPONENT_MAIN:
                logger = self.logger

            else:
                parts = rel_path.parts
                parts_count = len(parts)
                # Caller is in a script or module located in the component root.
                if parts_count == 1:
                    suffix = rel_path.stem

                # Caller is in a module located inside the component's python
                # folder.
                elif parts[0] == COMPONENT_PYTHON_FOLDER:
                    x = '.'.join(parts[1:-1])
                    suffix = ('lib.%s' % x) if x else 'lib'
                    filename = (rel_path.stem if rel_path.stem != '__init__' else '')
                    suffix = ('%s.%s' % (suffix, filename) if filename else suffix)

                # Caller is in a module located inside another component folder.
                else:
                    suffix = '.'.join(parts[:-1])
                    filename = (rel_path.stem if rel_path.stem != '__init__' else '')
                    suffix = ('%s.%s' % (suffix, filename) if filename else suffix)

                channel = 'glint.%s.%s' % (self.name, suffix)
                logger = logging.getLogger(channel)

        return logger

    def resolve_vendor_foldername(self):
        """Get the name of the folder containing 3rd-party python libraries
        with compiled modules. If a component uses a different naming scheme
        this must be re-implemented.

        :return: folder name.
        """
        # suffix to indicate operating system
        os_name = platform.system()
        suffix = {'Windows': '_win64', 'Linux': '_linux'}[os_name]

        # python version-specific infix which uses only the first 2 components
        # of the python version. e.g. 2.6 for 2.6.6 or 2.7 for 2.7.8
        infix = '_py' + ''.join( platform.python_version_tuple()[:2] )

        # version-specific and os-specific
        return 'vendor' + infix + suffix

    def register_component_paths(self):
        python_folder = self.location/COMPONENT_PYTHON_FOLDER
        vendor_folder = self.resolve_vendor_foldername()

        # Make sure a 3rd-party library's binary files are found.
        bin_dir = python_folder/vendor_folder/'bin'
        if bin_dir.exists():
            log.debug("Added to system path: %s", bin_dir)
            os.environ['PATH'] = str(bin_dir) + os.pathsep + os.environ['PATH']

        # Update python's module search path. Version-specific 3rdparty modules
        # have higher priority.
        vendor_dir1 = python_folder/vendor_folder/'lib'
        vendor_dir2 = python_folder/'vendor_common_lib'
        register_path(vendor_dir2)
        register_path(vendor_dir1)


# ------------------------------------------------------------------------------
#   helper functions
# ------------------------------------------------------------------------------

def _normalize_name(component_name):
    return component_name.lower().replace('-', '_')


def _import_lib(component):
    """Import the python folder inside the component and make it accessible
    to other modules under the component_name.lib namespace.
    """
    # Get the python folder. Make sure it's a valid package folder.
    python_folder = component.python_folder
    if not python_folder.exists():
        raise GlintError("Cannot import - folder %s does not exist!"
                         % python_folder)
    if not (python_folder/'__init__.py').exists():
        raise GlintError("Cannot import - folder %s is not a valid "
                         "python package!" % python_folder)

    # Import the folder as a python package under the component namespace.
    # The component's python folder is temporarily prepended to python's
    # module search path. This prevents python search path from growing too
    # long, in case there are many components loaded, but makes it possible
    # for component modules to still import other component modules as if
    # the python folder is in the search path.
    vendor_dir = str(python_folder/'vendor_common_lib')
    python_folder = str(python_folder)
    module_name = component.name.replace('-', '_') + '.lib'
    sys.path.insert(0, vendor_dir)
    sys.path.insert(0, python_folder)
    try:
        return imp.load_module(module_name, None, python_folder,
                               ('', '', imp.PKG_DIRECTORY))
    finally:
        sys.path.remove(vendor_dir)
        sys.path.remove(python_folder)


def _resolve_vendor_foldername():
    """Get the name of the folder containing 3rd-party python libraries
    with compiled modules.

    :return: folder name.
    """
    # suffix to indicate operating system
    os_name = platform.system()
    suffix = {'Windows': '_win64', 'Linux': '_linux'}[os_name]

    # python version-specific infix which uses only the first 2 components
    # of the python version. e.g. 2.6 for 2.6.6 or 2.7 for 2.7.8
    infix = '_py' + ''.join(platform.python_version_tuple()[:2])

    # version-specific and os-specific
    return 'vendor' + infix + suffix


# ------------------------------------------------------------------------------
#   public functions
# ------------------------------------------------------------------------------

def get_component_manager():
    return ComponentManager.instance()


def run(name, *args, **kwargs):
    main = get_main(name)
    return main.run(*args, **kwargs)


def get_main(component_name=None):
    c_main = None
    comp_mgr = ComponentManager.instance()
    if component_name:
        name = _normalize_name(component_name)
        try:
            c_main = comp_mgr._component_registry_n.get(name, {})['main']
        except KeyError:
            pass
    else:
        # Extracts path of caller.
        fp = Path(inspect.stack()[1][1])
        # Loop through the registry to find any matching component by checking
        # if caller is located inside component folder.
        for location, obj in comp_mgr._component_registry_l.items():
            try:
                fp.relative_to(location)
            except ValueError:
                pass
            else:
                c_main = obj['main']
                break
    return c_main


def get_main_class(module, valid_base_class):
    valid_classes = []
    try:
        for var in dir(module):
            value = getattr(module, var)
            if not inspect.isclass(value):
                continue
            for k in inspect.getmro(value):
                if k.__name__ == valid_base_class.__name__:
                    # Found a valid class.
                    valid_classes.append(value)
    except Exception as e:
        raise GlintError("Error while trying to load and inspect file %s. "
                         "Error Reported: %s" % (module.__file__, e))

    if not valid_classes:
        # missing class!
        msg = ("Error loading the file '%s'. Couldn't find a class deriving from the "
               "base class '%s'." % (module.__file__, valid_base_class.__name__))
        raise GlintError(msg)

    return valid_classes[-1]


def register_component_paths(location):
    python_folder = location/COMPONENT_PYTHON_FOLDER
    vendor_folder = _resolve_vendor_foldername()

    # Make sure a 3rd-party library's binary files are found.
    bin_dir = python_folder/vendor_folder/'bin'
    if bin_dir.exists():
        log.debug("Added to system path: %s", bin_dir)
        os.environ['PATH'] = str(bin_dir) + os.pathsep + os.environ['PATH']

    # Update python's module search path. Version-specific 3rdparty modules
    # have higher priority.
    vendor_dir1 = python_folder/vendor_folder/'lib'
    vendor_dir2 = python_folder/'vendor_common_lib'
    register_path(vendor_dir1)
    register_path(vendor_dir2)
