# -*- coding: utf-8 -*-
"""
    glint.component.launcher
    ~~~~~~~~~~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import os
import sys
import logging
import platform
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from ..site import get_glint_root
from ..constants import COMPONENT_PYTHON_FOLDER


log = logging.getLogger('glint.core.component.launcher')


class AppLauncher(object):
    def __init__(self):
        super(AppLauncher, self).__init__()
        # self.load_required_plugins()

    @property
    def location(self):
        """ The folder on disk where the component's entry point is located. """
        try:
            return self.__location
        except AttributeError:
            main_file = Path(sys.modules[self.__module__].__file__)
            self.__location = main_file.abspath().parent.normpath()
            return self.__location

    def register_core_paths(self):
        python_folder = get_glint_root()/'core'/COMPONENT_PYTHON_FOLDER
        python_path = os.environ.get('PYTHONPATH', '')
        python_path += os.pathsep + python_folder
        python_path += os.pathsep + (python_folder/'vendor_common_lib')
        python_path = python_path.strip(os.pathsep)

        os.environ['PYTHONPATH'] = python_path
        log.debug("Updated python path: %s", python_path)

    def register_component_paths(self):
        python_folder = self.location/COMPONENT_PYTHON_FOLDER
        vendor_dirname = self.resolve_vendor_foldername()

        # Make sure a 3rd-party library's binary files are found.
        bin_dir = python_folder/vendor_dirname/'bin'
        if bin_dir.exists():
            log.debug("Added to system path: %s", bin_dir)
            os.environ['PATH'] = bin_dir + os.pathsep + os.environ['PATH']

        # Update python's module search path.
        vendor_dir1 = python_folder/vendor_dirname/'lib'
        vendor_dir2 = python_folder/'vendor_common_lib'
        python_path = os.environ.get('PYTHONPATH', '')
        existing = python_path.split(os.pathsep)

        if vendor_dir1.exists() and vendor_dir1.normcase() not in existing:
            python_path += os.pathsep + vendor_dir1.normcase()
        if vendor_dir2.exists() and vendor_dir2.normcase() not in existing:
            python_path += os.pathsep + vendor_dir2.normcase()
        python_path = python_path.strip(os.pathsep)

        os.environ['PYTHONPATH'] = python_path
        log.debug("Updated python path: %s", python_path)

    def resolve_vendor_foldername(self):
        """
        Get the name of the folder containing 3rd-party python libraries
        with compiled modules. If a component uses a different naming scheme
        this must be re-implemented.

        Returns:
            folder name.
        """
        # suffix to indicate operating system
        os_name = platform.system()
        suffix = {'Windows': '_win64', 'Linux': '_linux'}[os_name]

        # python version-specific infix which uses only the first 2 components
        # of the python version. e.g. 2.6 for 2.6.6 or 2.7 for 2.7.8
        infix = '_py' + ''.join(platform.python_version_tuple()[:2])

        # version-specific and os-specific
        return 'vendor' + infix + suffix
