# -*- coding: utf-8 -*-
"""
    glint.constants
    ~~~~~~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
from enum import Enum


# B

# the file that an application (e.g. maya) loads ...
BOOTSTRAP_PLUGIN = 'plugin_bootstrap.py'

# the file that a glint plugin ...
BOOTSTRAP_ENV = 'env_bootstrap.py'

# C

# ...
COMMAND = Enum('Command', 'Plugin, Tool')

# ...
COMPONENT_TYPE = Enum('ComponentType', 'Engine, Plugin, Tool, Unknown')

# the file that serves as an entry point to a glint component
COMPONENT_MAIN = 'main.py'

# the file that contains component metadata
COMPONENT_MANIFEST = '.meta'

# name of file that contains settings info for all components including core
COMPONENT_SETTINGS = 'glint_settings.ini'

# the folder that contains the component's python modules
COMPONENT_PYTHON_FOLDER = 'python'

# the folder that contains the component's default hook modules
COMPONENT_HOOK_FOLDER = 'hooks'
