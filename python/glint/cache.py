"""
Caching implementation details
- The cache is composed of cache regions that are accessed via a key.
- Each region is associated with a loader function that does a loading of
  data from a datsource such as the site database
- Data from datasource can also be loaded to cache on demand by providing
  a loader argument when getting cache data.
- a glint command 'update-cache' can be used to update content of a cache
  region
- data structure
{
    region_access_key: {
        loader: None,
        data: {}
    }
}
"""
import logging


cache = {}
log = logging.getLogger('glint.core.cache')


def initialize_cache_region(region_id, loader, **kwargs):
    if region_id in cache:
        raise ValueError(f'Cache region with ID {region_id} already initialized.')

    cache[region_id] = {
        'data': loader('init', **kwargs),
        'loader': loader,
        'loader_kwargs': kwargs
    }


def update_cache(region_id, datakey, data):
    cache[region_id]['data'][datakey].update(data)


def get_cache_data(region_id, **kwargs):
    if 'datakey' in kwargs:
        datakey = kwargs['datakey']
        try:
            return cache[region_id]['data'][datakey]

        except KeyError:
            log.debug(f"{datakey} not found in cache, loading from datasource.")
            loader = cache[region_id]['loader']
            kwargs = cache[region_id]['loader_kwargs']
            cache[region_id]['data'][datakey] = loader(datakey, **kwargs)
            return cache[region_id]['data'][datakey]

    else:
        return cache[region_id]['data']


def purge_cache_data(region_id, datakey):
    if datakey in cache[region_id]['data']:
        del cache[region_id]['data'][datakey]
