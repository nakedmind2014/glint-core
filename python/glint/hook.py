# -*- coding: utf-8 -*-
"""
    glint.hook
    ~~~~~~~~~~

    :license: BSD, see LICENSE for more details.
"""
import os
import logging
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

from .util import import_file
from .site import get_glint_root
from .constants import COMPONENT_HOOK_FOLDER
from .api.errors import GlintError


_hook_registry = {}
log = logging.getLogger('glint.core.hook')


def register_hooks(manifest, hook_path):
    for name in manifest.get('hooks', []):
        _hook_registry[name] = {
            'path': hook_path/(name+'.py'),
            'module': None
        }


def _load_hook_helper(hook_name, hook_path):
    hook_mod = import_file(hook_path, do_not_cache=True)
    try:
        hook_mod.main
    except AttributeError:
        log.error("%s is an invalid hook file. No 'main' function found."
                  % hook_path)
    else:
        # Add entry in the registry
        _hook_registry[hook_name] = {'module': hook_mod, 'path': hook_path}


def load_all_hooks():
    all_hook_names = list(_hook_registry)
    if not all_hook_names:
        return

    def loop(hook_dir, remaining_names):
        for name in remaining_names:
            hook_file = hook_dir/(name+'.py')
            if hook_file.exists():
                all_hook_names.remove(name)
                _load_hook_helper(name, hook_file)

    hook_dir = os.environ.get('GLINT_HOOK_PATH')
    if hook_dir:
        hook_dir = Path(hook_dir)
        if hook_dir.exists():
            loop(hook_dir, all_hook_names[:])

    if not all_hook_names:
        return

    hook_dir = get_glint_root()/COMPONENT_HOOK_FOLDER
    if hook_dir.exists():
        loop(hook_dir, all_hook_names[:])

    if not all_hook_names:
        return

    for name in all_hook_names[:]:
        hook_file = _hook_registry[name]['path']
        if hook_file.exists():
            all_hook_names.remove(name)
            _load_hook_helper(name, hook_file)


def load_hook(hook_name):
    """Search for hook module in hook folders and load it.

    Imports file and checks for a function named main. Raise
    exception if no main function found. Otherwise, return the
    imported hook module.
    """
    filename = hook_name + '.py'
    hook_dir = os.environ.get('GLINT_HOOK_PATH')
    if hook_dir:
        hook_path = Path(hook_dir, filename)
        if not hook_path.exists():
            hook_path = get_glint_root()/'hooks'/filename
        if not hook_path.exists():
            hook_path = _hook_registry[hook_name]['path']
        if not hook_path.exists():
            raise GlintError("Missing hook file: {}".format(hook_path))

    else:
        hook_path = get_glint_root()/'hooks'/filename
        if not hook_path.exists():
            hook_path = _hook_registry[hook_name]['path']
        if not hook_path.exists():
            raise GlintError("Missing hook file: {}".format(hook_path))

    hook_mod = import_file(hook_path, do_not_cache=True)
    try:
        hook_mod.main
    except AttributeError:
        raise GlintError("%s is an invalid hook file. No 'main' function found."
                         % hook_path)

    # Add entry in the registry
    _hook_registry[hook_name] = {'module': hook_mod, 'path': hook_path}
    return hook_mod


def run_hook(hook_name, *args, **kwargs):
    if hook_name not in _hook_registry:
        raise GlintError("Hook not registered: %s" % hook_name)

    hook_mod = _hook_registry[hook_name]['module'] or load_hook(hook_name)

    # Log the exception but let the hook caller handle any
    # exception generated during hook execution.
    try:
        return hook_mod.main(*args, **kwargs)
    except Exception as e:
        log.debug("%s: executing hook defined in %s raised an exception: %s",
                  hook_name, _hook_registry[hook_name]['path'], e)
        raise
