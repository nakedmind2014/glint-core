# -*- coding: utf-8 -*-
"""
    glint.logging_setup
    ~~~~~~~~~~~~~~~~~~~

    This module contains the functions used for glint's logging
    setup.

    When debug logging is enabled in core, it applies globally
    to all components. But it can be disabled per component via
    component settings.

    :license: BSD, see LICENSE for more details.
"""
import logging
from logging.handlers import RotatingFileHandler

from .settings import get_setting


BACKUP_COUNT = 1
MAX_LOGFILE_SIZE = 20480

log = logging.getLogger('glint.core.logging_setup')
log_format = 'GLINT: %(message)s'
log_debug_format = 'GLINT (debug): %(name)s: %(message)s'
log_levels = {'debug': logging.DEBUG, 'warning': logging.WARNING,
              'info': logging.INFO, 'error': logging.ERROR}


class GlintFormatterBase(logging.Formatter):
    def __init__(self):
        super(GlintFormatterBase, self).__init__(fmt=log_format)

    def _format_helper(self, record, style):
        """ Use a different output format for debug log records. """
        orig_format = getattr(style, '_fmt')
        orig_name = record.name

        if record.levelno == logging.DEBUG:
            # Remove the redundant 'glint' prefix from the debug message
            record.name = '.'.join(record.name.split('.')[1:])
            setattr(style, '_fmt', log_debug_format)

        s = super(GlintFormatterBase, self).format(record)
        setattr(style, '_fmt', orig_format)
        record.name = orig_name
        return s


# Python 3.2 introduced a new 'style' parameter which made the
# python 2 workaround fail to work.
try:
    logging.Formatter()._style._fmt  # detect 'style' parameter support
    class GlintFormatter(GlintFormatterBase):
        def format(self, record):
            return self._format_helper(record, self._style)
except AttributeError:
    class GlintFormatter(GlintFormatterBase):
        def format(self, record):
            return self._format_helper(record, self)


def configure_main_loggers():
    """Configure glint's top-level loggers.

    This is one of the first steps done during glint's initialization.
    It sets up glint's default log handler and formatter. Components
    can override these defaults if they need to.

    If there is a need to set up logging configurations not possible
    with what glint offers, the entire glint logging setup can be
    disabled/bypassed via the "enable_logging_setup" core setting.

    The default is to configure the logging setup. This setting can
    only be set in core settings file. If it exists in components'
    settings, it is ignored.
    """
    if not get_setting('enable_logging_setup'):
        return

    if get_setting('log_to_disk'):
        handler = RotatingFileHandler(get_setting('log_file'), delay=True,
                                      backupCount=BACKUP_COUNT,
                                      maxBytes=MAX_LOGFILE_SIZE)
    else:
        handler = logging.StreamHandler()
    formatter = GlintFormatter()
    handler.setFormatter(formatter)

    # Configure the main glint logging channel.
    logger = logging.getLogger('glint')

    # Prevent glint logging events from being seen by the root logger.
    logger.propagate = False

    # Delete existing handlers to make way for the new.
    del logger.handlers[:]
    logger.addHandler(handler)

    # Set the log level. The default is to output only error messages.
    setting = get_setting('log_level')
    log_level = log_levels.get(setting)
    if log_level:
        log.debug("Setting global log level to %s...", setting)
        logger.setLevel(log_level)
    else:
        log.debug(("Unable to set global log level. Invalid or unknown "
                   "log level %s specified. Using default (error)."),
                   setting)
        logger.setLevel(logging.ERROR)


def configure_logger(handler=None, formatter=None):
    """Change the handler or formatter of glint's top-level loggers.

    This is used by components to override the default logging setup
    provided by glint.

    :param handler (optional): If not given and a formatter is provided,
        use existing handler.
    :param formatter (optional): If not given, and a handler is provided,
        use existing formatter.
    """
    if not get_setting('enable_logging_setup'):
        return

    # Nothing to do if parameters are not provided.
    if not formatter and not handler:
        return

    # Update the formatter of current log handler.
    if formatter and not handler:
        handler = logging.getLogger('glint').handlers[0]
    # Use glint's default formatter on new handler.
    elif not formatter and handler:
        formatter = GlintFormatter()
    handler.setFormatter(formatter)

    # Configure the main glint logging channels.
    logger = logging.getLogger('glint')
    # Delete existing handlers to make way for the new.
    del logger.handlers[:]
    logger.addHandler(handler)
