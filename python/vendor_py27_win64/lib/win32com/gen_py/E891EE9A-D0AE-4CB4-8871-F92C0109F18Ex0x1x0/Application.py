# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.01
# By python version 2.7.6 (default, Nov 10 2013, 19:24:24) [MSC v.1500 64 bit (AMD64)]
# From type library '{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}'
# On Fri Jun 19 00:27:56 2015
'Adobe Photoshop CS6 Object Library'
makepy_version = '0.5.01'
python_version = 0x20706f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}')
MajorVersion = 1
MinorVersion = 0
LibraryFlags = 8
LCID = 0x0

from win32com.client import CoClassBaseClass
import sys
__import__('win32com.gen_py.E891EE9A-D0AE-4CB4-8871-F92C0109F18Ex0x1x0._Application')
_Application = sys.modules['win32com.gen_py.E891EE9A-D0AE-4CB4-8871-F92C0109F18Ex0x1x0._Application']._Application
# This CoClass is known by the name 'Photoshop.Application.60'
class Application(CoClassBaseClass): # A CoClass
	# The Adobe Photoshop application
	CLSID = IID('{99AEA70E-4D13-44BF-A878-33345CBFCBC8}')
	coclass_sources = [
	]
	coclass_interfaces = [
		_Application,
	]
	default_interface = _Application

win32com.client.CLSIDToClass.RegisterCLSID( "{99AEA70E-4D13-44BF-A878-33345CBFCBC8}", Application )
