# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.01
# By python version 2.7.6 (default, Nov 10 2013, 19:24:24) [MSC v.1500 64 bit (AMD64)]
# From type library '{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}'
# On Fri Jun 19 00:27:56 2015
'Adobe Photoshop CS6 Object Library'
makepy_version = '0.5.01'
python_version = 0x20706f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}')
MajorVersion = 1
MinorVersion = 0
LibraryFlags = 8
LCID = 0x0

from win32com.client import DispatchBaseClass
class _Application(DispatchBaseClass):
	'The Adobe Photoshop application'
	CLSID = IID('{5DE90358-4D0B-4FA1-BA3E-C91BBA863F32}')
	coclass_clsid = IID('{99AEA70E-4D13-44BF-A878-33345CBFCBC8}')

	def Batch(self, InputFiles=defaultNamedNotOptArg, Action=defaultNamedNotOptArg, From=defaultNamedNotOptArg, Options=defaultNamedOptArg):
		'run the batch automation routine'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1112813617, LCID, 1, (8, 0), ((12, 1), (8, 1), (8, 1), (12, 17)),InputFiles
			, Action, From, Options)

	def ChangeColorSettings(self, Name=defaultNamedOptArg, File=defaultNamedOptArg):
		'set Color Settings to a named set or to the contents of a settings file'
		return self._oleobj_.InvokeTypes(1130906490, LCID, 1, (24, 0), ((12, 17), (12, 17)),Name
			, File)

	def CharIDToTypeID(self, CharID=defaultNamedNotOptArg):
		'convert from a four character code to a runtime ID'
		return self._oleobj_.InvokeTypes(1098002483, LCID, 1, (3, 0), ((8, 1),),CharID
			)

	def DoAction(self, Action=defaultNamedNotOptArg, From=defaultNamedNotOptArg):
		'play an action from the Actions Palette'
		return self._oleobj_.InvokeTypes(1148141923, LCID, 1, (24, 0), ((8, 1), (8, 1)),Action
			, From)

	def DoJavaScript(self, JavaScriptCode=defaultNamedNotOptArg, Arguments=defaultNamedOptArg, ExecutionMode=defaultNamedOptArg):
		'execute JavaScript code'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1147828311, LCID, 1, (8, 0), ((8, 1), (12, 17), (12, 17)),JavaScriptCode
			, Arguments, ExecutionMode)

	def DoJavaScriptFile(self, JavaScriptFile=defaultNamedNotOptArg, Arguments=defaultNamedOptArg, ExecutionMode=defaultNamedOptArg):
		'execute javascript file'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1147823703, LCID, 1, (8, 0), ((8, 1), (12, 17), (12, 17)),JavaScriptFile
			, Arguments, ExecutionMode)

	# Result is of type _ActionDescriptor
	def ExecuteAction(self, EventID=defaultNamedNotOptArg, Descriptor=defaultNamedOptArg, DisplayDialogs=defaultNamedOptArg):
		'play an ActionManager event'
		ret = self._oleobj_.InvokeTypes(1349280121, LCID, 1, (9, 0), ((3, 1), (12, 17), (12, 17)),EventID
			, Descriptor, DisplayDialogs)
		if ret is not None:
			ret = Dispatch(ret, u'ExecuteAction', '{70A60330-E866-46AA-A715-ABF418C41453}')
		return ret

	# Result is of type _ActionDescriptor
	def ExecuteActionGet(self, Reference=defaultNamedNotOptArg):
		'obtain an action descriptor'
		ret = self._oleobj_.InvokeTypes(1095198068, LCID, 1, (9, 0), ((9, 1),),Reference
			)
		if ret is not None:
			ret = Dispatch(ret, u'ExecuteActionGet', '{70A60330-E866-46AA-A715-ABF418C41453}')
		return ret

	def FeatureEnabled(self, Name=defaultNamedNotOptArg):
		'is the feature with the given name enabled?'
		return self._oleobj_.InvokeTypes(1718904174, LCID, 1, (11, 0), ((8, 1),),Name
			)

	def Load(self, Document=defaultNamedNotOptArg):
		'load a support document'
		return self._oleobj_.InvokeTypes(1281643372, LCID, 1, (24, 0), ((8, 1),),Document
			)

	def MakeContactSheet(self, InputFiles=defaultNamedNotOptArg, Options=defaultNamedOptArg):
		'create a contact sheet from multiple files'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1129599816, LCID, 1, (8, 0), ((12, 1), (12, 17)),InputFiles
			, Options)

	def MakePDFPresentation(self, InputFiles=defaultNamedNotOptArg, OutputFile=defaultNamedNotOptArg, Options=defaultNamedOptArg):
		'create a PDF presentation file'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1346651697, LCID, 1, (8, 0), ((12, 1), (8, 1), (12, 17)),InputFiles
			, OutputFile, Options)

	def MakePhotoGallery(self, InputFolder=defaultNamedNotOptArg, OutputFolder=defaultNamedNotOptArg, Options=defaultNamedOptArg):
		'Creates a web photo gallery'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(2004316263, LCID, 1, (8, 0), ((12, 1), (8, 1), (12, 17)),InputFolder
			, OutputFolder, Options)

	def MakePhotomerge(self, InputFiles=defaultNamedNotOptArg):
		'DEPRECATED. Merges multiple files into one, user interaction required.'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1886678375, LCID, 1, (8, 0), ((12, 1),),InputFiles
			)

	def MakePicturePackage(self, InputFiles=defaultNamedNotOptArg, Options=defaultNamedOptArg):
		'create a picture package from multiple files'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1347702859, LCID, 1, (8, 0), ((12, 1), (12, 17)),InputFiles
			, Options)

	# Result is of type Document
	def Open(self, Document=defaultNamedNotOptArg, As=defaultNamedOptArg, AsSmartObject=defaultNamedOptArg):
		'open the specified document'
		ret = self._oleobj_.InvokeTypes(1349731151, LCID, 1, (9, 0), ((8, 1), (12, 17), (12, 17)),Document
			, As, AsSmartObject)
		if ret is not None:
			ret = Dispatch(ret, u'Open', '{B1ADEFB6-C536-42D6-8A83-397354A769F8}')
		return ret

	def OpenDialog(self):
		'use the Photoshop open dialog to select files'
		return self._ApplyTypes_(1886613360, 1, (12, 0), (), u'OpenDialog', None,)

	def Purge(self, Target=defaultNamedNotOptArg):
		'purges one or more caches'
		return self._oleobj_.InvokeTypes(1349874279, LCID, 1, (24, 0), ((3, 1),),Target
			)

	def Quit(self):
		'quit the application'
		return self._oleobj_.InvokeTypes(1903520116, LCID, 1, (24, 0), (),)

	def Refresh(self):
		'pause the script until the application refreshes'
		return self._oleobj_.InvokeTypes(1382445928, LCID, 1, (24, 0), (),)

	def SetBackgroundColor(self, arg0=defaultUnnamedArg):
		return self._oleobj_.InvokeTypes(1650934627, LCID, 8, (24, 0), ((9, 0),),arg0
			)

	def SetForegroundColor(self, arg0=defaultUnnamedArg):
		return self._oleobj_.InvokeTypes(1718043491, LCID, 8, (24, 0), ((9, 0),),arg0
			)

	def StringIDToTypeID(self, StringID=defaultNamedNotOptArg):
		'convert from a string ID to a runtime ID'
		return self._oleobj_.InvokeTypes(1098002481, LCID, 1, (3, 0), ((8, 1),),StringID
			)

	def TypeIDToCharID(self, TypeID=defaultNamedNotOptArg):
		'convert from a runtime ID to a character ID'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1098002484, LCID, 1, (8, 0), ((3, 1),),TypeID
			)

	def TypeIDToStringID(self, TypeID=defaultNamedNotOptArg):
		'convert from a runtime ID to a string ID'
		# Result is a Unicode object
		return self._oleobj_.InvokeTypes(1098002482, LCID, 1, (8, 0), ((3, 1),),TypeID
			)

	_prop_map_get_ = {
		# Method 'ActiveDocument' returns object of type 'Document'
		"ActiveDocument": (1883325539, 2, (9, 0), (), "ActiveDocument", '{B1ADEFB6-C536-42D6-8A83-397354A769F8}'),
		# Method 'Application' returns object of type '_Application'
		"Application": (1667330160, 2, (9, 0), (), "Application", '{5DE90358-4D0B-4FA1-BA3E-C91BBA863F32}'),
		# Method 'BackgroundColor' returns object of type '_SolidColor'
		"BackgroundColor": (1650934627, 2, (9, 0), (), "BackgroundColor", '{D2D1665E-C1B9-4CA0-8AC9-529F6A3D9002}'),
		"Build": (1114205284, 2, (8, 0), (), "Build", None),
		"ColorSettings": (1129988974, 2, (8, 0), (), "ColorSettings", None),
		"DisplayDialogs": (1096116556, 2, (3, 0), (), "DisplayDialogs", None),
		# Method 'Documents' returns object of type 'Documents'
		"Documents": (1685021557, 2, (9, 0), (), "Documents", '{662506C7-6AAE-4422-ACA4-C63627CB1868}'),
		# Method 'Fonts' returns object of type 'TextFonts'
		"Fonts": (1665560180, 2, (9, 0), (), "Fonts", '{BBCE52D6-5D4B-4691-99E3-62C174BD2855}'),
		# Method 'ForegroundColor' returns object of type '_SolidColor'
		"ForegroundColor": (1718043491, 2, (9, 0), (), "ForegroundColor", '{D2D1665E-C1B9-4CA0-8AC9-529F6A3D9002}'),
		"FreeMemory": (1883655501, 2, (5, 0), (), "FreeMemory", None),
		"Locale": (1818452332, 2, (8, 0), (), "Locale", None),
		"MacintoshFileTypes": (1836344948, 2, (12, 0), (), "MacintoshFileTypes", None),
		# Method 'MeasurementLog' returns object of type 'MeasurementLog'
		"MeasurementLog": (1296838707, 2, (9, 0), (), "MeasurementLog", '{84ADBF06-8354-4B5C-9CB1-EA2565B66C7C}'),
		"Name": (1886282093, 2, (8, 0), (), "Name", None),
		# Method 'Notifiers' returns object of type 'Notifiers'
		"Notifiers": (1162752050, 2, (9, 0), (), "Notifiers", '{861C9290-2A0C-4614-8606-706B31BFD45B}'),
		"NotifiersEnabled": (1162752054, 2, (11, 0), (), "NotifiersEnabled", None),
		"Path": (1884320872, 2, (8, 0), (), "Path", None),
		# Method 'Preferences' returns object of type 'Preferences'
		"Preferences": (1884320358, 2, (9, 0), (), "Preferences", '{288BC58E-AB6A-467C-B244-D225349E3EB3}'),
		"PreferencesFolder": (1886545516, 2, (8, 0), (), "PreferencesFolder", None),
		"RecentFiles": (1919116908, 2, (12, 0), (), "RecentFiles", None),
		"ScriptingBuildDate": (1935830116, 2, (8, 0), (), "ScriptingBuildDate", None),
		"ScriptingVersion": (1884518003, 2, (8, 0), (), "ScriptingVersion", None),
		"SystemInformation": (1400468297, 2, (8, 0), (), "SystemInformation", None),
		"Version": (1986359923, 2, (8, 0), (), "Version", None),
		"Visible": (1884640883, 2, (11, 0), (), "Visible", None),
		"WinColorSettings": (1129988973, 2, (8, 0), (), "WinColorSettings", None),
		"WindowsFileTypes": (2003723892, 2, (12, 0), (), "WindowsFileTypes", None),
	}
	_prop_map_put_ = {
		"ActiveDocument": ((1883325539, LCID, 4, 0),()),
		"BackgroundColor": ((1650934627, LCID, 4, 0),()),
		"DisplayDialogs": ((1096116556, LCID, 4, 0),()),
		"ForegroundColor": ((1718043491, LCID, 4, 0),()),
		"NotifiersEnabled": ((1162752054, LCID, 4, 0),()),
		"Visible": ((1884640883, LCID, 4, 0),()),
	}
	def __iter__(self):
		"Return a Python iterator for this object"
		try:
			ob = self._oleobj_.InvokeTypes(-4,LCID,3,(13, 10),())
		except pythoncom.error:
			raise TypeError("This object does not support enumeration")
		return win32com.client.util.Iterator(ob, None)

win32com.client.CLSIDToClass.RegisterCLSID( "{5DE90358-4D0B-4FA1-BA3E-C91BBA863F32}", _Application )
