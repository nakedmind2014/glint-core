# -*- coding: mbcs -*-
# Created by makepy.py version 0.5.01
# By python version 2.7.6 (default, Nov 10 2013, 19:24:24) [MSC v.1500 64 bit (AMD64)]
# From type library '{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}'
# On Fri Jun 19 00:27:56 2015
'Adobe Photoshop CS6 Object Library'
makepy_version = '0.5.01'
python_version = 0x20706f0

import win32com.client.CLSIDToClass, pythoncom, pywintypes
import win32com.client.util
from pywintypes import IID
from win32com.client import Dispatch

# The following 3 lines may need tweaking for the particular server
# Candidates are pythoncom.Missing, .Empty and .ArgNotFound
defaultNamedOptArg=pythoncom.Empty
defaultNamedNotOptArg=pythoncom.Empty
defaultUnnamedArg=pythoncom.Empty

CLSID = IID('{E891EE9A-D0AE-4CB4-8871-F92C0109F18E}')
MajorVersion = 1
MinorVersion = 0
LibraryFlags = 8
LCID = 0x0

class constants:
	psAbsolute                    =2          # from enum PsAdjustmentReference
	psRelative                    =1          # from enum PsAdjustmentReference
	psBottomCenter                =8          # from enum PsAnchorPosition
	psBottomLeft                  =7          # from enum PsAnchorPosition
	psBottomRight                 =9          # from enum PsAnchorPosition
	psMiddleCenter                =5          # from enum PsAnchorPosition
	psMiddleLeft                  =4          # from enum PsAnchorPosition
	psMiddleRight                 =6          # from enum PsAnchorPosition
	psTopCenter                   =2          # from enum PsAnchorPosition
	psTopLeft                     =1          # from enum PsAnchorPosition
	psTopRight                    =3          # from enum PsAnchorPosition
	psCrisp                       =3          # from enum PsAntiAlias
	psNoAntialias                 =1          # from enum PsAntiAlias
	psSharp                       =2          # from enum PsAntiAlias
	psSmooth                      =5          # from enum PsAntiAlias
	psStrong                      =4          # from enum PsAntiAlias
	psManual                      =1          # from enum PsAutoKernType
	psMetrics                     =2          # from enum PsAutoKernType
	psOptical                     =3          # from enum PsAutoKernType
	psBMP16Bits                   =16         # from enum PsBMPDepthType
	psBMP1Bit                     =1          # from enum PsBMPDepthType
	psBMP24Bits                   =24         # from enum PsBMPDepthType
	psBMP32Bits                   =32         # from enum PsBMPDepthType
	psBMP4Bits                    =4          # from enum PsBMPDepthType
	psBMP8Bits                    =8          # from enum PsBMPDepthType
	psBMP_A1R5G5B5                =61         # from enum PsBMPDepthType
	psBMP_A4R4G4B4                =64         # from enum PsBMPDepthType
	psBMP_A8R8G8B8                =67         # from enum PsBMPDepthType
	psBMP_R5G6B5                  =62         # from enum PsBMPDepthType
	psBMP_R8G8B8                  =65         # from enum PsBMPDepthType
	psBMP_X1R5G5B5                =60         # from enum PsBMPDepthType
	psBMP_X4R4G4B4                =63         # from enum PsBMPDepthType
	psBMP_X8R8G8B8                =66         # from enum PsBMPDepthType
	psFolder                      =3          # from enum PsBatchDestinationType
	psNoDestination               =1          # from enum PsBatchDestinationType
	psSaveAndClose                =2          # from enum PsBatchDestinationType
	psCustomPattern               =5          # from enum PsBitmapConversionType
	psDiffusionDither             =3          # from enum PsBitmapConversionType
	psHalfThreshold               =1          # from enum PsBitmapConversionType
	psHalftoneScreen              =4          # from enum PsBitmapConversionType
	psPatternDither               =2          # from enum PsBitmapConversionType
	psHalftoneCross               =6          # from enum PsBitmapHalfToneType
	psHalftoneDiamond             =2          # from enum PsBitmapHalfToneType
	psHalftoneEllipse             =3          # from enum PsBitmapHalfToneType
	psHalftoneLine                =4          # from enum PsBitmapHalfToneType
	psHalftoneRound               =1          # from enum PsBitmapHalfToneType
	psHalftoneSquare              =5          # from enum PsBitmapHalfToneType
	psDocument16Bits              =16         # from enum PsBitsPerChannelType
	psDocument1Bit                =1          # from enum PsBitsPerChannelType
	psDocument32Bits              =32         # from enum PsBitsPerChannelType
	psDocument8Bits               =8          # from enum PsBitsPerChannelType
	psColorBlend                  =22         # from enum PsBlendMode
	psColorBurn                   =6          # from enum PsBlendMode
	psColorDodge                  =10         # from enum PsBlendMode
	psDarken                      =4          # from enum PsBlendMode
	psDarkerColor                 =28         # from enum PsBlendMode
	psDifference                  =18         # from enum PsBlendMode
	psDissolve                    =3          # from enum PsBlendMode
	psDivide                      =30         # from enum PsBlendMode
	psExclusion                   =19         # from enum PsBlendMode
	psHardLight                   =14         # from enum PsBlendMode
	psHardMix                     =26         # from enum PsBlendMode
	psHue                         =20         # from enum PsBlendMode
	psLighten                     =8          # from enum PsBlendMode
	psLighterColor                =27         # from enum PsBlendMode
	psLinearBurn                  =7          # from enum PsBlendMode
	psLinearDodge                 =11         # from enum PsBlendMode
	psLinearLight                 =16         # from enum PsBlendMode
	psLuminosity                  =23         # from enum PsBlendMode
	psMultiply                    =5          # from enum PsBlendMode
	psNormalBlend                 =2          # from enum PsBlendMode
	psOverlay                     =12         # from enum PsBlendMode
	psPassThrough                 =1          # from enum PsBlendMode
	psPinLight                    =17         # from enum PsBlendMode
	psSaturationBlend             =21         # from enum PsBlendMode
	psScreen                      =9          # from enum PsBlendMode
	psSoftLight                   =13         # from enum PsBlendMode
	psSubtract                    =29         # from enum PsBlendMode
	psVividLight                  =15         # from enum PsBlendMode
	psIBMByteOrder                =1          # from enum PsByteOrderType
	psMacOSByteOrder              =2          # from enum PsByteOrderType
	psCameraDefault               =0          # from enum PsCameraRAWSettingsType
	psCustomSettings              =2          # from enum PsCameraRAWSettingsType
	psSelectedImage               =1          # from enum PsCameraRAWSettingsType
	psExtraLargeCameraRAW         =4          # from enum PsCameraRAWSize
	psLargeCameraRAW              =3          # from enum PsCameraRAWSize
	psMaximumCameraRAW            =5          # from enum PsCameraRAWSize
	psMediumCameraRAW             =2          # from enum PsCameraRAWSize
	psMinimumCameraRAW            =0          # from enum PsCameraRAWSize
	psSmallCameraRAW              =1          # from enum PsCameraRAWSize
	psAllCaps                     =2          # from enum PsCase
	psNormalCase                  =1          # from enum PsCase
	psSmallCaps                   =3          # from enum PsCase
	psConvertToBitmap             =5          # from enum PsChangeMode
	psConvertToCMYK               =3          # from enum PsChangeMode
	psConvertToGrayscale          =1          # from enum PsChangeMode
	psConvertToIndexedColor       =6          # from enum PsChangeMode
	psConvertToLab                =4          # from enum PsChangeMode
	psConvertToMultiChannel       =7          # from enum PsChangeMode
	psConvertToRGB                =2          # from enum PsChangeMode
	psComponentChannel            =1          # from enum PsChannelType
	psMaskedAreaAlphaChannel      =2          # from enum PsChannelType
	psSelectedAreaAlphaChannel    =3          # from enum PsChannelType
	psSpotColorChannel            =4          # from enum PsChannelType
	PsColorBlendMode              =22         # from enum PsColorBlendMode
	psBehindBlend                 =24         # from enum PsColorBlendMode
	psClearBlend                  =25         # from enum PsColorBlendMode
	psColorBurnBlend              =6          # from enum PsColorBlendMode
	psColorDodgeBlend             =10         # from enum PsColorBlendMode
	psDarkenBlend                 =4          # from enum PsColorBlendMode
	psDifferenceBlend             =18         # from enum PsColorBlendMode
	psDissolveBlend               =3          # from enum PsColorBlendMode
	psDivideBlend                 =28         # from enum PsColorBlendMode
	psExclusionBlend              =19         # from enum PsColorBlendMode
	psHardLightBlend              =14         # from enum PsColorBlendMode
	psHardMixBlend                =26         # from enum PsColorBlendMode
	psHueBlend                    =20         # from enum PsColorBlendMode
	psLightenBlend                =8          # from enum PsColorBlendMode
	psLinearBurnBlend             =7          # from enum PsColorBlendMode
	psLinearDodgeBlend            =11         # from enum PsColorBlendMode
	psLinearLightBlend            =16         # from enum PsColorBlendMode
	psLuminosityBlend             =23         # from enum PsColorBlendMode
	psMultiplyBlend               =5          # from enum PsColorBlendMode
	psNormalBlendColor            =2          # from enum PsColorBlendMode
	psOverlayBlend                =12         # from enum PsColorBlendMode
	psPinLightBlend               =17         # from enum PsColorBlendMode
	psSaturationBlendColor        =21         # from enum PsColorBlendMode
	psScreenBlend                 =9          # from enum PsColorBlendMode
	psSoftLightBlend              =13         # from enum PsColorBlendMode
	psSubtractBlend               =27         # from enum PsColorBlendMode
	psVividLightBlend             =15         # from enum PsColorBlendMode
	psCMYKModel                   =3          # from enum PsColorModel
	psGrayscaleModel              =1          # from enum PsColorModel
	psHSBModel                    =5          # from enum PsColorModel
	psLabModel                    =4          # from enum PsColorModel
	psNoModel                     =50         # from enum PsColorModel
	psRGBModel                    =2          # from enum PsColorModel
	psAdobeColorPicker            =1          # from enum PsColorPicker
	psAppleColorPicker            =2          # from enum PsColorPicker
	psPlugInColorPicker           =4          # from enum PsColorPicker
	psWindowsColorPicker          =3          # from enum PsColorPicker
	psCustom                      =3          # from enum PsColorProfileType
	psNo                          =1          # from enum PsColorProfileType
	psWorking                     =2          # from enum PsColorProfileType
	psAdaptive                    =2          # from enum PsColorReductionType
	psBlackWhiteReduction         =5          # from enum PsColorReductionType
	psCustomReduction             =4          # from enum PsColorReductionType
	psMacintoshColors             =7          # from enum PsColorReductionType
	psPerceptualReduction         =0          # from enum PsColorReductionType
	psRestrictive                 =3          # from enum PsColorReductionType
	psSFWGrayscale                =6          # from enum PsColorReductionType
	psSelective                   =1          # from enum PsColorReductionType
	psWindowsColors               =8          # from enum PsColorReductionType
	psAdobeRGB                    =0          # from enum PsColorSpaceType
	psColorMatchRGB               =1          # from enum PsColorSpaceType
	psProPhotoRGB                 =2          # from enum PsColorSpaceType
	psSRGB                        =3          # from enum PsColorSpaceType
	psCopyrightedWork             =1          # from enum PsCopyrightedType
	psPublicDomain                =2          # from enum PsCopyrightedType
	psUnmarked                    =3          # from enum PsCopyrightedType
	psDuplication                 =1          # from enum PsCreateFields
	psInterpolation               =2          # from enum PsCreateFields
	psArtBox                      =5          # from enum PsCropToType
	psBleedBox                    =3          # from enum PsCropToType
	psBoundingBox                 =0          # from enum PsCropToType
	psCropBox                     =2          # from enum PsCropToType
	psMediaBox                    =1          # from enum PsCropToType
	psTrimBox                     =4          # from enum PsCropToType
	psColorComposite              =3          # from enum PsDCSType
	psGrayscaleComposite          =2          # from enum PsDCSType
	psNoComposite                 =1          # from enum PsDCSType
	psImageHighlight              =4          # from enum PsDepthMapSource
	psLayerMask                   =3          # from enum PsDepthMapSource
	psNoSource                    =1          # from enum PsDepthMapSource
	psTransparencyChannel         =2          # from enum PsDepthMapSource
	psAliasType                   =11         # from enum PsDescValueType
	psBooleanType                 =5          # from enum PsDescValueType
	psClassType                   =10         # from enum PsDescValueType
	psDoubleType                  =2          # from enum PsDescValueType
	psEnumeratedType              =8          # from enum PsDescValueType
	psIntegerType                 =1          # from enum PsDescValueType
	psLargeIntegerType            =13         # from enum PsDescValueType
	psListType                    =6          # from enum PsDescValueType
	psObjectType                  =7          # from enum PsDescValueType
	psRawType                     =12         # from enum PsDescValueType
	psReferenceType               =9          # from enum PsDescValueType
	psStringType                  =4          # from enum PsDescValueType
	psUnitDoubleType              =3          # from enum PsDescValueType
	psDisplayAllDialogs           =1          # from enum PsDialogModes
	psDisplayErrorDialogs         =2          # from enum PsDialogModes
	psDisplayNoDialogs            =3          # from enum PsDialogModes
	psHorizontal                  =1          # from enum PsDirection
	psVertical                    =2          # from enum PsDirection
	psStretchToFit                =1          # from enum PsDisplacementMapType
	psTile                        =2          # from enum PsDisplacementMapType
	psDiffusion                   =2          # from enum PsDitherType
	psNoDither                    =1          # from enum PsDitherType
	psNoise                       =4          # from enum PsDitherType
	psPattern                     =3          # from enum PsDitherType
	psBackgroundColor             =2          # from enum PsDocumentFill
	psTransparent                 =3          # from enum PsDocumentFill
	psWhite                       =1          # from enum PsDocumentFill
	psBitmap                      =5          # from enum PsDocumentMode
	psCMYK                        =3          # from enum PsDocumentMode
	psDuotone                     =8          # from enum PsDocumentMode
	psGrayscale                   =1          # from enum PsDocumentMode
	psIndexedColor                =6          # from enum PsDocumentMode
	psLab                         =4          # from enum PsDocumentMode
	psMultiChannel                =7          # from enum PsDocumentMode
	psRGB                         =2          # from enum PsDocumentMode
	psConcise                     =2          # from enum PsEditLogItemsType
	psDetailed                    =3          # from enum PsEditLogItemsType
	psSessionOnly                 =1          # from enum PsEditLogItemsType
	psPlaceAfter                  =4          # from enum PsElementPlacement
	psPlaceAtBeginning            =1          # from enum PsElementPlacement
	psPlaceAtEnd                  =2          # from enum PsElementPlacement
	psPlaceBefore                 =3          # from enum PsElementPlacement
	psPlaceInside                 =0          # from enum PsElementPlacement
	psEvenFields                  =2          # from enum PsEliminateFields
	psOddFields                   =1          # from enum PsEliminateFields
	psIllustratorPaths            =1          # from enum PsExportType
	psSaveForWeb                  =2          # from enum PsExportType
	psLowercase                   =2          # from enum PsExtensionType
	psUppercase                   =3          # from enum PsExtensionType
	psDdmm                        =16         # from enum PsFileNamingType
	psDdmmyy                      =15         # from enum PsFileNamingType
	psDocumentNameLower           =2          # from enum PsFileNamingType
	psDocumentNameMixed           =1          # from enum PsFileNamingType
	psDocumentNameUpper           =3          # from enum PsFileNamingType
	psExtensionLower              =17         # from enum PsFileNamingType
	psExtensionUpper              =18         # from enum PsFileNamingType
	psMmdd                        =11         # from enum PsFileNamingType
	psMmddyy                      =10         # from enum PsFileNamingType
	psSerialLetterLower           =8          # from enum PsFileNamingType
	psSerialLetterUpper           =9          # from enum PsFileNamingType
	psSerialNumber1               =4          # from enum PsFileNamingType
	psSerialNumber2               =5          # from enum PsFileNamingType
	psSerialNumber3               =6          # from enum PsFileNamingType
	psSerialNumber4               =7          # from enum PsFileNamingType
	psYyddmm                      =14         # from enum PsFileNamingType
	psYymmdd                      =13         # from enum PsFileNamingType
	psYyyymmdd                    =12         # from enum PsFileNamingType
	psFontPreviewExtraLarge       =4          # from enum PsFontPreviewType
	psFontPreviewHuge             =5          # from enum PsFontPreviewType
	psFontPreviewLarge            =3          # from enum PsFontPreviewType
	psFontPreviewMedium           =2          # from enum PsFontPreviewType
	psFontPreviewNone             =0          # from enum PsFontPreviewType
	psFontPreviewSmall            =1          # from enum PsFontPreviewType
	psBlackWhite                  =2          # from enum PsForcedColors
	psNoForced                    =1          # from enum PsForcedColors
	psPrimaries                   =3          # from enum PsForcedColors
	psWeb                         =4          # from enum PsForcedColors
	psOptimizedBaseline           =2          # from enum PsFormatOptionsType
	psProgressive                 =3          # from enum PsFormatOptionsType
	psStandardBaseline            =1          # from enum PsFormatOptionsType
	psConstrainBoth               =3          # from enum PsGalleryConstrainType
	psConstrainHeight             =2          # from enum PsGalleryConstrainType
	psConstrainWidth              =1          # from enum PsGalleryConstrainType
	psArial                       =1          # from enum PsGalleryFontType
	psCourierNew                  =2          # from enum PsGalleryFontType
	psHelvetica                   =3          # from enum PsGalleryFontType
	psTimesNewRoman               =4          # from enum PsGalleryFontType
	psBlackText                   =1          # from enum PsGallerySecurityTextColorType
	psCustomText                  =3          # from enum PsGallerySecurityTextColorType
	psWhiteText                   =2          # from enum PsGallerySecurityTextColorType
	psCentered                    =1          # from enum PsGallerySecurityTextPositionType
	psLowerLeft                   =3          # from enum PsGallerySecurityTextPositionType
	psLowerRight                  =5          # from enum PsGallerySecurityTextPositionType
	psUpperLeft                   =2          # from enum PsGallerySecurityTextPositionType
	psUpperRight                  =4          # from enum PsGallerySecurityTextPositionType
	psClockwise45                 =2          # from enum PsGallerySecurityTextRotateType
	psClockwise90                 =3          # from enum PsGallerySecurityTextRotateType
	psCounterClockwise45          =4          # from enum PsGallerySecurityTextRotateType
	psCounterClockwise90          =5          # from enum PsGallerySecurityTextRotateType
	psZero                        =1          # from enum PsGallerySecurityTextRotateType
	psCaption                     =5          # from enum PsGallerySecurityType
	psCopyright                   =4          # from enum PsGallerySecurityType
	psCredit                      =6          # from enum PsGallerySecurityType
	psCustomSecurityText          =2          # from enum PsGallerySecurityType
	psFilename                    =3          # from enum PsGallerySecurityType
	psNoSecurity                  =1          # from enum PsGallerySecurityType
	psTitle                       =7          # from enum PsGallerySecurityType
	psCustomThumbnail             =4          # from enum PsGalleryThumbSizeType
	psLarge                       =3          # from enum PsGalleryThumbSizeType
	psMedium                      =2          # from enum PsGalleryThumbSizeType
	psSmall                       =1          # from enum PsGalleryThumbSizeType
	psHeptagon                    =4          # from enum PsGeometry
	psHexagon                     =2          # from enum PsGeometry
	psOctagon                     =5          # from enum PsGeometry
	psPentagon                    =1          # from enum PsGeometry
	psSquareGeometry              =3          # from enum PsGeometry
	psTriangle                    =0          # from enum PsGeometry
	psGridDashedLine              =2          # from enum PsGridLineStyle
	psGridDottedLine              =3          # from enum PsGridLineStyle
	psGridSolidLine               =1          # from enum PsGridLineStyle
	psLargeGrid                   =4          # from enum PsGridSize
	psMediumGrid                  =3          # from enum PsGridSize
	psNoGrid                      =1          # from enum PsGridSize
	psSmallGrid                   =2          # from enum PsGridSize
	psGuideDashedLine             =2          # from enum PsGuideLineStyle
	psGuideSolidLine              =1          # from enum PsGuideLineStyle
	psAllPaths                    =2          # from enum PsIllustratorPathType
	psDocumentBounds              =1          # from enum PsIllustratorPathType
	psNamedPath                   =3          # from enum PsIllustratorPathType
	psAbsoluteColorimetric        =4          # from enum PsIntent
	psPerceptual                  =1          # from enum PsIntent
	psRelativeColorimetric        =3          # from enum PsIntent
	psSaturation                  =2          # from enum PsIntent
	psBeforeRunning               =3          # from enum PsJavaScriptExecutionMode
	psDebuggerOnError             =2          # from enum PsJavaScriptExecutionMode
	psNeverShowDebugger           =1          # from enum PsJavaScriptExecutionMode
	psCenter                      =2          # from enum PsJustification
	psCenterJustified             =5          # from enum PsJustification
	psFullyJustified              =7          # from enum PsJustification
	psLeft                        =1          # from enum PsJustification
	psLeftJustified               =4          # from enum PsJustification
	psRight                       =3          # from enum PsJustification
	psRightJustified              =6          # from enum PsJustification
	psBrazillianPortuguese        =13         # from enum PsLanguage
	psCanadianFrench              =4          # from enum PsLanguage
	psDanish                      =17         # from enum PsLanguage
	psDutch                       =16         # from enum PsLanguage
	psEnglishUK                   =2          # from enum PsLanguage
	psEnglishUSA                  =1          # from enum PsLanguage
	psFinnish                     =5          # from enum PsLanguage
	psFrench                      =3          # from enum PsLanguage
	psGerman                      =6          # from enum PsLanguage
	psItalian                     =9          # from enum PsLanguage
	psNorwegian                   =10         # from enum PsLanguage
	psNynorskNorwegian            =11         # from enum PsLanguage
	psOldGerman                   =7          # from enum PsLanguage
	psPortuguese                  =12         # from enum PsLanguage
	psSpanish                     =14         # from enum PsLanguage
	psSwedish                     =15         # from enum PsLanguage
	psSwissGerman                 =8          # from enum PsLanguage
	psRLELayerCompression         =1          # from enum PsLayerCompressionType
	psZIPLayerCompression         =2          # from enum PsLayerCompressionType
	psBlackAndWhiteLayer          =22         # from enum PsLayerKind
	psBrightnessContrastLayer     =9          # from enum PsLayerKind
	psChannelMixerLayer           =12         # from enum PsLayerKind
	psColorBalanceLayer           =8          # from enum PsLayerKind
	psCurvesLayer                 =7          # from enum PsLayerKind
	psExposureLayer               =19         # from enum PsLayerKind
	psGradientFillLayer           =4          # from enum PsLayerKind
	psGradientMapLayer            =13         # from enum PsLayerKind
	psHueSaturationLayer          =10         # from enum PsLayerKind
	psInversionLayer              =14         # from enum PsLayerKind
	psLayer3D                     =20         # from enum PsLayerKind
	psLevelsLayer                 =6          # from enum PsLayerKind
	psNormalLayer                 =1          # from enum PsLayerKind
	psPatternFillLayer            =5          # from enum PsLayerKind
	psPhotoFilterLayer            =18         # from enum PsLayerKind
	psPosterizeLayer              =16         # from enum PsLayerKind
	psSelectiveColorLayer         =11         # from enum PsLayerKind
	psSmartObjectLayer            =17         # from enum PsLayerKind
	psSolidFillLayer              =3          # from enum PsLayerKind
	psTextLayer                   =2          # from enum PsLayerKind
	psThresholdLayer              =15         # from enum PsLayerKind
	psVibrance                    =23         # from enum PsLayerKind
	psVideoLayer                  =21         # from enum PsLayerKind
	psArtLayer                    =1          # from enum PsLayerType
	psLayerSet                    =2          # from enum PsLayerType
	psMoviePrime                  =5          # from enum PsLensType
	psPrime105                    =3          # from enum PsLensType
	psPrime35                     =2          # from enum PsLensType
	psZoomLens                    =1          # from enum PsLensType
	psActualSize                  =0          # from enum PsMagnificationType
	psFitPage                     =1          # from enum PsMagnificationType
	psBackgroundColorMatte        =3          # from enum PsMatteType
	psBlackMatte                  =5          # from enum PsMatteType
	psForegroundColorMatte        =2          # from enum PsMatteType
	psNetscapeGrayMatte           =7          # from enum PsMatteType
	psNoMatte                     =1          # from enum PsMatteType
	psSemiGray                    =6          # from enum PsMatteType
	psWhiteMatte                  =4          # from enum PsMatteType
	psActiveMeasurements          =2          # from enum PsMeasurementRange
	psAllMeasurements             =1          # from enum PsMeasurementRange
	psMeasureCountTool            =2          # from enum PsMeasurementSource
	psMeasureRulerTool            =3          # from enum PsMeasurementSource
	psMeasureSelection            =1          # from enum PsMeasurementSource
	psNewBitmap                   =5          # from enum PsNewDocumentMode
	psNewCMYK                     =3          # from enum PsNewDocumentMode
	psNewGray                     =1          # from enum PsNewDocumentMode
	psNewLab                      =4          # from enum PsNewDocumentMode
	psNewRGB                      =2          # from enum PsNewDocumentMode
	psGaussianNoise               =2          # from enum PsNoiseDistribution
	psUniformNoise                =1          # from enum PsNoiseDistribution
	psOffsetRepeatEdgePixels      =3          # from enum PsOffsetUndefinedAreas
	psOffsetSetToLayerFill        =1          # from enum PsOffsetUndefinedAreas
	psOffsetWrapAround            =2          # from enum PsOffsetUndefinedAreas
	psOpenCMYK                    =3          # from enum PsOpenDocumentMode
	psOpenGray                    =1          # from enum PsOpenDocumentMode
	psOpenLab                     =4          # from enum PsOpenDocumentMode
	psOpenRGB                     =2          # from enum PsOpenDocumentMode
	psAliasPIXOpen                =25         # from enum PsOpenDocumentType
	psBMPOpen                     =2          # from enum PsOpenDocumentType
	psCameraRAWOpen               =32         # from enum PsOpenDocumentType
	psCompuServeGIFOpen           =3          # from enum PsOpenDocumentType
	psDICOMOpen                   =33         # from enum PsOpenDocumentType
	psEPSOpen                     =22         # from enum PsOpenDocumentType
	psEPSPICTPreviewOpen          =23         # from enum PsOpenDocumentType
	psEPSTIFFPreviewOpen          =24         # from enum PsOpenDocumentType
	psElectricImageOpen           =26         # from enum PsOpenDocumentType
	psFilmstripOpen               =5          # from enum PsOpenDocumentType
	psJPEGOpen                    =6          # from enum PsOpenDocumentType
	psPCXOpen                     =7          # from enum PsOpenDocumentType
	psPDFOpen                     =21         # from enum PsOpenDocumentType
	psPICTFileFormatOpen          =10         # from enum PsOpenDocumentType
	psPICTResourceFormatOpen      =11         # from enum PsOpenDocumentType
	psPNGOpen                     =13         # from enum PsOpenDocumentType
	psPhotoCDOpen                 =9          # from enum PsOpenDocumentType
	psPhotoshopDCS_1Open          =18         # from enum PsOpenDocumentType
	psPhotoshopDCS_2Open          =19         # from enum PsOpenDocumentType
	psPhotoshopEPSOpen            =4          # from enum PsOpenDocumentType
	psPhotoshopOpen               =1          # from enum PsOpenDocumentType
	psPhotoshopPDFOpen            =8          # from enum PsOpenDocumentType
	psPixarOpen                   =12         # from enum PsOpenDocumentType
	psPortableBitmapOpen          =27         # from enum PsOpenDocumentType
	psRawOpen                     =14         # from enum PsOpenDocumentType
	psSGIRGBOpen                  =29         # from enum PsOpenDocumentType
	psScitexCTOpen                =15         # from enum PsOpenDocumentType
	psSoftImageOpen               =30         # from enum PsOpenDocumentType
	psTIFFOpen                    =17         # from enum PsOpenDocumentType
	psTargaOpen                   =16         # from enum PsOpenDocumentType
	psWavefrontRLAOpen            =28         # from enum PsOpenDocumentType
	psWirelessBitmapOpen          =31         # from enum PsOpenDocumentType
	psOS2                         =1          # from enum PsOperatingSystem
	psWindows                     =2          # from enum PsOperatingSystem
	psLandscape                   =1          # from enum PsOrientation
	psPortrait                    =2          # from enum PsOrientation
	psPreciseOther                =2          # from enum PsOtherPaintingCursors
	psStandardOther               =1          # from enum PsOtherPaintingCursors
	psPDF13                       =1          # from enum PsPDFCompatibilityType
	psPDF14                       =2          # from enum PsPDFCompatibilityType
	psPDF15                       =3          # from enum PsPDFCompatibilityType
	psPDF16                       =4          # from enum PsPDFCompatibilityType
	psPDF17                       =5          # from enum PsPDFCompatibilityType
	psPDFJPEG                     =2          # from enum PsPDFEncodingType
	psPDFJPEG2000HIGH             =9          # from enum PsPDFEncodingType
	psPDFJPEG2000LOSSLESS         =14         # from enum PsPDFEncodingType
	psPDFJPEG2000LOW              =13         # from enum PsPDFEncodingType
	psPDFJPEG2000MED              =11         # from enum PsPDFEncodingType
	psPDFJPEG2000MEDHIGH          =10         # from enum PsPDFEncodingType
	psPDFJPEG2000MEDLOW           =12         # from enum PsPDFEncodingType
	psPDFJPEGHIGH                 =4          # from enum PsPDFEncodingType
	psPDFJPEGLOW                  =8          # from enum PsPDFEncodingType
	psPDFJPEGMED                  =6          # from enum PsPDFEncodingType
	psPDFJPEGMEDHIGH              =5          # from enum PsPDFEncodingType
	psPDFJPEGMEDLOW               =7          # from enum PsPDFEncodingType
	psPDFNone                     =0          # from enum PsPDFEncodingType
	psPDFZip                      =1          # from enum PsPDFEncodingType
	psPDFZip4Bit                  =3          # from enum PsPDFEncodingType
	psNoResample                  =0          # from enum PsPDFResampleType
	psPDFAverage                  =1          # from enum PsPDFResampleType
	psPDFBicubic                  =3          # from enum PsPDFResampleType
	psPDFSubSample                =2          # from enum PsPDFResampleType
	psNoStandard                  =0          # from enum PsPDFStandardType
	psPDFX1A2001                  =1          # from enum PsPDFStandardType
	psPDFX1A2003                  =2          # from enum PsPDFStandardType
	psPDFX32002                   =3          # from enum PsPDFStandardType
	psPDFX32003                   =4          # from enum PsPDFStandardType
	psPDFX42008                   =5          # from enum PsPDFStandardType
	psPICT16Bits                  =16         # from enum PsPICTBitsPerPixels
	psPICT2Bits                   =2          # from enum PsPICTBitsPerPixels
	psPICT32Bits                  =32         # from enum PsPICTBitsPerPixels
	psPICT4Bits                   =4          # from enum PsPICTBitsPerPixels
	psPICT8Bits                   =8          # from enum PsPICTBitsPerPixels
	psJPEGHighPICT                =5          # from enum PsPICTCompression
	psJPEGLowPICT                 =2          # from enum PsPICTCompression
	psJPEGMaximumPICT             =6          # from enum PsPICTCompression
	psJPEGMediumPICT              =4          # from enum PsPICTCompression
	psNoPICTCompression           =1          # from enum PsPICTCompression
	psBrushSize                   =3          # from enum PsPaintingCursors
	psPrecise                     =2          # from enum PsPaintingCursors
	psStandard                    =1          # from enum PsPaintingCursors
	psExact                       =1          # from enum PsPaletteType
	psLocalAdaptive               =8          # from enum PsPaletteType
	psLocalPerceptual             =6          # from enum PsPaletteType
	psLocalSelective              =7          # from enum PsPaletteType
	psMacOSPalette                =2          # from enum PsPaletteType
	psMasterAdaptive              =11         # from enum PsPaletteType
	psMasterPerceptual            =9          # from enum PsPaletteType
	psMasterSelective             =10         # from enum PsPaletteType
	psPreviousPalette             =12         # from enum PsPaletteType
	psUniform                     =5          # from enum PsPaletteType
	psWebPalette                  =4          # from enum PsPaletteType
	psWindowsPalette              =3          # from enum PsPaletteType
	psClippingPath                =2          # from enum PsPathKind
	psNormalPath                  =1          # from enum PsPathKind
	psTextMask                    =5          # from enum PsPathKind
	psVectorMask                  =4          # from enum PsPathKind
	psWorkPath                    =3          # from enum PsPathKind
	psLab16                       =4          # from enum PsPhotoCDColorSpace
	psLab8                        =3          # from enum PsPhotoCDColorSpace
	psRGB16                       =2          # from enum PsPhotoCDColorSpace
	psRGB8                        =1          # from enum PsPhotoCDColorSpace
	psExtraLargePhotoCD           =5          # from enum PsPhotoCDSize
	psLargePhotoCD                =4          # from enum PsPhotoCDSize
	psMaximumPhotoCD              =6          # from enum PsPhotoCDSize
	psMediumPhotoCD               =3          # from enum PsPhotoCDSize
	psMinimumPhotoCD              =1          # from enum PsPhotoCDSize
	psSmallPhotoCD                =2          # from enum PsPhotoCDSize
	psCaptionText                 =5          # from enum PsPicturePackageTextType
	psCopyrightText               =4          # from enum PsPicturePackageTextType
	psCreditText                  =6          # from enum PsPicturePackageTextType
	psFilenameText                =3          # from enum PsPicturePackageTextType
	psNoText                      =1          # from enum PsPicturePackageTextType
	psOriginText                  =7          # from enum PsPicturePackageTextType
	psUserText                    =2          # from enum PsPicturePackageTextType
	psCornerPoint                 =2          # from enum PsPointKind
	psSmoothPoint                 =1          # from enum PsPointKind
	psPostScriptPoints            =1          # from enum PsPointType
	psTraditionalPoints           =2          # from enum PsPointType
	psPolarToRectangular          =2          # from enum PsPolarConversionType
	psRectangularToPolar          =1          # from enum PsPolarConversionType
	psEightBitTIFF                =3          # from enum PsPreviewType
	psMonochromeTIFF              =2          # from enum PsPreviewType
	psNoPreview                   =1          # from enum PsPreviewType
	psAllCaches                   =4          # from enum PsPurgeTarget
	psClipboardCache              =3          # from enum PsPurgeTarget
	psHistoryCaches               =2          # from enum PsPurgeTarget
	psUndoCaches                  =1          # from enum PsPurgeTarget
	psAlways                      =1          # from enum PsQueryStateType
	psAsk                         =2          # from enum PsQueryStateType
	psNever                       =3          # from enum PsQueryStateType
	psSpin                        =1          # from enum PsRadialBlurMethod
	psZoom                        =2          # from enum PsRadialBlurMethod
	psRadialBlurBest              =3          # from enum PsRadialBlurQuality
	psRadialBlurDraft             =1          # from enum PsRadialBlurQuality
	psRadialBlurGood              =2          # from enum PsRadialBlurQuality
	psEntireLayer                 =5          # from enum PsRasterizeType
	psFillContent                 =3          # from enum PsRasterizeType
	psLayerClippingPath           =4          # from enum PsRasterizeType
	psLinkedLayers                =6          # from enum PsRasterizeType
	psShape                       =2          # from enum PsRasterizeType
	psTextContents                =1          # from enum PsRasterizeType
	psReferenceClassType          =7          # from enum PsReferenceFormType
	psReferenceEnumeratedType     =5          # from enum PsReferenceFormType
	psReferenceIdentifierType     =3          # from enum PsReferenceFormType
	psReferenceIndexType          =2          # from enum PsReferenceFormType
	psReferenceNameType           =1          # from enum PsReferenceFormType
	psReferenceOffsetType         =4          # from enum PsReferenceFormType
	psReferencePropertyType       =6          # from enum PsReferenceFormType
	psBicubic                     =4          # from enum PsResampleMethod
	psBicubicSharper              =5          # from enum PsResampleMethod
	psBicubicSmoother             =6          # from enum PsResampleMethod
	psBilinear                    =3          # from enum PsResampleMethod
	psNearestNeighbor             =2          # from enum PsResampleMethod
	psNoResampling                =1          # from enum PsResampleMethod
	psAllTools                    =2          # from enum PsResetTarget
	psAllWarnings                 =1          # from enum PsResetTarget
	psEverything                  =3          # from enum PsResetTarget
	psLargeRipple                 =3          # from enum PsRippleSize
	psMediumRipple                =2          # from enum PsRippleSize
	psSmallRipple                 =1          # from enum PsRippleSize
	psAlwaysSave                  =2          # from enum PsSaveBehavior
	psAskWhenSaving               =3          # from enum PsSaveBehavior
	psNeverSave                   =1          # from enum PsSaveBehavior
	psAliasPIXSave                =25         # from enum PsSaveDocumentType
	psBMPSave                     =2          # from enum PsSaveDocumentType
	psCompuServeGIFSave           =3          # from enum PsSaveDocumentType
	psElectricImageSave           =26         # from enum PsSaveDocumentType
	psJPEGSave                    =6          # from enum PsSaveDocumentType
	psPCXSave                     =7          # from enum PsSaveDocumentType
	psPICTFileFormatSave          =10         # from enum PsSaveDocumentType
	psPICTResourceFormatSave      =11         # from enum PsSaveDocumentType
	psPNGSave                     =13         # from enum PsSaveDocumentType
	psPhotoshopDCS_1Save          =18         # from enum PsSaveDocumentType
	psPhotoshopDCS_2Save          =19         # from enum PsSaveDocumentType
	psPhotoshopEPSSave            =4          # from enum PsSaveDocumentType
	psPhotoshopPDFSave            =8          # from enum PsSaveDocumentType
	psPhotoshopSave               =1          # from enum PsSaveDocumentType
	psPixarSave                   =12         # from enum PsSaveDocumentType
	psPortableBitmapSave          =27         # from enum PsSaveDocumentType
	psRawSave                     =14         # from enum PsSaveDocumentType
	psSGIRGBSave                  =29         # from enum PsSaveDocumentType
	psScitexCTSave                =15         # from enum PsSaveDocumentType
	psSoftImageSave               =30         # from enum PsSaveDocumentType
	psTIFFSave                    =17         # from enum PsSaveDocumentType
	psTargaSave                   =16         # from enum PsSaveDocumentType
	psWavefrontRLASave            =28         # from enum PsSaveDocumentType
	psWirelessBitmapSave          =31         # from enum PsSaveDocumentType
	psAscii                       =3          # from enum PsSaveEncoding
	psBinary                      =1          # from enum PsSaveEncoding
	psJPEGHigh                    =5          # from enum PsSaveEncoding
	psJPEGLow                     =2          # from enum PsSaveEncoding
	psJPEGMaximum                 =6          # from enum PsSaveEncoding
	psJPEGMedium                  =4          # from enum PsSaveEncoding
	psLogFile                     =2          # from enum PsSaveLogItemsType
	psLogFileAndMetadata          =3          # from enum PsSaveLogItemsType
	psMetadata                    =1          # from enum PsSaveLogItemsType
	psDoNotSaveChanges            =2          # from enum PsSaveOptions
	psPromptToSaveChanges         =3          # from enum PsSaveOptions
	psSaveChanges                 =1          # from enum PsSaveOptions
	psDiminishSelection           =3          # from enum PsSelectionType
	psExtendSelection             =2          # from enum PsSelectionType
	psIntersectSelection          =4          # from enum PsSelectionType
	psReplaceSelection            =1          # from enum PsSelectionType
	psShapeAdd                    =1          # from enum PsShapeOperation
	psShapeIntersect              =3          # from enum PsShapeOperation
	psShapeSubtract               =4          # from enum PsShapeOperation
	psShapeXOR                    =2          # from enum PsShapeOperation
	psSmartBlurEdgeOnly           =2          # from enum PsSmartBlurMode
	psSmartBlurNormal             =1          # from enum PsSmartBlurMode
	psSmartBlurOverlayEdge        =3          # from enum PsSmartBlurMode
	psSmartBlurHigh               =3          # from enum PsSmartBlurQuality
	psSmartBlurLow                =1          # from enum PsSmartBlurQuality
	psSmartBlurMedium             =2          # from enum PsSmartBlurQuality
	psDocumentSpace               =1          # from enum PsSourceSpaceType
	psProofSpace                  =2          # from enum PsSourceSpaceType
	psHorizontalSpherize          =2          # from enum PsSpherizeMode
	psNormalSpherize              =1          # from enum PsSpherizeMode
	psVerticalSpherize            =3          # from enum PsSpherizeMode
	psStrikeBox                   =3          # from enum PsStrikeThruType
	psStrikeHeight                =2          # from enum PsStrikeThruType
	psStrikeOff                   =1          # from enum PsStrikeThruType
	psCenterStroke                =2          # from enum PsStrokeLocation
	psInsideStroke                =1          # from enum PsStrokeLocation
	psOutsideStroke               =3          # from enum PsStrokeLocation
	psTarga16Bits                 =16         # from enum PsTargaBitsPerPixels
	psTarga24Bits                 =24         # from enum PsTargaBitsPerPixels
	psTarga32Bits                 =32         # from enum PsTargaBitsPerPixels
	psAdobeEveryLine              =2          # from enum PsTextComposer
	psAdobeSingleLine             =1          # from enum PsTextComposer
	psParagraphText               =2          # from enum PsTextType
	psPointText                   =1          # from enum PsTextType
	psBlocksTexture               =1          # from enum PsTextureType
	psCanvasTexture               =2          # from enum PsTextureType
	psFrostedTexture              =3          # from enum PsTextureType
	psTextureFile                 =5          # from enum PsTextureType
	psTinyLensTexture             =4          # from enum PsTextureType
	psNoTIFFCompression           =1          # from enum PsTiffEncodingType
	psTiffJPEG                    =3          # from enum PsTiffEncodingType
	psTiffLZW                     =2          # from enum PsTiffEncodingType
	psTiffZIP                     =4          # from enum PsTiffEncodingType
	psArtHistoryBrush             =9          # from enum PsToolType
	psBackgroundEraser            =4          # from enum PsToolType
	psBlur                        =11         # from enum PsToolType
	psBrush                       =2          # from enum PsToolType
	psBurn                        =14         # from enum PsToolType
	psCloneStamp                  =5          # from enum PsToolType
	psColorReplacementTool        =16         # from enum PsToolType
	psDodge                       =13         # from enum PsToolType
	psEraser                      =3          # from enum PsToolType
	psHealingBrush                =7          # from enum PsToolType
	psHistoryBrush                =8          # from enum PsToolType
	psPatternStamp                =6          # from enum PsToolType
	psPencil                      =1          # from enum PsToolType
	psSharpen                     =12         # from enum PsToolType
	psSmudge                      =10         # from enum PsToolType
	psSponge                      =15         # from enum PsToolType
	psBlindsHorizontal            =1          # from enum PsTransitionType
	psBlindsVertical              =2          # from enum PsTransitionType
	psBoxIn                       =4          # from enum PsTransitionType
	psBoxOut                      =5          # from enum PsTransitionType
	psDissolveTransition          =3          # from enum PsTransitionType
	psGlitterDown                 =6          # from enum PsTransitionType
	psGlitterRight                =7          # from enum PsTransitionType
	psGlitterRightDown            =8          # from enum PsTransitionType
	psNoTrasition                 =9          # from enum PsTransitionType
	psRandom                      =10         # from enum PsTransitionType
	psSplitHorizontalIn           =11         # from enum PsTransitionType
	psSplitHorizontalOut          =12         # from enum PsTransitionType
	psSplitVerticalIn             =13         # from enum PsTransitionType
	psSplitVerticalOut            =14         # from enum PsTransitionType
	psWipeDown                    =15         # from enum PsTransitionType
	psWipeLeft                    =16         # from enum PsTransitionType
	psWipeRight                   =17         # from enum PsTransitionType
	psWipeUp                      =18         # from enum PsTransitionType
	psBottomRightPixel            =9          # from enum PsTrimType
	psTopLeftPixel                =1          # from enum PsTrimType
	psTransparentPixels           =0          # from enum PsTrimType
	psTypeMM                      =4          # from enum PsTypeUnits
	psTypePixels                  =1          # from enum PsTypeUnits
	psTypePoints                  =5          # from enum PsTypeUnits
	psRepeatEdgePixels            =2          # from enum PsUndefinedAreas
	psWrapAround                  =1          # from enum PsUndefinedAreas
	psUnderlineLeft               =3          # from enum PsUnderlineType
	psUnderlineOff                =1          # from enum PsUnderlineType
	psUnderlineRight              =2          # from enum PsUnderlineType
	psCM                          =3          # from enum PsUnits
	psInches                      =2          # from enum PsUnits
	psMM                          =4          # from enum PsUnits
	psPercent                     =7          # from enum PsUnits
	psPicas                       =6          # from enum PsUnits
	psPixels                      =1          # from enum PsUnits
	psPoints                      =5          # from enum PsUnits
	psFour                        =4          # from enum PsUrgency
	psHigh                        =8          # from enum PsUrgency
	psLow                         =1          # from enum PsUrgency
	psNone                        =0          # from enum PsUrgency
	psNormal                      =5          # from enum PsUrgency
	psSeven                       =7          # from enum PsUrgency
	psSix                         =6          # from enum PsUrgency
	psThree                       =3          # from enum PsUrgency
	psTwo                         =2          # from enum PsUrgency
	psArc                         =2          # from enum PsWarpStyle
	psArcLower                    =3          # from enum PsWarpStyle
	psArcUpper                    =4          # from enum PsWarpStyle
	psArch                        =5          # from enum PsWarpStyle
	psBulge                       =6          # from enum PsWarpStyle
	psFish                        =11         # from enum PsWarpStyle
	psFishEye                     =13         # from enum PsWarpStyle
	psFlag                        =9          # from enum PsWarpStyle
	psInflate                     =14         # from enum PsWarpStyle
	psNoWarp                      =1          # from enum PsWarpStyle
	psRise                        =12         # from enum PsWarpStyle
	psShellLower                  =7          # from enum PsWarpStyle
	psShellUpper                  =8          # from enum PsWarpStyle
	psSqueeze                     =15         # from enum PsWarpStyle
	psTwist                       =16         # from enum PsWarpStyle
	psWave                        =10         # from enum PsWarpStyle
	psSine                        =1          # from enum PsWaveType
	psSquare                      =3          # from enum PsWaveType
	psTriangular                  =2          # from enum PsWaveType
	psAsShot                      =0          # from enum PsWhiteBalanceType
	psAuto                        =1          # from enum PsWhiteBalanceType
	psCloudy                      =3          # from enum PsWhiteBalanceType
	psCustomCameraSettings        =8          # from enum PsWhiteBalanceType
	psDaylight                    =2          # from enum PsWhiteBalanceType
	psFlash                       =7          # from enum PsWhiteBalanceType
	psFluorescent                 =6          # from enum PsWhiteBalanceType
	psShade                       =4          # from enum PsWhiteBalanceType
	psTungsten                    =5          # from enum PsWhiteBalanceType
	psAroundCenter                =1          # from enum PsZigZagType
	psOutFromCenter               =2          # from enum PsZigZagType
	psPondRipples                 =3          # from enum PsZigZagType

RecordMap = {
}

CLSIDToClassMap = {}
CLSIDToPackageMap = {
	'{50D0174F-484D-4A2B-8BF0-A21B84167D82}' : u'_PDFOpenOptions',
	'{12B598B3-499F-4789-B6A4-900F0125E6EB}' : u'PNGSaveOptions',
	'{7D14BA29-1672-482F-8F48-9DA1E94800FD}' : u'PathPoint',
	'{A9701D4D-73D2-4547-9CA9-3E655C9D8327}' : u'ExportOptionsIllustrator',
	'{9A37A0AC-E951-4B16-A548-886B77338DE0}' : u'LayerComp',
	'{B6D22EB9-EC6D-46DB-B52A-5561433A1217}' : u'SubPathItem',
	'{D334A509-00F8-4092-A9AF-6E1176D06536}' : u'_PICTFileSaveOptions',
	'{1F7A215C-382B-4973-8093-BC1090A0107B}' : u'HSBColor',
	'{99AEA70E-4D13-44BF-A878-33345CBFCBC8}' : u'Application',
	'{F4D7F5C2-37DB-4DF7-8A7D-528902056596}' : u'_LabColor',
	'{726B458C-74B0-47AE-B390-99753B55DF2E}' : u'LayerComps',
	'{177E3CA8-73EA-4D4E-A6F0-E2AEF336F273}' : u'PathPointInfo',
	'{B3C35001-B625-48D7-9D3B-C9D66D9CF5F1}' : u'_PathPointInfo',
	'{1AD38EEB-C67D-4FAC-BDF4-779F0CFC85AB}' : u'GalleryImagesOptions',
	'{B7283EEC-23B1-49A6-B151-0E97E4AF353C}' : u'SubPathItems',
	'{FBBC7D1E-78AB-432D-970A-DAAEA9AA1C4D}' : u'PhotoshopSaveOptions',
	'{632F36B3-1D76-48BE-ADC3-D7FB62A0C2FB}' : u'MeasurementScale',
	'{F4E21694-AEBF-44FB-90AB-EECD58C1B6F3}' : u'_TargaSaveOptions',
	'{3407D9A0-A820-4BE5-8E28-410D677488AA}' : u'BitmapConversionOptions',
	'{A5F25DBB-333F-4397-8D33-99E7D9B84E1C}' : u'GIFSaveOptions',
	'{9E6AAF3B-3CD1-47FD-A80A-EBB7ADBC53AD}' : u'PresentationOptions',
	'{A2DFD5E5-3983-4121-AC5D-E51E392F4DFF}' : u'BMPSaveOptions',
	'{8B0CB532-4ACC-4BF3-9E42-0949B679D120}' : u'PathItem',
	'{861C9290-2A0C-4614-8606-706B31BFD45B}' : u'Notifiers',
	'{13C5C35E-B099-4275-80B6-CF17DE8F2336}' : u'GrayColor',
	'{2C7F9B6C-0676-4ACC-91B9-53BBE3D4F5ED}' : u'DCS2_SaveOptions',
	'{84ADBF06-8354-4B5C-9CB1-EA2565B66C7C}' : u'MeasurementLog',
	'{2C253307-8CD7-4747-AD28-64D9D88FE2D9}' : u'ActionReference',
	'{E595896F-0407-4659-994F-19235FFB4D97}' : u'CMYKColor',
	'{746FEF90-A182-4BD0-A4F6-BB6BBAE87A78}' : u'DocumentInfo',
	'{D8CDC179-C3D5-4EED-A042-938683C29355}' : u'IndexedConversionOptions',
	'{DC865034-A587-4CC4-8A5A-453032562BE4}' : u'XMPMetadata',
	'{EE8364D9-B811-4C7D-A3A8-97C4EBFAB83A}' : u'_DICOMOpenOptions',
	'{EC6A366C-F901-488D-A2C3-9E2E78B72DC6}' : u'ArtLayers',
	'{643099A1-0B67-4920-9B14-E14BE8F63D5F}' : u'_BitmapConversionOptions',
	'{FC08B435-5F19-49DF-ABE7-ADCE9F0729FF}' : u'_ExportOptionsIllustrator',
	'{D74B820F-AA86-42DD-8D85-F4D67A62F200}' : u'_RawSaveOptions',
	'{22D0B851-E811-40E2-9A79-E84EA602C9F1}' : u'_IndexedConversionOptions',
	'{2DC64F97-8C69-4016-A8EB-89A00217291F}' : u'Channels',
	'{E548108C-5B87-4BD5-BD88-C034DB5B10E4}' : u'PhotoCDOpenOptions',
	'{323DD2BC-0205-4A44-9F8E-0CF2556F00DF}' : u'LayerSets',
	'{436CE722-7369-4395-ACC2-2DE7A09269DF}' : u'_PhotoshopSaveOptions',
	'{FF49950B-15CF-4228-833C-AAA05BAF919E}' : u'BatchOptions',
	'{4D40BE2D-FE11-4060-B52A-DE31C837D51D}' : u'_BMPSaveOptions',
	'{376C4F3B-0345-440B-90D9-FE78AECA249C}' : u'_PresentationOptions',
	'{7E8F9046-9F8E-4594-A22C-9F6B4C227CD7}' : u'_SubPathInfo',
	'{098A6E1B-2858-452A-861E-768ECF77342B}' : u'JPEGSaveOptions',
	'{2EB2592D-F02D-4117-A22C-26E5CDFAEEE2}' : u'_GalleryCustomColorOptions',
	'{69172A3F-E06E-42E6-B733-4DC36E2AC948}' : u'HistoryStates',
	'{66869370-9672-492D-93AC-0ADD62F427F1}' : u'CountItem',
	'{E7A940CD-9AC7-4D76-975D-24D6BA0FDD16}' : u'TextItem',
	'{95D69B63-B319-44D3-8307-C988E96E7E58}' : u'_GallerySecurityOptions',
	'{D26A2BBD-CB80-486D-BC0E-1218F778D385}' : u'RGBColor',
	'{77AFA639-A23D-4710-94A0-5D5DE8F19677}' : u'GalleryThumbnailOptions',
	'{A24CEE4D-F054-4189-865A-DF11D77BA60B}' : u'DICOMOpenOptions',
	'{750824C6-C347-4CDB-AA96-8ABA1EBDF9EA}' : u'_NoColor',
	'{B0D18870-EAC3-4D35-8612-6F734B3FA656}' : u'_BatchOptions',
	'{CBC6639C-1C24-4820-80D9-1166C7D59782}' : u'TiffSaveOptions',
	'{F715C957-54CE-4E55-9856-591D4CD082FD}' : u'_EPSOpenOptions',
	'{8AEA5346-0DD7-4D56-AE28-0C993F35B6F1}' : u'TargaSaveOptions',
	'{D2D1665E-C1B9-4CA0-8AC9-529F6A3D9002}' : u'_SolidColor',
	'{C21B3526-8BAC-496A-8E87-D2EDB1FC87D5}' : u'ActionList',
	'{F1AF982E-2BBD-406D-9FD6-CA6C898A7FFE}' : u'_DCS2_SaveOptions',
	'{C88838E3-5A82-4EE7-A66C-E0360C9B0356}' : u'TextFont',
	'{DDA16C46-15B2-472D-A659-41AA7BFDC4FD}' : u'Layers',
	'{5148663B-F632-4AB0-9484-2DBC197CEA82}' : u'_JPEGSaveOptions',
	'{16BE80A3-57B1-4871-83AC-7F844EEEB1CA}' : u'ArtLayer',
	'{568C23FD-CEE1-4E9C-A743-5335A23A9134}' : u'PicturePackageOptions',
	'{45F1195F-3554-4B3F-A00A-E1D189C0DC3E}' : u'_RGBColor',
	'{BBCE52D6-5D4B-4691-99E3-62C174BD2855}' : u'TextFonts',
	'{9E582123-C2BA-4A25-9E56-3FD3BAF8CB71}' : u'CameraRAWOpenOptions',
	'{91B5F8AE-3CC5-4775-BCD3-FF1E0724BB01}' : u'PathItems',
	'{9E01C1DA-DF69-4C2C-85EC-616370DF1CF0}' : u'CountItems',
	'{A2770B5C-794A-41FD-8370-FEFF0CA6FBF9}' : u'DCS1_SaveOptions',
	'{94C4A25A-2C91-4514-A783-3173AFC48430}' : u'_DCS1_SaveOptions',
	'{C1C35524-2AA4-4630-80B9-011EFE3D5779}' : u'LayerSet',
	'{662506C7-6AAE-4422-ACA4-C63627CB1868}' : u'Documents',
	'{2BD9D654-530E-4BA5-B6D2-B0EB1FEA1B32}' : u'SGIRGBSaveOptions',
	'{4A304587-695B-482A-A48D-CDC2C9AC597A}' : u'SubPathInfo',
	'{8214A53C-0E67-49D4-A65A-D56F07B17D37}' : u'PathPoints',
	'{24F0AB76-0D84-4C40-8B96-570FB3985310}' : u'SolidColor',
	'{70A60330-E866-46AA-A715-ABF418C41453}' : u'_ActionDescriptor',
	'{01CD87DE-1F53-485D-A096-0D318611AB6D}' : u'_SGIRGBSaveOptions',
	'{B46396C2-8295-4754-B3A0-65C6512FB8F5}' : u'RawSaveOptions',
	'{EDC373C3-FE30-40BA-A31C-0251CA7456F1}' : u'HistoryState',
	'{6B785D83-5B5F-4402-A712-BAEBD8C5B812}' : u'_RawFormatOpenOptions',
	'{94C016CD-178F-4FD7-85BB-F5925A34A122}' : u'_PixarSaveOptions',
	'{09DA6B10-9684-44EE-A575-01F54660BDDC}' : u'Selection',
	'{5F168D2A-F9EA-4866-8C55-4875E0940622}' : u'_GalleryBannerOptions',
	'{6A16CF17-EE0B-4A2F-9F9A-76FAC0D51EC5}' : u'EPSOpenOptions',
	'{97C81476-3F5D-4934-8CAA-1ED0242105B0}' : u'ColorSamplers',
	'{D54491EF-6F09-4DE3-B49A-D57EDB2F40B8}' : u'_EPSSaveOptions',
	'{ECF6155E-BFAA-40F5-8438-F967FDE1F5FF}' : u'GalleryBannerOptions',
	'{8B4F1F1E-4ED7-4291-AE61-76ADF4D1D50B}' : u'Notifier',
	'{1DB7F789-1D27-49DD-A0C2-91B13F4ECCA4}' : u'ActionDescriptor',
	'{BA94FEB3-1812-4D0B-8954-4AAC46C6FC24}' : u'EPSSaveOptions',
	'{1B28B8CD-7578-415F-AC67-DC22A69F4C07}' : u'_GrayColor',
	'{DFF407C7-3BCC-45AC-B6CC-EE6D52032D85}' : u'_ActionReference',
	'{48F059EF-BC3E-4763-B820-AA3BB88875FF}' : u'PixarSaveOptions',
	'{F867E6C9-B5DB-4C5A-B3BA-63224D08A01B}' : u'_PDFSaveOptions',
	'{064BBE94-396D-4B25-9071-AC5B14D0487F}' : u'_ContactSheetOptions',
	'{47473C27-0EF2-4604-AEC9-85BCE5364137}' : u'GalleryOptions',
	'{29C13F49-BCEF-4FE2-BFC7-6F03B82B726F}' : u'_CMYKColor',
	'{55031766-E456-4E54-A0D0-8E545601A2D8}' : u'_ActionList',
	'{299FCD63-098D-4039-8AEC-F4D83DE04865}' : u'PDFSaveOptions',
	'{46AB9A1D-1B32-4C59-8142-B223ECCF1F74}' : u'_GalleryImagesOptions',
	'{ECC0E713-BC0D-4C54-86CA-777823D0DBE4}' : u'NoColor',
	'{B1ADEFB6-C536-42D6-8A83-397354A769F8}' : u'Document',
	'{7BDD6E7B-8FF7-4977-8E6A-5B62CFA47268}' : u'GalleryCustomColorOptions',
	'{96D48725-75BF-4733-962F-120C5681ADE4}' : u'RawFormatOpenOptions',
	'{9F67EDB6-F1D0-458E-B254-67379CABAAA0}' : u'LabColor',
	'{89417281-E1AF-4800-B82A-9F37ED0478EF}' : u'_GIFSaveOptions',
	'{C2783141-B50D-4F0C-9E2E-BF76EA8A4E60}' : u'_GalleryOptions',
	'{5DE90358-4D0B-4FA1-BA3E-C91BBA863F32}' : u'_Application',
	'{F91F9C5B-AC34-45B7-AFF2-871D9DD2E8EC}' : u'_HSBColor',
	'{372B4D75-EB10-4D0A-8203-5778D521253D}' : u'_TiffSaveOptions',
	'{288BC58E-AB6A-467C-B244-D225349E3EB3}' : u'Preferences',
	'{91A3D47B-9579-4013-9206-7B6859439DA2}' : u'_ExportOptionsSaveForWeb',
	'{599D4B7A-8C9F-41D0-AFBC-54CC1D0F957A}' : u'ExportOptionsSaveForWeb',
	'{478BF855-E42A-4D63-8C9D-F562DE5FF7A8}' : u'_PNGSaveOptions',
	'{9FE8667D-3D4D-4509-8C39-659F961E10C6}' : u'PICTFileSaveOptions',
	'{8FD90215-58CF-4301-9F13-B0C26BA9FABF}' : u'ContactSheetOptions',
	'{ABD0F9CE-822B-4BB1-A811-3EC852B43C0F}' : u'_PicturePackageOptions',
	'{4B9E6B85-0613-4873-8AB7-575CD2226768}' : u'Channel',
	'{68F15227-7568-47E1-A4F8-5615C24BDD28}' : u'_PhotoCDOpenOptions',
	'{FA15F3C2-CE4F-4DF2-A37A-ADAE44A50D55}' : u'GallerySecurityOptions',
	'{46DFAF34-75E0-470E-8217-B0C763137DD0}' : u'_GalleryThumbnailOptions',
	'{B125A66B-4C94-4E55-AF2F-57EC4DCB484B}' : u'ColorSampler',
	'{D2616C9B-22B5-491B-8285-FF1B2B0B944A}' : u'PDFOpenOptions',
	'{65D1B010-0D87-481C-B2E6-22EFB5A54129}' : u'_CameraRAWOpenOptions',
}
VTablesToClassMap = {}
VTablesToPackageMap = {
}


NamesToIIDMap = {
	'Layers' : '{DDA16C46-15B2-472D-A659-41AA7BFDC4FD}',
	'_PhotoshopSaveOptions' : '{436CE722-7369-4395-ACC2-2DE7A09269DF}',
	'TextFont' : '{C88838E3-5A82-4EE7-A66C-E0360C9B0356}',
	'HistoryStates' : '{69172A3F-E06E-42E6-B733-4DC36E2AC948}',
	'SubPathItem' : '{B6D22EB9-EC6D-46DB-B52A-5561433A1217}',
	'_SubPathInfo' : '{7E8F9046-9F8E-4594-A22C-9F6B4C227CD7}',
	'_PresentationOptions' : '{376C4F3B-0345-440B-90D9-FE78AECA249C}',
	'PathItem' : '{8B0CB532-4ACC-4BF3-9E42-0949B679D120}',
	'_GalleryBannerOptions' : '{5F168D2A-F9EA-4866-8C55-4875E0940622}',
	'ColorSamplers' : '{97C81476-3F5D-4934-8CAA-1ED0242105B0}',
	'_ActionReference' : '{DFF407C7-3BCC-45AC-B6CC-EE6D52032D85}',
	'_GrayColor' : '{1B28B8CD-7578-415F-AC67-DC22A69F4C07}',
	'TextItem' : '{E7A940CD-9AC7-4D76-975D-24D6BA0FDD16}',
	'_PixarSaveOptions' : '{94C016CD-178F-4FD7-85BB-F5925A34A122}',
	'XMPMetadata' : '{DC865034-A587-4CC4-8A5A-453032562BE4}',
	'PathPoints' : '{8214A53C-0E67-49D4-A65A-D56F07B17D37}',
	'_GIFSaveOptions' : '{89417281-E1AF-4800-B82A-9F37ED0478EF}',
	'_LabColor' : '{F4D7F5C2-37DB-4DF7-8A7D-528902056596}',
	'Document' : '{B1ADEFB6-C536-42D6-8A83-397354A769F8}',
	'Documents' : '{662506C7-6AAE-4422-ACA4-C63627CB1868}',
	'_CMYKColor' : '{29C13F49-BCEF-4FE2-BFC7-6F03B82B726F}',
	'Channel' : '{4B9E6B85-0613-4873-8AB7-575CD2226768}',
	'_PDFSaveOptions' : '{F867E6C9-B5DB-4C5A-B3BA-63224D08A01B}',
	'_BMPSaveOptions' : '{4D40BE2D-FE11-4060-B52A-DE31C837D51D}',
	'LayerComp' : '{9A37A0AC-E951-4B16-A548-886B77338DE0}',
	'_GalleryImagesOptions' : '{46AB9A1D-1B32-4C59-8142-B223ECCF1F74}',
	'_SolidColor' : '{D2D1665E-C1B9-4CA0-8AC9-529F6A3D9002}',
	'_GalleryThumbnailOptions' : '{46DFAF34-75E0-470E-8217-B0C763137DD0}',
	'_BatchOptions' : '{B0D18870-EAC3-4D35-8612-6F734B3FA656}',
	'_BitmapConversionOptions' : '{643099A1-0B67-4920-9B14-E14BE8F63D5F}',
	'_PNGSaveOptions' : '{478BF855-E42A-4D63-8C9D-F562DE5FF7A8}',
	'HistoryState' : '{EDC373C3-FE30-40BA-A31C-0251CA7456F1}',
	'_ActionList' : '{55031766-E456-4E54-A0D0-8E545601A2D8}',
	'DocumentInfo' : '{746FEF90-A182-4BD0-A4F6-BB6BBAE87A78}',
	'_RawSaveOptions' : '{D74B820F-AA86-42DD-8D85-F4D67A62F200}',
	'MeasurementScale' : '{632F36B3-1D76-48BE-ADC3-D7FB62A0C2FB}',
	'ArtLayers' : '{EC6A366C-F901-488D-A2C3-9E2E78B72DC6}',
	'_IndexedConversionOptions' : '{22D0B851-E811-40E2-9A79-E84EA602C9F1}',
	'_NoColor' : '{750824C6-C347-4CDB-AA96-8ABA1EBDF9EA}',
	'CountItems' : '{9E01C1DA-DF69-4C2C-85EC-616370DF1CF0}',
	'_ContactSheetOptions' : '{064BBE94-396D-4B25-9071-AC5B14D0487F}',
	'_TargaSaveOptions' : '{F4E21694-AEBF-44FB-90AB-EECD58C1B6F3}',
	'_EPSSaveOptions' : '{D54491EF-6F09-4DE3-B49A-D57EDB2F40B8}',
	'_TiffSaveOptions' : '{372B4D75-EB10-4D0A-8203-5778D521253D}',
	'ArtLayer' : '{16BE80A3-57B1-4871-83AC-7F844EEEB1CA}',
	'_ExportOptionsSaveForWeb' : '{91A3D47B-9579-4013-9206-7B6859439DA2}',
	'_PDFOpenOptions' : '{50D0174F-484D-4A2B-8BF0-A21B84167D82}',
	'Preferences' : '{288BC58E-AB6A-467C-B244-D225349E3EB3}',
	'_RGBColor' : '{45F1195F-3554-4B3F-A00A-E1D189C0DC3E}',
	'_SGIRGBSaveOptions' : '{01CD87DE-1F53-485D-A096-0D318611AB6D}',
	'_ActionDescriptor' : '{70A60330-E866-46AA-A715-ABF418C41453}',
	'_DICOMOpenOptions' : '{EE8364D9-B811-4C7D-A3A8-97C4EBFAB83A}',
	'SubPathItems' : '{B7283EEC-23B1-49A6-B151-0E97E4AF353C}',
	'_GalleryCustomColorOptions' : '{2EB2592D-F02D-4117-A22C-26E5CDFAEEE2}',
	'_DCS2_SaveOptions' : '{F1AF982E-2BBD-406D-9FD6-CA6C898A7FFE}',
	'_PathPointInfo' : '{B3C35001-B625-48D7-9D3B-C9D66D9CF5F1}',
	'_DCS1_SaveOptions' : '{94C4A25A-2C91-4514-A783-3173AFC48430}',
	'Notifier' : '{8B4F1F1E-4ED7-4291-AE61-76ADF4D1D50B}',
	'_PicturePackageOptions' : '{ABD0F9CE-822B-4BB1-A811-3EC852B43C0F}',
	'_EPSOpenOptions' : '{F715C957-54CE-4E55-9856-591D4CD082FD}',
	'CountItem' : '{66869370-9672-492D-93AC-0ADD62F427F1}',
	'_CameraRAWOpenOptions' : '{65D1B010-0D87-481C-B2E6-22EFB5A54129}',
	'Selection' : '{09DA6B10-9684-44EE-A575-01F54660BDDC}',
	'TextFonts' : '{BBCE52D6-5D4B-4691-99E3-62C174BD2855}',
	'_JPEGSaveOptions' : '{5148663B-F632-4AB0-9484-2DBC197CEA82}',
	'_PICTFileSaveOptions' : '{D334A509-00F8-4092-A9AF-6E1176D06536}',
	'ColorSampler' : '{B125A66B-4C94-4E55-AF2F-57EC4DCB484B}',
	'PathPoint' : '{7D14BA29-1672-482F-8F48-9DA1E94800FD}',
	'_RawFormatOpenOptions' : '{6B785D83-5B5F-4402-A712-BAEBD8C5B812}',
	'_HSBColor' : '{F91F9C5B-AC34-45B7-AFF2-871D9DD2E8EC}',
	'_PhotoCDOpenOptions' : '{68F15227-7568-47E1-A4F8-5615C24BDD28}',
	'Channels' : '{2DC64F97-8C69-4016-A8EB-89A00217291F}',
	'_GalleryOptions' : '{C2783141-B50D-4F0C-9E2E-BF76EA8A4E60}',
	'_GallerySecurityOptions' : '{95D69B63-B319-44D3-8307-C988E96E7E58}',
	'LayerComps' : '{726B458C-74B0-47AE-B390-99753B55DF2E}',
	'_Application' : '{5DE90358-4D0B-4FA1-BA3E-C91BBA863F32}',
	'LayerSets' : '{323DD2BC-0205-4A44-9F8E-0CF2556F00DF}',
	'Notifiers' : '{861C9290-2A0C-4614-8606-706B31BFD45B}',
	'LayerSet' : '{C1C35524-2AA4-4630-80B9-011EFE3D5779}',
	'PathItems' : '{91B5F8AE-3CC5-4775-BCD3-FF1E0724BB01}',
	'MeasurementLog' : '{84ADBF06-8354-4B5C-9CB1-EA2565B66C7C}',
	'_ExportOptionsIllustrator' : '{FC08B435-5F19-49DF-ABE7-ADCE9F0729FF}',
}

win32com.client.constants.__dicts__.append(constants.__dict__)

