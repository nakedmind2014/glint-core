import os
import sys
import types
import tempfile
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import patch
except ImportError:
    from mock import patch

import pytest
import glint


def test_expand_vars_use_default_source():
    os.environ['GLINT_TEMP_DIR'] = tempfile.gettempdir()
    actual = glint.util.expand_vars('%GLINT_TEMP_DIR%/test')
    assert actual == tempfile.gettempdir()+'/test'


def test_expand_vars_use_given_dict_as_source():
    value_source = {'ref_key': tempfile.gettempdir()+'/glint'}
    actual = glint.util.expand_vars('$ref_key/test', [value_source])
    assert actual == tempfile.gettempdir()+'/glint/test'


def test_expand_vars_support_recursive_expansion_using_both_custom_and_default_sources():
    os.environ['GLINT_TEMP_DIR'] = tempfile.gettempdir()
    other_source = {'ref_key': '%GLINT_TEMP_DIR%/glint'}
    actual = glint.util.expand_vars('$ref_key/test', [other_source])
    assert actual == tempfile.gettempdir()+'/glint/test'


def test_expand_vars_accept_source_list_with_priority_based_on_list_order():
    first_source = {'ref_key': 'temp/glint'}
    second_source = {'ref_key': 'xxx/glint'}
    actual = glint.util.expand_vars('$ref_key/test', [first_source, second_source])
    assert actual == 'temp/glint/test'


def _create_temp_file(suffix='', content=''):
    fd, temp_file = tempfile.mkstemp(suffix=suffix, text='a')
    with os.fdopen(fd, 'w') as fh:
        fh.write(content)
    return Path(temp_file)


def test_import_file():
    script_path = _create_temp_file(suffix='.py')
    try:
        actual = glint.util.import_file(script_path)
        assert isinstance(actual, types.ModuleType)
        assert actual.__name__ == script_path.stem
    finally:
        script_path.unlink()


def test_camelcase2underscore():
    actual = glint.util.camelcase_to_underscore('CamelCase')
    assert actual == 'camel_case'
    actual = glint.util.camelcase_to_underscore('_CamelCase_')
    assert actual == 'camel_case'
    actual = glint.util.camelcase_to_underscore('__CamelCase__')
    assert actual == 'camel_case'
    actual = glint.util.camelcase_to_underscore('__Camel_Case__')
    assert actual == 'camel_case'


def test_whitespace2camelcase():
    actual = glint.util.whitespace_to_camelcase('camel case')
    assert actual == 'camelCase'
    actual = glint.util.whitespace_to_camelcase('  camel   case  ')
    assert actual == 'camelCase'
    actual = glint.util.whitespace_to_camelcase('camel\tcase\n ')
    assert actual == 'camelCase'
    actual = glint.util.whitespace_to_camelcase('camel case !@#$')
    assert actual == 'camelCase'


@patch('glint.util.Path.exists', return_value=True)
def test_register_path_only_if_it_exists(_):
    fp = Path('C:/Windows/system32')
    glint.util.register_path(fp)
    assert str(fp) in sys.path, "Existing path not registered"


@patch('glint.util.Path.exists', return_value=True)
def test_register_path_does_not_accept_dot_path(_):
    fp = Path('.')
    glint.util.register_path(fp)
    assert str(fp) not in sys.path, "Dot path is registered"


@patch('glint.util.Path.exists', return_value=True)
def test_register_path_only_once(_):
    fp = Path('C:/Windows/system32')
    glint.util.register_path(fp)
    glint.util.register_path(fp)
    assert sys.path.count(str(fp)) == 1, "Path added multiple times"


if __name__ == '__main__':
    pytest.main([__file__])
