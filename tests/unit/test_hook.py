from __future__ import unicode_literals
import imp
import shutil
import logging
import tempfile
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import Mock, patch
except ImportError:
    from mock import Mock, patch
import pytest

import glint


test_glint_root = Path(tempfile.gettempdir())/'glint-root'


@pytest.fixture(scope='module', autouse=True)
def test_setup():
    # Make sure to start with an empty test dir
    try:
        shutil.rmtree(str(test_glint_root))
    except WindowsError:
        pass


class TestRegisterHooks:
    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.hook import _hook_registry, register_hooks
        _hook_registry.clear()
        self.register_hooks = register_hooks
        self.registry = _hook_registry
        self.location = test_glint_root

    def test_manifest_hooks(self):
        manifest = {'hooks': ['hook1', 'hook2', 'hook3']}
        self.register_hooks(manifest, self.location)
        expected = {
            'hook1': {'path': test_glint_root/'hook1.py', 'module': None},
            'hook2': {'path': test_glint_root/'hook2.py', 'module': None},
            'hook3': {'path': test_glint_root/'hook3.py', 'module': None},
        }
        assert self.registry == expected

    def test_no_manifest_hooks(self):
        self.register_hooks({}, self.location)
        assert self.registry == {}


class TestLoadHook:
    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.hook import _hook_registry, load_hook
        _hook_registry.clear()
        _hook_registry['hook1'] = {
            'path': test_glint_root/'plugins/test-plugin/hooks/hook1.py',
            'module': None
        }

        self.hook_mod = Mock()
        self.load_hook = load_hook
        self.registry = _hook_registry

        patcher1 = patch('os.environ.get')
        self.os_environ = patcher1.start()

        patcher2 = patch('glint.hook.get_glint_root', return_value=test_glint_root)
        patcher2.start()

        patcher3 = patch('glint.hook.import_file', return_value=self.hook_mod)
        self.import_file = patcher3.start()

        (test_glint_root/'hooks').mkdir(parents=True)
        (test_glint_root/'custom_hooks').mkdir(parents=True)
        (test_glint_root/'plugins/test-plugin/hooks').mkdir(parents=True)
        yield

        try:
            shutil.rmtree(str(test_glint_root))
        except OSError:
            pass
        patcher1.stop()
        patcher2.stop()
        patcher3.stop()

    def test_var_set__file_in_var_path(self):
        """ Load hook file in location pointed to by GLINT_HOOK_PATH """
        self.os_environ.return_value = str(test_glint_root/'custom_hooks')
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'hooks/hook1.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file3.write_text('')

        self.load_hook('hook1')
        # Hook file in custom hook location is the one loaded
        assert self.registry == {'hook1': {'path': hook_file1, 'module': self.hook_mod}}

    def test_var_set__file_not_in_var_path(self):
        """ Load hook file in GLINT_ROOT/hooks """
        self.os_environ.return_value = str(test_glint_root/'custom_hooks')
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file2.write_text('')

        self.load_hook('hook1')
        # Hook file in default hook location is the one loaded
        assert self.registry == {'hook1': {'path': hook_file1, 'module': self.hook_mod}}

    def test_var_set__file_not_in_var_path_and_global_default(self):
        """ Load component's default hook """
        self.os_environ.return_value = str(test_glint_root/'custom_hooks')
        # Create files so filecheck succeeds
        hook_file = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file.write_text('')

        self.load_hook('hook1')
        # Default component hook file is the one loaded
        assert self.registry == {'hook1': {'path': hook_file, 'module': self.hook_mod}}

    def test_var_set__file_not_found(self):
        """ Raise exception if hook file cannot be found, even in
        component's hook folder """
        self.os_environ.return_value = str(test_glint_root/'custom_hooks')
        with pytest.raises(glint.errors.GlintError):
            self.load_hook('hook1')

    def test_var_not_set__file_in_global_default(self):
        """ Load hook file in GLINT_ROOT/hooks """
        self.os_environ.return_value = None
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file2.write_text('')

        self.load_hook('hook1')
        # Hook file in default hook location is the one loaded
        assert self.registry == {'hook1': {'path': hook_file1, 'module': self.hook_mod}}

    def test_var_not_set__file_not_in_global_default(self):
        """ Load component's default hook """
        self.os_environ.return_value = None
        # Create files so filecheck succeeds
        hook_file = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file.write_text('')

        self.load_hook('hook1')
        # Default component hook file is the one loaded
        assert self.registry == {'hook1': {'path': hook_file, 'module': self.hook_mod}}

    def test_var_not_set__file_not_found(self):
        """ Raise exception if hook file cannot be found, even in
        component's hook folder """
        self.os_environ.return_value = None
        with pytest.raises(glint.errors.GlintError):
            self.load_hook('hook1')

    def test_hook_module_no_entry_point(self):
        """ Raise exception if hook module contains no main function """
        self.os_environ.return_value = None
        # Create file so filecheck succeeds
        (test_glint_root/'hooks/hook1.py').write_text('')
        # Simulate hook module with no main function
        self.import_file.return_value = Mock(spec=[])

        with pytest.raises(glint.errors.GlintError):
            self.load_hook('hook1')

    def test_return_loaded_module(self):
        self.os_environ.return_value = None
        # Create file so filecheck succeeds
        (test_glint_root/'plugins/test-plugin/hooks/hook1.py').write_text('')
        assert self.load_hook('hook1') == self.hook_mod


class TestLoadAllHooks:
    def _import_file_mock(self, p, *args, **kwargs):
        return {
            'hook1': self.hook_mod1, 'hook2': self.hook_mod2,
            'hook3': self.hook_mod3
        }[p.stem]

    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.hook import _hook_registry, load_all_hooks
        _hook_registry.clear()
        _hook_registry['hook1'] = {
            'path': test_glint_root/'plugins/test-plugin/hooks/hook1.py',
            'module': None
        }
        _hook_registry['hook2'] = {
            'path': test_glint_root/'plugins/test-plugin/hooks/hook2.py',
            'module': None
        }
        _hook_registry['hook3'] = {
            'path': test_glint_root/'plugins/test-plugin/hooks/hook3.py',
            'module': None
        }

        self.hook_mod1 = Mock()
        self.hook_mod2 = Mock()
        self.hook_mod3 = Mock()

        self.load_all_hooks = load_all_hooks
        self.registry = _hook_registry

        patcher1 = patch('os.environ.get', return_value=str(test_glint_root/'custom_hooks'))
        self.os_environ = patcher1.start()

        patcher2 = patch('glint.hook.get_glint_root', return_value=test_glint_root)
        patcher2.start()

        patcher3 = patch('glint.hook.import_file', side_effect=self._import_file_mock)
        patcher3.start()

        (test_glint_root/'hooks').mkdir(parents=True)
        (test_glint_root/'custom_hooks').mkdir(parents=True)
        (test_glint_root/'plugins/test-plugin/hooks').mkdir(parents=True)
        yield

        try:
            shutil.rmtree(str(test_glint_root))
        except OSError:
            pass
        patcher1.stop()
        patcher2.stop()
        patcher3.stop()

    def test_files_all_in_var_path(self):
        """ Load hook files in location pointed to by GLINT_HOOK_PATH """
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'custom_hooks/hook2.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'custom_hooks/hook3.py'
        hook_file3.write_text('')

        self.load_all_hooks()
        expected = {
            'hook1': {'path': hook_file1, 'module': self.hook_mod1},
            'hook2': {'path': hook_file2, 'module': self.hook_mod2},
            'hook3': {'path': hook_file3, 'module': self.hook_mod3}
        }
        assert self.registry == expected

    def test_files_in_var_path_and_in_global_default(self):
        """ Load hook files found in GLINT_HOOK_PATH and in GLINT_ROOT/hooks """
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'custom_hooks/hook2.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'hooks/hook3.py'
        hook_file3.write_text('')

        self.load_all_hooks()
        expected = {
            'hook1': {'path': hook_file1, 'module': self.hook_mod1},
            'hook2': {'path': hook_file2, 'module': self.hook_mod2},
            'hook3': {'path': hook_file3, 'module': self.hook_mod3}
        }
        assert self.registry == expected

    def test_files_in_var_path_in_global_default_and_in_component_folder(self):
        """ Load hook files found in GLINT_HOOK_PATH, GLINT_ROOT/hooks
        and component folders """
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'hooks/hook2.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'plugins/test-plugin/hooks/hook3.py'
        hook_file3.write_text('')

        self.load_all_hooks()
        expected = {
            'hook1': {'path': hook_file1, 'module': self.hook_mod1},
            'hook2': {'path': hook_file2, 'module': self.hook_mod2},
            'hook3': {'path': hook_file3, 'module': self.hook_mod3},
        }
        assert self.registry == expected

    def test_duplicate_files_in_var_path_in_globa_default_and_in_component_folder(self):
        """ Prioritize loading of hook files found in GLINT_HOOK_PATH and then in
        GLINT_ROOT/hooks over those in component folders """
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'custom_hooks/hook2.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'hooks/hook3.py'
        hook_file3.write_text('')
        hook_file4 = test_glint_root/'plugins/test-plugin/hooks/hook1.py'
        hook_file4.write_text('')
        hook_file5 = test_glint_root/'plugins/test-plugin/hooks/hook2.py'
        hook_file5.write_text('')
        hook_file6 = test_glint_root/'plugins/test-plugin/hooks/hook3.py'
        hook_file6.write_text('')

        self.load_all_hooks()
        expected = {
            'hook1': {'path': hook_file1, 'module': self.hook_mod1},
            'hook2': {'path': hook_file2, 'module': self.hook_mod2},
            'hook3': {'path': hook_file3, 'module': self.hook_mod3},
        }
        assert self.registry == expected

    def test_one_file_has_no_main(self):
        """ Load only hooks with main """
        # Create files so filecheck succeeds
        hook_file1 = test_glint_root/'custom_hooks/hook1.py'
        hook_file1.write_text('')
        hook_file2 = test_glint_root/'hooks/hook2.py'
        hook_file2.write_text('')
        hook_file3 = test_glint_root/'plugins/test-plugin/hooks/hook3.py'
        hook_file3.write_text('')
        # Simulate hook module with no main function
        self.hook_mod3 = Mock(spec=[])

        self.load_all_hooks()
        expected = {
            'hook1': {'path': hook_file1, 'module': self.hook_mod1},
            'hook2': {'path': hook_file2, 'module': self.hook_mod2},
            'hook3': {'path': test_glint_root/'plugins/test-plugin/hooks/hook3.py',
                      'module': None},
        }
        assert self.registry == expected


class TestRunHook:
    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.hook import _hook_registry, run_hook
        _hook_registry.clear()

        self.run_hook = run_hook
        self.registry = _hook_registry

        self.hook_mod = imp.new_module('test-hook')
        exec('def main(): pass', self.hook_mod.__dict__)

        patcher = patch('glint.hook.load_hook', return_value=self.hook_mod)
        self.load_hook = patcher.start()

        _hook_registry['test-hook'] = {'module': self.hook_mod, 'path': ''}
        yield

        patcher.stop()

    def test_not_registered(self):
        with pytest.raises(glint.errors.GlintError):
            self.run_hook('unregistered-hook')

    def test_not_yet_loaded(self):
        """ Call load_hook with the hook name """
        self.registry['test-hook'] = {'module': None}
        self.run_hook('test-hook')
        self.load_hook.assert_called_with('test-hook')

    def test_not_yet_loaded__module_not_found(self):
        """ Exception bubbles up to hook caller """
        self.registry['test-hook'] = {'module': None}
        self.load_hook.side_effect = glint.errors.GlintError
        with pytest.raises(glint.errors.GlintError):
            self.run_hook('test-hook')

    def test_exception_in_hook(self):
        """ Exception bubbles up to hook caller """
        # Calling main() function generates an exception
        exec('def main(): raise ValueError', self.hook_mod.__dict__)
        with pytest.raises(ValueError):
            self.run_hook('test-hook')

    def test_execute__unexpected_args(self):
        """ Passing args to a hook that does not accept args
        raises TypeError exception """
        exec('def main(): pass', self.hook_mod.__dict__)
        with pytest.raises(TypeError):
            self.run_hook('test-hook', 1, x=2)

    def test_execute__with_args(self):
        """ Pass given args to hook """
        exec('def main(x, y=2, z=3): return x, y, z', self.hook_mod.__dict__)
        actual = self.run_hook('test-hook', 1, y=5, z=6)
        assert actual == (1, 5, 6)


if __name__ == '__main__':
    pytest.main([__file__, '-s'])
