from __future__ import unicode_literals
import os
import textwrap
import tempfile
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path

import pytest


class TestBase:
    @pytest.fixture(autouse=True)
    def set_up(self):
        import glint

        # Clear registry before each run. Not ideal since it depends
        # on implementation detail.
        glint.settings.__dict__['__global_settings'].clear()
        del glint.settings.__dict__['__path_keys'][:]
        glint.settings.__dict__['__settings_file'] = None

        self.load_settings = glint.settings.load_settings
        self.get_setting = glint.settings.get_setting
        os.environ['GLINT_SETTINGS_PATH'] = str(self.settings_file)
        # Tell glint the path to the settings file if present
        glint.settings.check_settings_file()


class TestSettings_DefaultValueNotSpecified(TestBase):
    settings_file = None

    def test_default_to_str_if_type_not_specified(self):
        self.load_settings('core', {'test_key': {}})
        assert self.get_setting('test_key') == ''

    def test_int_defaults_to_zero(self):
        settings_def = {'test_key': {'type': 'int'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 0

    def test_float_defaults_to_zero(self):
        settings_def = {'test_key': {'type': 'float'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 0.0

    def test_bool_defaults_to_False(self):
        settings_def = {'test_key': {'type': 'bool'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') is False

    def test_list_defaults_to_empty_list(self):
        settings_def = {'test_key': {'type': 'list'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == []

    def test_path_defaults_to_empty_string(self):
        settings_def = {'test_key': {'type': 'path'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == ''

    def test_private_setting_is_not_loaded_in_global_settings(self):
        settings_def = {'test_key': {'private': True}}
        local_dict = {}
        self.load_settings('core', settings_def, local_dict)
        assert local_dict == {'test_key': ''}
        assert self.get_setting('test_key') is None


class TestSettings_DefaultValueSpecified(TestBase):
    settings_file = None

    def test_use_specified_default_value(self):
        settings_def = {'test_key': {'type': 'str', 'default_value': 'test_value'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 'test_value'

    def test_convert_to_Path_object_if_path_default_value_specified(self):
        settings_def = {'test_key': {'type': 'path', 'default_value': '%TMP%/path/to/file'}}
        self.load_settings('core', settings_def)
        assert isinstance(self.get_setting('test_key'), Path)

    def test_expand_if_path_default_value_contains_env_vars(self):
        settings_def = {'test_key': {'type': 'path', 'default_value': '%TMP%/path/to/file'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == Path(os.environ['TMP'], 'path/to/file')


class TestSettings_FromSettingsFile(TestBase):
    settings_file = Path(tempfile.gettempdir())/'settings.ini'
    # Create file so check succeeds
    settings_file.write_text('')

    def test_str(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = new value
        """))

        settings_def = {'test_key': {'type': 'str', 'default_value': 'test_value'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 'new value'

    def test_int(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = 10
        """))

        settings_def = {'test_key': {'type': 'int'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 10

    def test_float(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = 10.5
        """))

        settings_def = {'test_key': {'type': 'float'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == 10.5

    def test_bool(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = yes
        """))

        settings_def = {'test_key': {'type': 'bool'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') is True

    def test_list(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = 1,2,3,4,5
        """))

        settings_def = {'test_key': {'type': 'list'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == ['1', '2', '3', '4', '5', ]

    def test_path_setting_loaded_as_Path_object(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = C:/path/to/file
        """))

        settings_def = {'test_key': {'type': 'path'}}
        self.load_settings('core', settings_def)
        assert self.get_setting('test_key') == Path('C:/path/to/file')

    def test_settings_in_invalid_section_are_not_loaded(self):
        """Default value unchanged"""
        self.settings_file.write_text(textwrap.dedent("""\
            [Whatever]
            test_key = no
        """))
        self.load_settings('core', {'test_key': {}}, self.settings_file)
        assert self.get_setting('test_key') == ''

    def test_undefined_keys_are_not_loaded(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = new value
        """))
        self.load_settings('core', {}, self.settings_file)
        assert self.get_setting('test_key') is None

    def test_private_setting_is_not_loaded_in_global_settings(self):
        self.settings_file.write_text(textwrap.dedent("""\
            [core]
            test_key = new value
        """))

        settings_def = {'test_key': {'private': True}}
        local_dict = {}
        self.load_settings('core', settings_def, local_dict)

        assert local_dict == {'test_key': 'new value'}
        assert self.get_setting('test_key') is None


if __name__ == '__main__':
    pytest.main([__file__])
