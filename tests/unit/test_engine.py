import os
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import Mock, PropertyMock, ANY, call, patch
except ImportError:
    from mock import Mock, PropertyMock, ANY, call, patch

import pytest
from mockextras import when

import glint


class TestEngineBase:
    @pytest.fixture(autouse=True)
    def set_up(self):
        self.engine = glint.EngineBase()

    def test_get_launchers(self):
        for i in range(1, 3):
            self.engine.register_launcher('toolA%s' % i,
                                          {'label': 'toolA%s' % i,
                                           'group': 1,
                                           'position_in_group': i},
                                          Mock())
        for i in range(1, 3):
            self.engine.register_launcher('toolB%s' % i,
                                          {'label': 'toolB%s' % i,
                                           'group': 2,
                                           'position_in_group': i},
                                          Mock())

        actual = self.engine.get_launchers()
        expected = [
            [{'name': 'toolA1', 'label': 'toolA1', 'tooltip': '', 'icon': None, 'shortcut': None},
             {'name': 'toolA2', 'label': 'toolA2', 'tooltip': '', 'icon': None, 'shortcut': None}],
            [{'name': 'toolB1', 'label': 'toolB1', 'tooltip': '', 'icon': None, 'shortcut': None},
             {'name': 'toolB2', 'label': 'toolB2', 'tooltip': '', 'icon': None, 'shortcut': None}],
        ]
        assert actual == expected

    @pytest.mark.skip('currently failing')
    def test_get_launchers__no_group_specified(self):
        for i in range(1, 3):
            self.engine.register_launcher('toolA%s' % i, {'label': 'toolA%s' % i},
                                          Mock())
        for i in range(1, 3):
            self.engine.register_launcher('toolB%s' % i,
                                          {'label': 'toolB%s' % i,
                                           'group': 2,
                                           'position_in_group': i},
                                          Mock())
        for i in range(1, 3):
            self.engine.register_launcher('toolC%s' % i,
                                          {'label': 'toolC%s' % i,
                                           'group': 1,
                                           'position_in_group': i},
                                          Mock())

        actual = self.engine.get_launchers()
        expected = [
            [{'name': 'toolC1', 'label': 'toolC1', 'tooltip': '', 'icon': None},
             {'name': 'toolC2', 'label': 'toolC2', 'tooltip': '', 'icon': None}],
            [{'name': 'toolB1', 'label': 'toolB1', 'tooltip': '', 'icon': None},
             {'name': 'toolB2', 'label': 'toolB2', 'tooltip': '', 'icon': None}],
            [{'name': 'toolA1', 'label': 'toolA1', 'tooltip': '', 'icon': None},
             {'name': 'toolA2', 'label': 'toolA2', 'tooltip': '', 'icon': None}],
        ]
        assert actual == expected


engine_path = Path('/glint/engines/test-engine')


@pytest.fixture()
def tool_root():
    patcher = patch('glint.component.engine.get_tool_root')
    # For the sake of making sure path exists, point tool_root to
    # a system folder.
    mock = patcher.start()
    if os.name == 'nt':
        mock.return_value = Path('C:\\')
    elif os.name == 'posix':
        mock.return_value = Path('/')
    yield mock.return_value

    patcher.stop()


class TestLoadEngine:
    @pytest.fixture(autouse=True)
    def set_up(self):
        patcher1 = patch.object(glint.component.engine, 'comp_mgr')
        self.comp_mgr = patcher1.start()
        patcher2 = patch('glint.component.engine.load_plugin')
        self.load_plugin = patcher2.start()
        patcher3 = patch('glint.component.engine.load_tool')
        self.load_tool = patcher3.start()

        self.comp_mgr.get_current_engine.return_value = None
        self.engine = self.comp_mgr.load_component.return_value
        yield

        patcher1.stop()
        patcher2.stop()
        patcher3.stop()

    def test_raise_GlintError_if_another_engine_already_loaded(self):
        self.comp_mgr.get_current_engine.return_value = Mock()
        msg = "glint engine is already loaded"
        with pytest.raises(glint.errors.GlintError, match=msg):
            glint.load_engine(engine_path)

    ###########################################################
    # Set expectations on how external calls are made
    def test_check_first_if_another_engine_is_already_loaded(self):
        glint.load_engine(engine_path)
        self.comp_mgr.get_current_engine.assert_called_with()

    def test_call_load_component(self):
        glint.load_engine(engine_path)
        self.comp_mgr.load_component.assert_called_with(
            engine_path, glint.constants.COMPONENT_TYPE.Engine, glint.EngineBase)

    def test_register_engine_component_paths(self):
        glint.load_engine(engine_path)
        self.engine.register_component_paths.assert_called_with()

    def test_load_required_plugins_in_declaration_order(self):
        when(self.engine.get_meta).called_with('requires', {}).then(
            ['test-plugin2', 'test-plugin1'])
        glint.load_engine(engine_path)
        expected = [call('test-plugin2'), call('test-plugin1')]
        assert self.load_plugin.mock_calls == expected

    def test_load_additional_plugins_if_specified(self):
        when(self.engine.get_setting).called_with('additional_plugins').then(
            ['test-plugin2', 'test-plugin1'])
        glint.load_engine(engine_path)
        expected = [call('test-plugin2'), call('test-plugin1')]
        assert self.load_plugin.mock_calls == expected

    def test_call_component_initializer(self):
        glint.load_engine(engine_path)
        self.comp_mgr.run_initializer.assert_called_with(
            self.comp_mgr.load_component.return_value)

    def test_load_tools_that_support_the_engine(self, tool_root):
        tool = Mock(**{'is_dir.return_value': True, 'name': PropertyMock})
        with patch('glint.component.engine.scandir', return_value=[tool]):
            with patch.object(tool, 'name', new_callable=PropertyMock):
                tool.name = 'test-tool'
                glint.load_engine(engine_path)
        self.load_tool.assert_called_with(tool_root/'test-tool')

    @pytest.mark.skip(reason="Don't know yet how to implement this test")
    def test_load_plugins_before_loading_tools(self, tool_root):
        pass

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_return_loaded_engine_if_successful(self):
        assert (glint.load_engine(engine_path)
                is self.comp_mgr.load_component.return_value)

    def test_abort_if_component_loading_raises_exception(self):
        self.comp_mgr.load_component.side_effect = ValueError
        with pytest.raises(ValueError):
            glint.load_engine(engine_path)

    def test_abort_if_initializer_raises_exception(self):
        self.comp_mgr.run_initializer.side_effect = ValueError
        with pytest.raises(ValueError):
            glint.load_engine(engine_path)
        self.comp_mgr.unload_component.assert_called_with(ANY)

    def test_abort_if_required_plugin_fails_to_load(self):
        when(self.engine.get_meta).called_with('requires', {}).then(['test-plugin'])
        when(self.load_plugin).called_with('test-plugin').then(Exception('BOOM!'))
        with pytest.raises(Exception):
            glint.load_engine(engine_path)
        self.comp_mgr.unload_component.assert_called_with(ANY)

    def test_unload_loaded_required_plugins_if_a_required_plugin_fails_to_load(self):
        plugin1 = Mock(spec=glint.component.Component)
        plugin1.name = 'test-plugin1'

        when(self.engine.get_meta).called_with('requires', {}).then(
            ['test-plugin1', 'test-plugin2'])
        when(self.load_plugin).called_with('test-plugin1').then(plugin1)
        when(self.load_plugin).called_with('test-plugin2').then(Exception('BOOM!'))
        when(self.comp_mgr).called_with('plugin').then(Exception('BOOM!'))

        with pytest.raises(Exception):
            glint.load_engine(engine_path)
        self.comp_mgr.unload_component.assert_has_calls([call('test-plugin1')])

    def test_unload_all_loaded_plugins_if_initializer_raises_exception(self):
        plugin1 = Mock(spec=glint.component.Component)
        plugin1.name = 'test-plugin1'
        plugin2 = Mock(spec=glint.component.Component)
        plugin2.name = 'test-plugin2'
        plugin3 = Mock(spec=glint.component.Component)
        plugin3.name = 'additional-plugin'

        when(self.engine.get_meta).called_with('requires', {}).then(
            ['test-plugin1', 'test-plugin2'])
        when(self.engine.get_setting).called_with('additional_plugins').then(
            ['additional-plugin'])
        when(self.load_plugin).called_with('test-plugin1').then(plugin1)
        when(self.load_plugin).called_with('test-plugin2').then(plugin2)
        when(self.load_plugin).called_with('additional-plugin').then(plugin3)
        when(self.comp_mgr.run_initializer).called_with(ANY).then(Exception('BOOM!'))

        with pytest.raises(Exception):
            glint.load_engine(engine_path)

        expected = [call('test-plugin1'), call('test-plugin2'),
                    call('additional-plugin')]
        self.comp_mgr.unload_component.assert_has_calls(expected, any_order=False)

    def test_error_in_loading_additional_plugin_has_no_effect(self):
        when(self.engine.get_setting).called_with('additional_plugins').then(
            ['test-plugin'])
        when(self.load_plugin).called_with('test-plugin').then(Exception('BOOM!'))
        glint.load_engine(engine_path)


if __name__ == '__main__':
    pytest.main([__file__])
