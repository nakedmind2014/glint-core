try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import Mock, ANY, patch
except ImportError:
    from mock import patch

import pytest

import glint


tool_path = Path('/glint/tools/test-tool')


class TestBase:
    @pytest.fixture(autouse=True)
    def base_setup(self):
        patcher1 = patch.object(glint.component.tool, 'comp_mgr')
        self.comp_mgr = patcher1.start()
        patcher2 = patch('glint.component.tool.get_main')
        self.get_main = patcher2.start()
        yield

        patcher1.stop()
        patcher2.stop()


class TestLoadTool(TestBase):
    ###########################################################
    # Set expectations on how external calls are made
    def test_call_load_component(self):
        glint.load_tool(tool_path)
        self.comp_mgr.load_component.assert_called_with(
            tool_path, glint.constants.COMPONENT_TYPE.Tool,
            glint.ToolBase, {'pre_load': ANY})

    def test_call_component_initializer(self):
        glint.load_tool(tool_path)
        self.comp_mgr.run_initializer.assert_called_with(
            self.comp_mgr.load_component.return_value)

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_return_loaded_tool_if_successful(self):
        assert (glint.load_tool(tool_path)
                is self.comp_mgr.load_component.return_value)

    def test_unload_component_and_return_None_if_component_loading_raises_exception(self):
        self.comp_mgr.load_component.side_effect = Exception
        assert glint.load_tool(tool_path) is None
        assert self.comp_mgr.unload_component.called

    def test_unload_component_and_return_None_if_initializer_raises_exception(self):
        self.comp_mgr.run_initializer.side_effect = Exception
        assert glint.load_tool(tool_path) is None
        assert self.comp_mgr.unload_component.called


class TestReloadTool(TestBase):
    @pytest.fixture(autouse=True)
    def set_up(self):
        self.get_main.return_value = Mock()

    ###########################################################
    # Set expectations on how external calls are made
    def test_check_first_if_tool_is_currently_loaded(self):
        glint.reload_tool('test-tool')
        self.get_main.assert_called_with('test-tool')

    def test_call_reload_component(self):
        glint.reload_tool('test-tool')
        self.comp_mgr.reload_component.assert_called_with(
            'test-tool', glint.ToolBase)

    def test_rerun_initializer_when_reloading_tool(self):
        glint.reload_tool('test-tool')
        self.comp_mgr.run_initializer.assert_called_with(
            self.comp_mgr.reload_component.return_value
        )

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_return_reloaded_tool_if_successful(self):
        assert (glint.reload_tool('test-tool')
                is self.comp_mgr.reload_component.return_value)

    def test_return_None_if_reloading_tool_raises_exception(self):
        self.comp_mgr.reload_component.side_effect = Exception
        assert glint.reload_tool('test-tool') is None

    def test_return_None_if_reloading_unloaded_tool(self):
        self.get_main.return_value = None
        assert glint.reload_tool('test-tool') is None
        assert not self.comp_mgr.reload_component.called


if __name__ == '__main__':
    pytest.main([__file__])
