import logging
import pytest


@pytest.fixture(scope='session', autouse=True)
def disable_logging():
    # Make sure no logging output during test runs
    logging.disable(logging.CRITICAL)
    yield

    # Restore logging output
    logging.disable(logging.NOTSET)
