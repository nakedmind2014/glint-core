try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import Mock, ANY, patch
except ImportError:
    from mock import patch

import pytest

import glint


@pytest.fixture()
def plugin_root():
    patcher = patch('glint.component.plugin.get_plugin_root',
                    return_value=Path('/glint/plugins'))
    yield patcher.start().return_value
    patcher.stop()


class TestBase:
    @pytest.fixture(autouse=True)
    def base_setup(self):
        patcher1 = patch.object(glint.component.plugin, 'comp_mgr')
        self.comp_mgr = patcher1.start()
        patcher2 = patch('glint.component.plugin.get_main', return_value=None)
        self.get_main = patcher2.start()
        yield

        patcher1.stop()
        patcher2.stop()


class TestLoadPlugin(TestBase):
    ###########################################################
    # Set expectations on how external calls are made
    def test_check_first_if_plugin_is_already_loaded(self):
        glint.load_plugin('test-plugin')
        self.get_main.assert_called_with('test-plugin')

    def test_call_load_component(self, plugin_root):
        glint.load_plugin('test-plugin')
        self.comp_mgr.load_component.assert_called_with(
            plugin_root/'test-plugin', glint.constants.COMPONENT_TYPE.Plugin,
            glint.PluginBase, {'pre_load': ANY})

    def test_call_component_initializer(self):
        glint.load_plugin('test-plugin')
        self.comp_mgr.run_initializer.assert_called_with(
            self.comp_mgr.load_component.return_value)

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_just_return_plugin_object_if_already_loaded(self):
        self.get_main.return_value = Mock()
        assert glint.load_plugin('test-plugin') is self.get_main.return_value
        self.comp_mgr.load_component.assert_not_called()

    def test_return_loaded_plugin_if_successful(self):
        assert (glint.load_plugin('test-plugin')
                is self.comp_mgr.load_component.return_value)

    def test_unload_component_and_reraise_exc_if_component_loading_raises_exception(self):
        self.comp_mgr.load_component.side_effect = ValueError
        with pytest.raises(ValueError):
            glint.load_plugin('test-plugin')
        self.comp_mgr.unload_component.assert_called()

    def test_unload_component_and_reraise_exc_if_initializer_raises_exception(self):
        self.comp_mgr.run_initializer.side_effect = ValueError
        with pytest.raises(ValueError):
            glint.load_plugin('test-plugin')
        self.comp_mgr.unload_component.assert_called()


class TestReloadPlugin(TestBase):
    @pytest.fixture(autouse=True)
    def set_up(self):
        self.get_main.return_value = Mock()

    ###########################################################
    # Set expectations on how external calls are made
    def test_check_first_if_plugin_is_currently_loaded(self):
        glint.reload_plugin('test-plugin')
        self.get_main.assert_called_with('test-plugin')

    def test_call_reload_component(self):
        glint.reload_plugin('test-plugin')
        self.comp_mgr.reload_component.assert_called_with(
            'test-plugin', glint.PluginBase)

    def test_rerun_initializer_when_reloading_plugin(self):
        glint.reload_plugin('test-plugin')
        self.comp_mgr.run_initializer.assert_called_with(
            self.comp_mgr.reload_component.return_value
        )

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_return_reloaded_plugin_if_successful(self):
        assert (glint.reload_plugin('test-plugin')
                is self.comp_mgr.reload_component.return_value)

    def test_return_None_if_reloading_plugin_raises_exception(self):
        self.comp_mgr.reload_component.side_effect = Exception
        assert glint.reload_plugin('test-plugin') is None

    def test_return_None_if_reloading_unloaded_plugin(self):
        self.get_main.return_value = None
        assert glint.reload_plugin('test-plugin') is None
        self.comp_mgr.reload_component.assert_not_called()


if __name__ == '__main__':
    pytest.main([__file__])
