import os
import shutil
import tempfile
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import patch
except ImportError:
    from mock import patch
try:
    reload
except NameError:
    import importlib
    reload = importlib.reload

import pytest
import glint


test_glint_root = Path(tempfile.gettempdir())/'glint-root'
if os.name == 'nt':
    default = Path('C:/glint')
elif os.name == 'posix':
    default = '/usr/local/glint'


class TestGlintRoot:
    def rmtree(self, fp):
        try:
            shutil.rmtree(str(fp))
        except OSError:
            pass

    @pytest.fixture(autouse=True)
    def set_up(self):
        patcher1 = patch('platform.system', return_value='Windows')
        self.system = patcher1.start()

        patcher2 = patch('os.environ.get', return_value=str(test_glint_root))
        self.os_environ = patcher2.start()

        # Reload so cache is reset
        import glint.site; reload(glint.site)
        self.site = glint.site
        try:
            (test_glint_root/'core').mkdir(parents=True)
        except OSError:
            pass

        yield

        patcher1.stop()
        patcher2.stop()

        if test_glint_root.exists():
            self.rmtree(test_glint_root)

    def test_use_GLINT_ROOT_value_if_defined(self):
        assert self.site.get_glint_root() == test_glint_root

    def test_raise_GlintError_if_custom_path_not_exist_and_default_path_not_exist(self):
        # Make sure first that test_glint_root does not exists. If running the
        # tests in a machine with an existing default glint install, temporarily
        # rename the glint install folder so it's not accidentally modified by
        # the test.
        self.rmtree(test_glint_root)
        if default.exists():
            default.rename(default.with_name('glintxxx'))

        with pytest.raises(glint.errors.GlintError):
            self.site.get_glint_root()

        if Path('C:/glintxxx').exists():
            Path('C:/glintxxx').rename(default)

    def test_use_default_path_if_custom_path_not_exist(self):
        self.rmtree(test_glint_root)
        default_exists = True
        if not default.exists():
            (default/'core').mkdir(parents=True)
            default_exists = False

        assert self.site.get_glint_root() == default

        # Do not delete default root in case it already exists before running
        # the tests because it might contain an existing glint install.
        if not default_exists:
            self.rmtree(default)

    def test_raise_GlintError_if_glint_root_does_not_contain_core_folder(self):
        self.rmtree(test_glint_root)
        test_glint_root.mkdir(parents=True)
        with pytest.raises(glint.errors.GlintError):
            self.site.get_glint_root()

    def test_raise_GlintError_if_no_custom_path_is_defined_for_unsupported_platform(self):
        self.system.return_value = 'Mac'
        self.os_environ.return_value = None
        expected_msg = "Unsupported operating system platform"
        with pytest.raises(glint.errors.GlintError, match=expected_msg):
            self.site.get_glint_root()

    def test_subsequent_calls_to_get_glint_root_uses_cache(self):
        self.site.get_glint_root()
        self.os_environ.reset_mock()

        actual = self.site.get_glint_root()
        assert not self.os_environ.called

        # Make sure cached value is a Path instance.
        assert isinstance(actual, Path)

    @patch('glint.site.get_glint_root')
    def test_subsequent_calls_to_get_core_root_uses_cache(self, get_glint_root):
        self.site.get_core_root()
        assert get_glint_root.called
        get_glint_root.reset_mock()

        # Second call will use cache.
        self.site.get_core_root()
        assert not get_glint_root.called


class TestPluginRoot:
    @pytest.fixture(autouse=True)
    def set_up(self):
        # Reload so cache is reset
        import glint.site; reload(glint.site)
        self.site = glint.site

        patcher1 = patch('os.environ.get', return_value='C:/temp/plugins')
        self.os_environ = patcher1.start()

        patcher2 = patch('glint.site.get_glint_root', return_value=Path('C:/glint'))
        patcher2.start()

        yield

        patcher1.stop()
        patcher2.stop()

    def test_use_GLINT_PLUGIN_ROOT_value_if_defined(self):
        assert self.site.get_plugin_root() == Path('C:/temp/plugins')

    def test_use_glint_install_plugins_folder_if_env_var_not_set(self):
        self.os_environ.return_value = None
        assert self.site.get_plugin_root() == Path('C:/glint/plugins')

    def test_subsequent_calls_to_get_plugin_root_uses_cache(self):
        self.site.get_plugin_root()
        self.os_environ.reset_mock()

        actual = self.site.get_plugin_root()
        assert not self.os_environ.called

        # Make sure cached value is a Path instance.
        assert isinstance(actual, Path)


class TestToolRoot:
    @pytest.fixture(autouse=True)
    def set_up(self):
        # Reload so cache is reset
        import glint.site; reload(glint.site)
        self.site = glint.site

        patcher1 = patch('glint.site.get_glint_root', return_value=Path('C:/glint'))
        patcher1.start()

        patcher2 = patch('os.environ.get', return_value='C:/temp/tools')
        self.os_environ = patcher2.start()

        yield

        patcher1.stop()
        patcher2.stop()

    def test_use_GLINT_TOOL_ROOT_value_if_defined(self):
        assert self.site.get_tool_root() == Path('C:/temp/tools')

    def test_use_glint_install_tools_folder_if_env_var_not_set(self):
        self.os_environ.return_value = None
        assert self.site.get_tool_root() == Path('C:/glint/tools')

    def test_subsequent_calls_to_get_tool_root_uses_cache(self):
        self.site.get_tool_root()
        self.os_environ.reset_mock()

        actual = self.site.get_tool_root()
        assert not self.os_environ.called

        # Make sure cached value is a Path instance.
        assert isinstance(actual, Path)


class TestGlintHome:
    def get_env(self, name):
        if name == 'GLINT_HOME':
            return self.glint_home
        elif name == 'APPDATA':
            return 'C:\\temp'
        elif name == 'HOME':
            return '/tmp'

    @pytest.fixture(autouse=True)
    def set_up(self):
        self.glint_home = None

        patcher1 = patch('platform.system', return_value='Windows')
        self.system = patcher1.start()

        patcher2 = patch('os.environ.get', side_effect=self.get_env)
        self.os_environ = patcher2.start()

        # Reload so cache is reset
        import glint.site; reload(glint.site)
        self.site = glint.site

        yield

        patcher1.stop()
        patcher2.stop()

    def test_use_GLINT_HOME_value_if_defined(self):
        self.glint_home = 'C:/temp'
        assert self.site.get_glint_home() == Path('C:/temp')

    def test_windows_default_if_custom_path_not_defined(self):
        """windows default: %APPDATA%/glint"""
        assert self.site.get_glint_home() == Path('C:/temp/glint')

    def test_linux_default_if_custom_path_not_defined(self):
        """linux default: $HOME/.glint"""
        self.system.return_value = 'Linux'
        assert self.site.get_glint_home() == Path('/tmp/.glint')

    def test_raise_GlintError_if_no_custom_path_is_defined_for_unsupported_platform(self):
        self.system.return_value = 'Mac'
        expected_msg = "Unsupported operating system platform"
        with pytest.raises(glint.errors.GlintError, match=expected_msg):
            self.site.get_glint_home()

    def test_subsequent_calls_to_get_glint_home_uses_cache(self):
        self.site.get_glint_home()
        self.os_environ.reset_mock()

        actual = self.site.get_glint_home()
        assert not self.os_environ.called

        # Make sure cached value is a Path instance.
        assert isinstance(actual, Path)


if __name__ == '__main__':
    pytest.main([__file__])
