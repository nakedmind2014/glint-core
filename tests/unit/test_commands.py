try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock

import pytest

import glint
import glint.commands as gcmds
from glint.api.commands import (register_command, get_command_handler,
                                _command_registry)


@pytest.fixture(autouse=True)
def clear():
    # Clear registry before each run. Not ideal since it depends
    # on implementation detail.
    _command_registry.clear()


def test_call_registered_command_via_commands_namespace():
    handler = MagicMock()
    register_command('test_command', handler)
    gcmds.test_command()
    assert handler.called


def test_raise_GlintError_if_command_name_already_registered_and_force_flag_disabled():
    register_command('mock_command', MagicMock())
    with pytest.raises(glint.errors.GlintError):
        register_command('mock_command', MagicMock(), force=False)


def test_replace_existing_command_handler_if_force_flag_enabled():
    register_command('mock_command', MagicMock())
    new_handler = MagicMock()
    register_command('mock_command', new_handler, force=True)
    assert get_command_handler('mock_command') is new_handler


def test_raise_GlintError_if_unregistered_command_is_called():
    with pytest.raises(glint.errors.GlintError):
        getattr(gcmds, 'test_cmd1')


def test_raise_GlintError_if_command_has_no_registered_handler():
    register_command('test_command')
    with pytest.raises(glint.errors.GlintError):
        getattr(gcmds, 'test_command')


if __name__ == '__main__':
    pytest.main([__file__])
