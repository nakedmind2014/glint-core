import pytest

import glint.errors as gerr
import glint.api.errors as api_err


@pytest.fixture(autouse=True)
def clear():
    # Clear registry before each run
    api_err._error_registry.clear()
    api_err._error_registry.update({
        'GlintError': api_err.GlintError,
        'GlintRPCError': api_err.GlintRPCError
    })


def test_no_need_to_register_core_exceptions():
    with pytest.raises(api_err.GlintError):
        raise gerr.GlintError
    with pytest.raises(api_err.GlintRPCError):
        raise gerr.GlintRPCError


def test_raising_registered_exception_via_errors_namespace():
    class TestError(Exception):
        pass

    api_err.register_exception(TestError)
    with pytest.raises(TestError):
        raise gerr.TestError


def test_supports_from_import_syntax():
    class TestError(Exception):
        pass

    api_err.register_exception(TestError)
    from glint.errors import TestError


def test_does_not_support_normal_import_syntax():
    class TestError(Exception):
        pass

    api_err.register_exception(TestError)
    with pytest.raises(ModuleNotFoundError):
        import glint.errors.TestError


def test_raise_GlintError_if_exception_of_same_name_already_registered():
    class TestError(Exception):
        pass

    api_err.register_exception(TestError)
    msg = "Custom exception with name 'TestError' is already registered"
    with pytest.raises(api_err.GlintError, match=msg):
        api_err.register_exception(TestError)


def test_raise_GlintError_if_exception_name_is_reserved():
    class GlintError(Exception):
        pass

    msg = "Cannot override glint core exception 'GlintError'"
    with pytest.raises(api_err.GlintError, match=msg):
        api_err.register_exception(GlintError)


def test_raise_GlintError_if_unregistered_exception_is_raised():
    msg = "Custom exception with name 'TestError' is not registered"
    with pytest.raises(api_err.GlintError, match=msg):
        raise gerr.TestError


if __name__ == '__main__':
    pytest.main([__file__])
