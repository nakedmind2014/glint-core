import six
from datetime import date, datetime
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import patch, MagicMock
except ImportError:
    from mock import patch, MagicMock

import pytest
import simplejson as json
import glint


class TestProcessRPCRequest:
    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.api.commands import _command_registry
        from glint.api.remote import process_rpc_request, rpc_registry

        # Clear registry before each run. Not ideal since it depends
        # on implementation detail.
        _command_registry.clear()
        rpc_registry.clear()

        self.cmd_registry = _command_registry
        self.process_rpc_request = process_rpc_request
        self.rpc_registry = rpc_registry

    def test_call_function(self):
        """ Run function registered in rpc registry """
        command = MagicMock(return_value=2)
        self.rpc_registry['test_command'] = {'command': command}
        rpc_data = {'command': 'test_command', 'args': [1], 'kwargs': {'x': 1}}
        actual = self.process_rpc_request(rpc_data)

        assert actual == b'{"result":2}'
        command.assert_called_with(1, x=1)

    def test_call_registered_command(self):
        """ Return handler output if registered command is called remotely. """
        handler = MagicMock(return_value=2)
        glint.register_command('test_command', handler)
        rpc_data = {'command': 'test_command', 'args': [1], 'kwargs': {'x': 1}}
        actual = self.process_rpc_request(rpc_data)

        assert actual == b'{"result":2}'
        handler.assert_called_with(1, x=1)

    def test_call_unknown_command_or_function(self):
        rpc_data = {'command': 'unknown_command'}
        actual = json.loads(self.process_rpc_request(rpc_data))

        expected = {'error': {'msg': 'Command name unknown_command is not registered.',
                              'exc': 'glint.GlintError'}}
        assert actual == expected

    def test_command_exception(self):
        """ Return exception info if command raises an exception. """
        glint.register_command('test_command', lambda: 1/0)
        rpc_data = {'command': 'test_command'}
        actual = json.loads(self.process_rpc_request(rpc_data))

        if six.PY3:
            expected = {'error': {'msg': 'division by zero', 'exc': 'ZeroDivisionError'}}
        elif six.PY2:
            expected = {'error': {'msg': 'integer division or modulo by zero',
                                  'exc': 'exceptions.ZeroDivisionError'}}
        assert actual == expected

    def test_path_objects(self):
        handler = MagicMock(return_value=Path('C:/temp'))
        glint.register_command('test_command', handler)
        rpc_data = {'command': 'test_command'}

        actual = json.loads(self.process_rpc_request(rpc_data))
        expected = {'result': {'__type__': 'path', '__data__': 'C:\\temp'}}
        assert actual == expected

    def test_date_objects(self):
        handler = MagicMock(return_value=date(2018, 4, 27))
        glint.register_command('test_command', handler)
        rpc_data = {'command': 'test_command'}

        actual = json.loads(self.process_rpc_request(rpc_data))
        expected = {'result': {'__type__': 'date', '__data__': '2018-04-27'}}
        assert actual == expected

    def test_datetime_objects(self):
        handler = MagicMock(return_value=datetime(2018, 4, 27))
        glint.register_command('test_command', handler)
        rpc_data = {'command': 'test_command'}

        actual = json.loads(self.process_rpc_request(rpc_data))
        expected = {'result': {'__type__': 'datetime', '__data__': '2018-04-27T00:00:00'}}
        assert actual == expected


@patch('glint.api.remote.get_setting', return_value='localhost')
@patch('glint.api.remote.requests.post')
class TestRPCStub:
    @pytest.fixture(autouse=True)
    def set_up(self):
        from glint.api.remote import rpc_stub, rpc_registry, headers
        self.headers = headers
        self.rpc_stub = rpc_stub
        self.rpc_registry = rpc_registry
        rpc_registry = {'test_command': {}}

    @pytest.mark.parametrize('server', [None, 'farm_rpc_server'])
    def test_server_successful_post(self, post, get_setting, server):
        servers = {'default_rpc_server': 'localhost', 'farm_rpc_server': 'gollum'}
        get_setting.side_effect = lambda x: servers[x]
        post.return_value.status_code = 200
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': server}
        actual = self.rpc_stub(1, 2, x=1, y=2)

        expected = {'command': 'test_command', 'args': [1, 2],
                    'kwargs': {'x': 1, 'y': 2}}
        k = server or 'default_rpc_server'
        post.assert_called_with('http://%s/runcmd' % servers[k],
                                data=json.dumps(expected, separators=(',', ':')).encode('utf-8'),
                                headers=self.headers)
        # Return the 'result' section of the response data
        assert actual == post.return_value.json.return_value['result']

    def test_server_unavailable(self, post, _):
        post.return_value.status_code = 500
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': None}

        # raise exception
        errmsg = 'RPC service unavailable. Unable to connect to localhost.'
        with pytest.raises(glint.GlintRPCError, match=errmsg):
            self.rpc_stub()

    def test_unknown_server_error(self, post, _):
        errmsg = 'This is an unknown server error.'
        post.return_value.status_code = 'unknown_code'
        post.return_value.text = errmsg
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': None}

        # raise exception
        with pytest.raises(glint.GlintRPCError, match=errmsg):
            self.rpc_stub()

    def test_specific_cmd_exception_in_server(self, post, _):
        if six.PY3:
            ret_val = {'error': {'exc': 'KeyError', 'msg': 'unknown key'}}
        elif six.PY2:
            ret_val = {'error': {'exc': 'exceptions.KeyError', 'msg': 'unknown key'}}
        post.return_value.status_code = 200
        post.return_value.json.return_value = ret_val
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': None}

        # raise exception
        errmsg = 'unknown key'
        with pytest.raises(KeyError, match=errmsg):
            self.rpc_stub()

    def test_generic_cmd_exception_in_server(self, post, _):
        if six.PY3:
            ret_val = {'error': {'exc': 'Exception', 'msg': 'unknown error'}}
        elif six.PY2:
            ret_val = {'error': {'exc': 'exceptions.Exception', 'msg': 'unknown error'}}
        post.return_value.status_code = 200
        post.return_value.json.return_value = ret_val
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': None}

        # raise exception
        errmsg = 'unknown error'
        with pytest.raises(Exception, match=errmsg):
            self.rpc_stub()

    def test_local_type_error_exception(self, post, _):
        """ Catch local TypeError exceptions generated when trying to raise locally
        the exception generated when command was executed in server"""
        post.return_value.status_code = 200
        # Exception given is invalid.
        post.return_value.json.return_value = {'error': {'exc': 'glint.commands',
                                                         'msg': ''}}
        self.rpc_stub.__name__ = 'test_command'
        self.rpc_registry['test_command'] = {'server': None}

        # raise exception
        errmsg = 'TypeError exception generated'
        with pytest.raises(glint.GlintRPCError, match=errmsg):
            self.rpc_stub()

    @pytest.mark.skip('not yet implemented')
    def test_cmd_error_with_no_exception_info(self, *args):
        pass

    def test_glint_remote(self, post, _):
        post.return_value.status_code = 200
        glint.remote.test_command()
        expected = {'command': 'test_command', 'args': [], 'kwargs': {}}
        post.assert_called_with('http://localhost/runcmd',
                                data=json.dumps(expected, separators=(',', ':')).encode('utf-8'),
                                headers=self.headers)


class CustomDataClass:
    data = []

    def __init__(self, data):
        self.data = data

    def __eq__(self, obj):
        return self.data == obj.data

    def serialize(self):
        return json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(data):
        return CustomDataClass(json.loads(data))


class TestSerialize:
    def test_path(self):
        actual = glint.api.remote.serialize(Path('C:/temp'))
        if six.PY3:
            expected = b'{"__type__":"path","__data__":"C:\\\\temp"}'
        elif six.PY2:
            expected = '{"__data__":"C:\\\\temp","__type__":"path"}'
        assert actual == expected

    def test_date(self):
        actual = glint.api.remote.serialize(date(2018, 4, 27))
        if six.PY3:
            expected = b'{"__type__":"date","__data__":"2018-04-27"}'
        elif six.PY2:
            expected = '{"__data__":"2018-04-27","__type__":"date"}'
        assert actual == expected

    def test_datetime(self):
        actual = glint.api.remote.serialize(datetime(2018, 4, 27))
        if six.PY3:
            expected = b'{"__type__":"datetime","__data__":"2018-04-27T00:00:00"}'
        elif six.PY2:
            expected = '{"__data__":"2018-04-27T00:00:00","__type__":"datetime"}'
        assert actual == expected

    def test_custom_object(self):
        glint.api.remote.register_rpc_object(
            'CustomDataClass',
            CustomDataClass,
            serializer=lambda x: x.serialize()
        )
        actual = glint.api.remote.serialize(CustomDataClass([1, 2, 3]))
        if six.PY3:
            expected = b'{"__type__":"CustomDataClass","__data__":"[1,2,3]"}'
        elif six.PY2:
            expected = '{"__data__":"[1,2,3]","__type__":"CustomDataClass"}'
        assert actual == expected


class TestDeserialize:
    def test_path(self):
        data = '{"__type__":"path","__data__":"C:\\\\temp"}'
        actual = glint.api.remote.deserialize(data)
        assert actual == Path('C:/temp')

    def test_date(self):
        data = '{"__type__":"date","__data__":"2018-04-27"}'
        actual = glint.api.remote.deserialize(data)
        assert actual == date(2018, 4, 27)

    def test_datetime(self):
        data = '{"__type__":"datetime","__data__":"2018-04-27T00:00:00"}'
        actual = glint.api.remote.deserialize(data)
        assert actual == datetime(2018, 4, 27)

    def test_custom_object(self):
        glint.api.remote.register_rpc_object(
            'CustomDataClass',
            CustomDataClass,
            deserializer=lambda x: CustomDataClass.deserialize(x)
        )
        data = '{"__type__":"CustomDataClass","__data__":"[1,2,3]"}'
        actual = glint.api.remote.deserialize(data)
        assert actual == CustomDataClass([1, 2, 3])


if __name__ == '__main__':
    pytest.main([__file__])
