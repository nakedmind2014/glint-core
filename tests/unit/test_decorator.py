try:
    from unittest.mock import patch
except ImportError:
    from mock import patch

import pytest


@patch('glint.api.decorator.rpc_endpoint', return_value=False)
@patch('glint.api.decorator.rpc_stub')
class TestRPCDecorator:
    @pytest.fixture(autouse=True)
    def set_up(self):
        import glint.api.decorator as deco
        self.use_rpc = deco.use_rpc
        self.rpc_registry = deco.rpc_registry
        self.rpc_registry.clear()

    def test_invalid_keyword_args(self, rpc_stub, *args):
        """ Raise exception """
        with pytest.raises(ValueError):
            @self.use_rpc(invalid_kwarg=1)
            def test():
                pass
        assert not rpc_stub.called

    def test_specify_rpc_server(self, rpc_stub, rpc_endpoint):
        """ Copy server info to rpc registry """
        @self.use_rpc(server='server_name')
        def testcmd():
            pass
        assert self.rpc_registry['testcmd']['server'] == 'server_name'

    def test_no_keyword_args(self, rpc_stub, *args):
        """ Use default rpc server """
        @self.use_rpc()
        def testcmd():
            pass
        assert self.rpc_registry['testcmd']['server'] is None

    def test_plain_decorator(self, rpc_stub, *args):
        """ Use default rpc server """
        @self.use_rpc
        def testcmd():
            pass
        assert self.rpc_registry['testcmd']['server'] is None

    def test_in_client(self, rpc_stub, rpc_endpoint):
        """ Call rpc stub with the given arguments """
        rpc_endpoint.return_value = False
        @self.use_rpc
        def testcmd(x, y=2):
            return x, y

        testcmd(1, y=5)
        rpc_stub.assert_called_with(1, y=5)

    def test_in_rpc_server(self, rpc_stub, rpc_endpoint):
        """ Invoke the decorated command directly when running in server """
        rpc_endpoint.return_value = True
        @self.use_rpc
        def testcmd(arg):
            return arg

        assert testcmd(100) == 100
        assert not rpc_stub.called


if __name__ == '__main__':
    pytest.main([__file__])
