from __future__ import unicode_literals
import os
import imp
import shutil
import logging
import weakref
import tempfile
import textwrap
import simplejson as json
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import MagicMock, PropertyMock, ANY, call, patch, mock_open
except ImportError:
    from mock import MagicMock, PropertyMock, ANY, call, patch, mock_open

import pytest
import glint


test_glint_root = Path(tempfile.gettempdir())/'glint-root'

# malformed manifest has extra comma.
malformed_manifest = """{
    "name": "test-plugin",
}"""

invalid_manifest1 = """{
    "version": "0.0.1"
}"""

invalid_manifest2 = """{
    "name": "test-plugin"
}"""

plugin_manifest = json.OrderedDict({
    'name': 'test-plugin',
    'version': '0.0.1',
    'requires': {'core': '0.0.1', 'test-depend-plugin-1': '0.0.1', 'test-depend-plugin-2': '0.0.1'},
    'settings': {'verbose_level': {'type': 'int','private': True}, 'log_dir': {'type': 'path'}}
})

mock_type = MagicMock()


@pytest.fixture(scope='module', autouse=True)
def test_setup():
    # Disable side effects when importing glint
    os.environ['GLINT_DISABLE_INIT'] = '1'
    # Make sure to start with an empty test dir
    try:
        shutil.rmtree(str(test_glint_root))
    except WindowsError:
        pass

    # Create the test files
    root = test_glint_root/'plugins'
    test_plugin_code = 'import glint\nclass TestPlugin(glint.PluginBase):pass'
    (root/'test-plugin/python').mkdir(parents=True)
    (root/'test-plugin/.meta').write_text(json.dumps(plugin_manifest))
    (root/'test-plugin/main.py').write_text(test_plugin_code)
    (root/'test-plugin/python/__init__.py').write_text('')

    (root/'test-plugin-no-init/python').mkdir(parents=True)
    (root/'test-plugin-no-init/.meta').write_text(json.dumps(plugin_manifest))
    (root/'test-plugin-no-init/main.py').write_text(test_plugin_code)

    (root/'test-plugin-no-main/python').mkdir(parents=True)
    (root/'test-plugin-no-main/.meta').write_text(json.dumps(plugin_manifest))
    (root/'test-plugin-no-main/python/__init__.py').write_text('')

    (root/'test-plugin-no-manifest/python').mkdir(parents=True)
    (root/'test-plugin-no-manifest/main.py').write_text(test_plugin_code)
    (root/'test-plugin-no-manifest/python/__init__.py').write_text('')
    yield

    # Tear down
    del os.environ['GLINT_DISABLE_INIT']
    try:
        shutil.rmtree(str(test_glint_root))
    except OSError:
        pass


def create_component_mock(name, settings):
    mock_comp = MagicMock(spec=glint.component.Component)
    mock_comp.name = name
    mock_comp.get_setting.side_effect = lambda *x: settings.get(x[0])
    return mock_comp


class ComponentTestBase:
    @pytest.fixture(autouse=True)
    def set_up(self):
        patchers = []
        self.comp_mgr = glint.component.ComponentManager()

        # Clear registries before each run. Not ideal since it depends
        # on implementation detail.
        self.comp_mgr._component_registry_n.clear()
        self.comp_mgr._component_registry_l.clear()
        self.comp_mgr._component_registry_i.clear()
        self.comp_mgr._main_modules.clear()

        patcher = patch('glint.component.get_main_class',
                        return_value=glint.component.Component)
        patcher.start()
        patchers.append(patcher)

        patcher = patch('glint.component.import_file')
        patcher.start()
        patchers.append(patcher)

        patcher = patch('glint.component.register_hooks')
        patcher.start()
        patchers.append(patcher)

        patcher = patch('glint.component.load_settings')
        patcher.start()
        patchers.append(patcher)

        yield

        for p in patchers:
            p.stop()

        # Clear logging configuration for next run
        logging.shutdown()
        logging.root.manager.loggerDict.clear()


class TestComponentManager_LoadComponent(ComponentTestBase):
    ###########################################################
    # Internal
    def test_raise_GlintError_if_nonexistent_component_location(self):
        location = test_glint_root/'plugins/test-plugin-not-exist'
        expected_msg = 'Component location .* does not exist.'
        with pytest.raises(glint.errors.GlintError, match=expected_msg):
            self.comp_mgr.load_component(location, mock_type, MagicMock())

    def test_raise_GlintError_if_no_main_module(self):
        location = test_glint_root/'plugins/test-plugin-no-main'
        expected_msg = 'Entry point was not found for component '
        with pytest.raises(glint.errors.GlintError, match=expected_msg):
            self.comp_mgr.load_component(location, mock_type, MagicMock())

    def test_raise_GlintError_if_no_manifest(self):
        location = test_glint_root/'plugins/test-plugin-no-manifest'
        expected_msg = 'Manifest was not found for component '
        with pytest.raises(glint.errors.GlintError, match=expected_msg):
            self.comp_mgr.load_component(location, mock_type, MagicMock())

    def test_raise_GlintError_if_manifest_contains_malformed_json(self):
        with patch('glint.component.io.open', mock_open(read_data=malformed_manifest)):
            expected_msg = 'Error reading manifest of component '
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())

    def test_raise_GlintError_if_invalid_manifest__component_name_not_defined(self):
        with patch('glint.component.io.open', mock_open(read_data=invalid_manifest1)):
            expected_msg = 'Invalid component manifest. Missing entry: name.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())

    def test_raise_GlintError_if_invalid_manifest__component_version_not_defined(self):
        with patch('glint.component.io.open', mock_open(read_data=invalid_manifest2)):
            expected_msg = 'Invalid component manifest. Missing entry: version.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())

    def test_load_component_dependencies_based_on_manifest_declaration_order(self):
        """The order in which component dependencies are declared in manifest
        file is preserved when manifest is loaded.
        """
        content = json.dumps(plugin_manifest)
        with patch('glint.component.io.open', mock_open(read_data=content)):
            callback = MagicMock()
            self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock(),
                                         callbacks={'pre_load': callback})
            actual = list(callback.call_args[0][0].get('requires'))
            assert actual == ['core', 'test-depend-plugin-1', 'test-depend-plugin-2']

    def test_combine_builtin_settings_definition_with_manifest_settings_definition(self):
        """Built-in log_level definition is combined with settings def in manifest."""
        content = json.dumps(plugin_manifest)
        with patch('glint.component.io.open', mock_open(read_data=content)):
            self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())
            actual_keys = glint.component.load_settings.call_args[0][1].keys()
            assert 'log_level' in actual_keys

    ###########################################################
    # Set expectations on how external calls are made
    def test_load_component_settings(self):
        content = json.dumps(plugin_manifest)
        with patch('glint.component.io.open', mock_open(read_data=content)):
            self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())
            glint.component.load_settings.assert_called_once_with('test-plugin', ANY, {})

    def test_invoke_preload_callback_if_provided(self, instance_matcher):
        """The pre_load callback is called with the manifest as argument."""
        content = json.dumps(plugin_manifest)
        with patch('glint.component.io.open', mock_open(read_data=content)):
            callback = MagicMock()
            self.comp_mgr.load_component(
                MagicMock(), mock_type, MagicMock(), callbacks={'pre_load': callback})
            callback.assert_called_with(instance_matcher(json.OrderedDict))

    def test_register_hooks_declared_in_the_manifest(self, instance_matcher):
        content = json.dumps(plugin_manifest)
        manifest = instance_matcher(json.OrderedDict)
        location = test_glint_root/'plugins/test-plugin'
        with patch('glint.component.io.open', mock_open(read_data=content)):
            self.comp_mgr.load_component(location, mock_type, MagicMock())
            glint.component.register_hooks.assert_called_once_with(manifest, location/'hooks')

    ###########################################################
    # Verify how return values from collaborators are handled
    def test_do_not_handle_preload_callback_exception(self):
        callbacks = {'pre_load': MagicMock(side_effect=Exception)}
        with pytest.raises(Exception):
            self.comp_mgr.load_component(MagicMock(), mock_type, glint.PluginBase, callbacks)

    def test_return_loaded_component_if_successful(self):
        location = test_glint_root/'plugins/test-plugin'
        obj = self.comp_mgr.load_component(location, mock_type, glint.PluginBase)
        # The loader returns the entry point object.
        assert isinstance(obj, glint.component.Component)

    def test_register_as_current_engine_if_engine_component_type(self):
        location = test_glint_root/'plugins/test-plugin'
        obj = self.comp_mgr.load_component(
            location, glint.constants.COMPONENT_TYPE.Engine, glint.PluginBase)
        # The loader returns the entry point object.
        assert self.comp_mgr.get_current_engine() is obj

    ###########################################################
    # logging setup override
    def test_override_logging_setup__valid_loglevel__override_global(self):
        # Set initial global log level to INFO
        logging.getLogger('glint').setLevel(logging.INFO)
        mock_comp = create_component_mock('test-comp', {'log_level': 'debug'})
        self.comp_mgr._override_logging_setup(mock_comp)
        # Component setting overrides global setting
        actual = logging.getLogger('glint.test-comp').getEffectiveLevel()
        assert actual == logging.DEBUG

    def test_override_logging_setup__invalid_level__use_global(self):
        # Set global log level to INFO
        logging.getLogger('glint').setLevel(logging.INFO)
        mock_comp = create_component_mock('test-comp', {'log_level': 'invalid'})
        self.comp_mgr._override_logging_setup(mock_comp)
        # Component setting uses global setting
        actual = logging.getLogger('glint.test-comp').getEffectiveLevel()
        assert actual == logging.INFO


@pytest.mark.skip(reason="To be implemented")
class TestComponentManager_RunInitializer(ComponentTestBase):
    def test_call_initializer_if_component_module_has_one(self):
        # check that component instance is passed to initializer
        pass

    def test_temporarily_insert_component_python_path_when_calling_initializer(self):
        pass

    def test_pass_init_args_to_initializer(self):
        pass


class TestComponentManager_Registry(ComponentTestBase):
    def test_component_added_to_registry_if_loading_successful(self):
        location = test_glint_root/'plugins/test-plugin'
        obj = self.comp_mgr.load_component(location, mock_type, glint.PluginBase)
        # Component registry has an entry for the loaded component
        assert glint.get_main(component_name='test-plugin') is obj

    def test_component_main_module_added_to_registry_if_loading_successful(self):
        location = test_glint_root/'plugins/test-plugin'
        self.comp_mgr.load_component(location, mock_type, glint.PluginBase)
        # Module registry has an entry for the component's main module
        assert self.comp_mgr.get_main_module('test-plugin') is not None

    def test_component_not_added_to_registry_if_loading_unsuccessful(self):
        with patch('glint.component.io.open', mock_open(read_data=invalid_manifest1)):
            expected_msg = 'Invalid component manifest. Missing entry: name.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())
        assert glint.get_main(component_name='test-plugin') is None

    def test_component_module_not_added_to_registry_if_loading_unsuccessful(self):
        with patch('glint.component.io.open', mock_open(read_data=invalid_manifest1)):
            expected_msg = 'Invalid component manifest. Missing entry: name.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_component(MagicMock(), mock_type, MagicMock())
        assert self.comp_mgr.get_main_module('test-plugin') is None


class TestComponentManager(ComponentTestBase):
    def test_only_one_instance_of_component_manager_is_created(self):
        mgr1 = glint.component.get_component_manager()
        mgr2 = glint.component.get_component_manager()
        assert mgr1 is mgr2

    @pytest.mark.skip(reason="To be implemented")
    def test_return_loaded_components(self):
        pass

    # --------------------------------------------------------------------------
    #   load_core_manifest
    # --------------------------------------------------------------------------

    def test_load_core_manifest__malformed_manifest(self):
        with patch('glint.component.io.open', mock_open(read_data=malformed_manifest)):
            expected_msg = 'Error reading core manifest.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_core_manifest()

    def test_load_core_manifest__invalid_manifest__missing_version(self):
        with patch('glint.component.io.open', mock_open(read_data=invalid_manifest2)):
            expected_msg = 'Invalid core manifest. Missing entry: version.'
            with pytest.raises(glint.errors.GlintError, match=expected_msg):
                self.comp_mgr.load_core_manifest()


class TestComponentGetLogger:
    @pytest.fixture(autouse=True)
    def set_up(self):
        patchers = []
        self.location = test_glint_root/'plugins/test-plugin'
        self.component = glint.component.Component()

        patcher = patch('inspect.stack')
        self.stack = patcher.start()
        patchers.append(patcher)

        patcher = patch('glint.component.Component.location', new_callable=PropertyMock)
        patcher.start().return_value = self.location
        patchers.append(patcher)

        patcher = patch('glint.component.Component.name', new_callable=PropertyMock)
        patcher.start().return_value = 'test-plugin'
        patchers.append(patcher)

        yield

        for p in patchers:
            p.stop()

    def test_get_logger__main(self):
        self.stack.return_value = ['_', ['_', str(self.location)+'/main.py']]
        assert self.component.get_logger().name == 'glint.test-plugin.main'

    def test_get_logger__toplevel_script(self):
        self.stack.return_value = ['_', ['_', str(self.location)+'/run.py']]
        assert self.component.get_logger().name == 'glint.test-plugin.run'

    def test_get_logger__lib_module(self):
        self.stack.return_value = ['_', ['_', str(self.location)+'/python/package/subpackage/test_file.py']]
        assert self.component.get_logger().name == 'glint.test-plugin.lib.package.subpackage.test_file'

    def test_get_logger__other_modules__normalized_case(self):
        self.stack.return_value = ['_', ['_', str(self.location)+'/startup/userSetup.py']]
        assert self.component.get_logger().name == 'glint.test-plugin.startup.userSetup'

    def test_get_logger__not_component_module(self):
        self.stack.return_value = ['_', ['_', 'C:/temp/userSetup.py']]
        assert self.component.get_logger().name == 'root'


class TestUnloadComponent:
    @pytest.fixture(autouse=True)
    def set_up(self):
        self.comp_mgr = glint.component.ComponentManager()

        patcher1 = patch('glint.component.get_main_class')
        patcher1.start().return_value = glint.component.Component

        self.location = test_glint_root/'plugins/test-plugin'
        patcher2 = patch('glint.component.Component.location', new_callable=PropertyMock)
        patcher2.start().return_value = self.location

        yield

        patcher1.stop()
        patcher2.stop()

    def test_remove_registry_references_to_unloaded_module(self):
        self.comp_mgr.load_component(self.location, mock_type, glint.PluginBase)
        wr_main = weakref.ref(self.comp_mgr._component_registry_n['test_plugin']['main'])
        wr_meta = weakref.ref(self.comp_mgr._component_registry_n['test_plugin']['manifest'])
        wr_mod = weakref.ref(self.comp_mgr._main_modules['test_plugin'])

        self.comp_mgr.unload_component('test-plugin')
        assert wr_main() is None
        assert wr_meta() is None
        # This fails in a test environment because, for some reason, two objects
        # still reference the module object. But checking in a test script in
        # which a component is loaded and then unloaded, the module is actually
        # deleted. So I think this is safe to ignore.
        # assert wr_mod() is None

    def test_reset_current_engine_if_unloading_engine_component(self):
        self.comp_mgr.load_component(
            self.location, glint.constants.COMPONENT_TYPE.Engine,
            glint.PluginBase)
        self.comp_mgr.unload_component('test-plugin')
        assert self.comp_mgr.get_current_engine() is None


@pytest.mark.skip("Dont know yet how to implement these tests")
class TestReloadComponent:
    def test(self):
        self.comp_mgr.reload_component('test-plugin', glint.PluginBase)


@pytest.mark.skip("To be implemented")
class TestImportLib:
    def test(self):
        pass


class TestGetMain(ComponentTestBase):
    def test_underscore_and_hyphen_are_treated_the_same(self):
        location = test_glint_root/'plugins/test-plugin'
        c_main = self.comp_mgr.load_component(location, mock_type, glint.PluginBase)
        assert glint.get_main('test-plugin') is c_main
        assert glint.get_main('test_plugin') is c_main

    def test_return_None_if_name_not_found(self):
        assert glint.get_main('test-plugin') is None

    @patch('inspect.stack')
    def test_use_caller_location_if_name_not_specified(self, stack):
        location = test_glint_root/'plugins/test-plugin'
        stack.return_value = ['_', ['_', str(location)+'/python/module.py']]
        c_main = self.comp_mgr.load_component(location, mock_type, glint.PluginBase)
        assert glint.get_main() is c_main

    @patch('inspect.stack')
    def test_return_None_if_name_not_specified_and_caller_not_inside_component_folder(self, stack):
        stack.return_value = ['_', ['_', 'path/to/non-component/module']]
        assert glint.get_main() is None


class TestGetMainClass:
    @pytest.fixture(autouse=True)
    def set_up(self):
        code = 'class Subklass(list): pass'
        self.module = imp.new_module('module')
        self.module.__file__ = 'module.py'
        exec(code, self.module.__dict__)

    def test_raise_GlintError_if_baseclass_subclass_not_found(self):
        with pytest.raises(glint.errors.GlintError):
            glint.component.get_main_class(self.module, str)

    def test_return_subclass_of_given_baseclass(self):
        actual = glint.component.get_main_class(self.module, list)
        assert actual is self.module.Subklass

    def test_return_subclass_of_given_baseclass_even_with_mixin(self):
        code = textwrap.dedent("""
        class SubklassMixin(object):
            pass
        class Subklass(SubklassMixin, list):
            pass
        """)
        self.module = imp.new_module('module')
        self.module.__file__ = 'module.py'
        exec(code, self.module.__dict__)
        actual = glint.component.get_main_class(self.module, list)
        assert actual is self.module.Subklass


class TestRegisterComponentPaths:
    @pytest.fixture(autouse=True)
    def set_up(self):
        patchers = []
        self.location = test_glint_root/'plugins/test-plugin'

        patcher = patch('platform.python_version_tuple', return_value=('3', '6', '3'))
        self.python_version_tuple = patcher.start()
        patchers.append(patcher)

        patcher = patch('platform.system', return_value='Windows')
        self.system = patcher.start()
        patchers.append(patcher)

        patcher = patch('glint.component.register_path')
        self.register_path = patcher.start()
        patchers.append(patcher)

        yield

        for p in patchers:
            p.stop()

    @patch('glint.component.Path.exists', return_value=True)
    def test_include_vendor_bin_in_path(self, _):
        bin_dir = str(self.location/'python/vendor_py36_win64/bin')
        glint.component.register_component_paths(self.location)
        assert os.environ['PATH'].startswith(bin_dir), "Vendor bin folder not added to PATH"

    def test_platform_specific_vendor_folder_has_higher_priority(self):
        self.system.return_value = 'Linux'
        self.python_version_tuple.return_value = '2', '7', '8'
        glint.component.register_component_paths(self.location)
        # Linux-specific vendor folder is searched first
        expected = (call(self.location/'python/vendor_py27_linux/lib'),
                    call(self.location/'python/vendor_common_lib'))
        self.register_path.assert_has_calls(expected, any_order=False)


if __name__ == '__main__':
    pytest.main([__file__])
