from __future__ import unicode_literals
import platform


vendor_dirname = 'vendor_py%s_win64' % ''.join(platform.python_version_tuple()[:2])

glint_settings = """
[test-engine]
additional_plugins = test-additional-plugin

[test-plugin]
log_level = debug
log_dir = %LOG_ROOT%/path/to/log_files
with_var = ${TESTVAR}_framework
"""

engine_manifest = """{
    "name": "test-engine",
    "version": "0.0.1",
    "requires": {"core": "0.0.1", "test-plugin": "0.0.1"}
}"""

engine_code = """
import glint
class TestEngine(glint.EngineBase):
    initialized = False

def initializer(engine):
    engine.initialized = True
"""

plugin_manifest = """{
    "name": "test-plugin",
    "display_name": "Test Plugin",
    "version": "0.0.1",
    "description": "A test plugin",
    "requires": {"core": "0.0.1", "test-depend-plugin-1": "0.0.1",
                 "test-depend-plugin-2": "0.0.1"},
    "settings": {"with_var": {"default_value": null},
                 "log_dir": {"type": "path"},
                 "default_rpc_server": {"default_value": "localhost"},
                 "alt_server": {"default_value": "127.0.0.1"}},
    "hooks": ["test_hook"]
}"""

plugin_code = """
import glint
class TestPlugin(glint.PluginBase):
    initialized = False
    hook_executed = False

    def get_logger_test(self):
        # Call get_logger within the component main module. It should
        # be the same as calling self.logger.
        return self.get_logger()

def initializer(plugin):
    plugin.initialized = True

    # Add vendor folders cause a lib function uses a vendor package
    plugin.register_component_paths()

    # Register command used to check rpc support for non-builtin datatypes
    glint.register_command('data_test', plugin.lib.data_test, force=True)
"""

plugin_logger_test_code = """
# This is used to check correct logging namespace for component
# python modules.
def get_logger_test(main):
    return main.get_logger()
"""

plugin_lib_code = """
import simplejson as json
import glint

#################################################
# Custom data type in used in the rpc test
class CustomDataClass:
    data = []

    def __init__(self, data):
        self.data = data

    def __eq__(self, obj):
        return self.data == obj.data

    def serialize(self):
        return json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(data):
        return CustomDataClass(json.loads(data))

glint.api.remote.register_rpc_object(
    'CustomDataClass',
    CustomDataClass,
    serializer=lambda x: x.serialize(),
    deserializer=lambda x: CustomDataClass.deserialize(x)
)

#################################################
# This is for checking support for non-builtin and custom data types
# in the rpc setup.
@glint.use_rpc()
def data_test(data):
    return data

#################################################
# This is for checking server exceptions "bubbling up" to client code.
@glint.use_rpc(server='alt_server')
def exception_test(exc_type):
    if exc_type == 'glint':
        raise glint.errors.GlintError("Raising GlintError in server")
    elif exc_type == 'key_error':
        raise KeyError("Raising KeyError in server")

#################################################
# This is for checking plugin reloading
changed = False

#################################################
# This is used to check correct logging namespace for component's
# lib module.
def get_logger_test(main):
    return main.get_logger()

#################################################
# Expose function and module via lib namespace
from test_plugin import call_vendor_func, module
"""

plugin_lib_code2 = """
import vendor_pkg
def call_vendor_func():
    return vendor_pkg.vendor_func()
"""

plugin_vendor_code = """
def vendor_func():
    return 'vendor_ok'
"""

depend_plugin_1_manifest = """{
    "name": "test-depend-plugin-1", "version": "0.0.1", "requires": {"core": "0.0.1"}
}"""

depend_plugin_2_manifest = """{
    "name": "test-depend-plugin-2", "version": "0.0.1", "requires": {"core": "0.0.1"}
}"""

additional_plugin_manifest = """{
    "name": "test-additional-plugin", "version": "0.0.1", "requires": {"core": "0.0.1"}
}"""

tool_plugin_manifest = """{
    "name": "tool-plugin",
    "version": "0.0.1",
    "requires": {"core": "0.0.1"}
}"""

tool_1_manifest = """{
    "name": "test-tool-1",
    "version": "0.0.1",
    "requires": {"core": "0.0.1", "tool-plugin": "0.0.1"},
    "supports": {"test-engine": "0.0.1"}
}"""

tool_2_manifest = """{
    "name": "test-tool-2",
    "version": "0.0.1",
    "requires": {"core": "0.0.1"},
    "supports": {"test-engine": "0.0.1"}
}"""

tool_3_manifest = """{
    "name": "test-tool-3",
    "version": "0.0.1",
    "requires": {"core": "0.0.1"}
}"""

tool_1_code = """
import glint
class TestTool1(glint.ToolBase):
    initialized = False

def initializer(tool):
    tool.initialized = True
"""


def create_test_files(glint_root):
    #### CUSTOM HOOK ####
    (glint_root/'hooks').mkdir(parents=True)
    (glint_root/'hooks/test_hook.py').write_text('def main(): return "custom_hook_ok"')

    #### ENGINE ####
    root = glint_root/'engines/test-engine'
    (root/'python').mkdir(parents=True)
    (root/'python/vendor_common_lib').mkdir(parents=True)
    (root/'python'/vendor_dirname/'bin').mkdir(parents=True)
    (root/'python'/vendor_dirname/'lib/vendor_pkg').mkdir(parents=True)

    (root/'main.py').write_text(engine_code)
    (root/'.meta').write_text(engine_manifest)
    (root/'python/__init__.py').write_text('')

    #### PLUGINS ####
    root = glint_root/'plugins/test-plugin'
    (root/'hooks').mkdir(parents=True)
    (root/'startup').mkdir(parents=True)
    (root/'python/test_plugin').mkdir(parents=True)
    (root/'python/vendor_common_lib').mkdir(parents=True)
    (root/'python'/vendor_dirname/'bin').mkdir(parents=True)
    (root/'python'/vendor_dirname/'lib/vendor_pkg').mkdir(parents=True)

    (root/'main.py').write_text(plugin_code)
    (root/'.meta').write_text(plugin_manifest)
    (root/'python/__init__.py').write_text(plugin_lib_code)
    (root/'python/test_plugin/__init__.py').write_text(plugin_lib_code2)
    (root/'python/test_plugin/module.py').write_text(plugin_lib_code2)
    (root/'python'/vendor_dirname/'lib/vendor_pkg/__init__.py').write_text(plugin_vendor_code)
    (root/'startup/startup.py').write_text(plugin_logger_test_code)
    (root/'hooks/test_hook.py').write_text('def main(): return "default_hook_ok"')

    root = glint_root/'plugins'
    (root/'test-depend-plugin-1').mkdir(parents=True)
    (root/'test-depend-plugin-1/main.py').write_text(
        'import glint\nclass TestDependPlugin1(glint.PluginBase):pass'
    )
    (root/'test-depend-plugin-1/.meta').write_text(depend_plugin_1_manifest)

    (root/'test-depend-plugin-2').mkdir(parents=True)
    (root/'test-depend-plugin-2/main.py').write_text(
        'import glint\nclass TestDependPlugin2(glint.PluginBase):pass'
    )
    (root/'test-depend-plugin-2/.meta').write_text(depend_plugin_2_manifest)

    (root/'test-additional-plugin').mkdir(parents=True)
    (root/'test-additional-plugin/main.py').write_text(
        'import glint\nclass Main(glint.PluginBase):pass'
    )
    (root/'test-additional-plugin/.meta').write_text(additional_plugin_manifest)

    (root/'tool-plugin').mkdir(parents=True)
    (root/'tool-plugin/main.py').write_text(
        'import glint\nclass ToolPlugin(glint.PluginBase):pass'
    )
    (root/'tool-plugin/.meta').write_text(tool_plugin_manifest)

    #### TOOLS ####
    root = glint_root/'tools'
    (root/'test-tool-1').mkdir(parents=True)
    (root/'test-tool-1/main.py').write_text(tool_1_code)
    (root/'test-tool-1/.meta').write_text(tool_1_manifest)

    (root/'test-tool-2').mkdir(parents=True)
    (root/'test-tool-2/main.py').write_text(
        'import glint\nclass TestTool2(glint.ToolBase):pass'
    )
    (root/'test-tool-2/.meta').write_text(tool_2_manifest)

    (root/'test-tool-3').mkdir(parents=True)
    (root/'test-tool-3/main.py').write_text(
        'import glint\nclass TestTool3(glint.ToolBase):pass'
    )
    (root/'test-tool-3/.meta').write_text(tool_3_manifest)

    # settings file
    (glint_root/'glint_settings.ini').write_text(glint_settings)
