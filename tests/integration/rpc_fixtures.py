from __future__ import print_function, unicode_literals
import logging
import threading
try:
    from http.server import BaseHTTPRequestHandler, HTTPServer
except ImportError:
    from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
try:
    from unittest.mock import patch
except ImportError:
    from mock import patch

import simplejson as json

import glint


plugin_manifest = """{
    "name": "test-plugin",
    "display_name": "Test Plugin",
    "version": "0.0.1",
    "description": "A test plugin",
    "requires": {"core": "0.0.1"},
    "settings": {"default_rpc_server": {"default_value": "localhost"},
                 "alt_server": {"default_value": "127.0.0.1"}}
}"""

plugin_code = """
import glint
class TestPlugin(glint.PluginBase): pass

def initializer(plugin):
    glint.register_command('data_test', plugin.lib.data_test)
"""

plugin_lib_code = """
import simplejson as json
import glint
class CustomDataClass:
    data = []

    def __init__(self, data):
        self.data = data

    def __eq__(self, obj):
        return self.data == obj.data

    def serialize(self):
        return json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(data):
        return CustomDataClass(json.loads(data))

glint.api.remote.register_rpc_object(
    'CustomDataClass',
    CustomDataClass,
    serializer=lambda x: x.serialize(),
    deserializer=lambda x: CustomDataClass.deserialize(x)
)

@glint.use_rpc()
def data_test(data):
    return data

@glint.use_rpc(server='alt_server')
def exception_test(exc_type):
    if exc_type == 'glint':
        raise glint.errors.GlintError("Raising GlintError in server")
    elif exc_type == 'key_error':
        raise KeyError("Raising KeyError in server")
"""

server_plugin_code = """
import glint
class ServerPlugin(glint.PluginBase): pass

def initializer(plugin):
    glint.register_command('data_test', plugin.lib.data_test)
    glint.register_command('server_only_command', plugin.lib.server_only_command)
"""

server_plugin_lib_code = """
import simplejson as json
import glint

#################################################
# Custom data type in used in the rpc test
class CustomDataClass:
    data = []

    def __init__(self, data):
        self.data = data

    def __eq__(self, obj):
        return self.data == obj.data

    def serialize(self):
        return json.dumps(self.data, separators=(',', ':'))

    @staticmethod
    def deserialize(data):
        return CustomDataClass(json.loads(data))

glint.api.remote.register_rpc_object(
    'CustomDataClass',
    CustomDataClass,
    serializer=lambda x: x.serialize(),
    deserializer=lambda x: CustomDataClass.deserialize(x)
)

#################################################
# This is for checking support for non-builtin and custom data types
# in the rpc setup.
@glint.use_rpc()
def data_test(data):
    return data

#################################################
# This is for checking server exceptions "bubbling up" to client code.
@glint.use_rpc(server='alt_server')
def exception_test(exc_type):
    if exc_type == 'glint':
        raise glint.errors.GlintError("Raising GlintError in server")
    elif exc_type == 'key_error':
        raise KeyError("Raising KeyError in server")

#################################################
# This is for checking ...
def server_only_command():
    return 'command_ok'
"""


class RequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def _stop_server(self):
        # print(time.asctime(), "Server is going down...")
        self.server.shutdown()

    def log_message(self, format, *args):
        # Disable server logging
        return

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])  # get the size of data
        post_data = self.rfile.read(content_length)  # get the data itself
        self._set_headers()

        if post_data == b'shutdown':
            t = threading.Thread(target=self._stop_server)
            t.start()
        else:
            result = glint.process_rpc_request(json.loads(post_data))
            try:
                self.wfile.write(bytes(result, encoding='utf-8'))
            except TypeError:
                # Python 2
                self.wfile.write(result)


def create_server_files(glint_root):
    root = glint_root/'plugins/server-plugin'
    (root/'python').mkdir(parents=True)
    (root/'main.py').write_text(server_plugin_code)
    (root/'.meta').write_text(plugin_manifest)
    (root/'python/__init__.py').write_text(server_plugin_lib_code)


def start_server(server_class=HTTPServer, handler_class=RequestHandler, port=80):
    logging.getLogger('glint').setLevel(logging.CRITICAL)
    # Mock platform.system so it always returns a windows environment.
    patch('platform.system', return_value='Windows').start()

    glint.load_plugin('server-plugin')
    httpd = server_class(('', port), handler_class)

    # print(time.asctime(), "Server starts - localhost:%s" % port)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

    httpd.server_close()
    # print(time.asctime(), "Server stops - localhost:%s" % port)
