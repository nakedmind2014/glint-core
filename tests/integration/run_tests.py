from __future__ import unicode_literals
import os
import sys
import time
import shutil
import logging
import tempfile
import textwrap
import subprocess
from datetime import date, datetime
try:
    from pathlib import Path
except ImportError:
    # Python 2 backport
    from pathlib2 import Path
try:
    from unittest.mock import patch
except ImportError:
    from mock import patch
try:
    subprocess.run  # Python 3.6 and above only
except AttributeError:
    subprocess.run = subprocess.call

import pytest
import requests

import glint
from glint.constants import COMPONENT_TYPE


from fixtures import (
    create_test_files, engine_manifest, plugin_lib_code, vendor_dirname
)
from rpc_fixtures import create_server_files, start_server


test_glint_root = Path(tempfile.gettempdir())/'glint-root'
engine_root = test_glint_root/'engines/test-engine'


@pytest.fixture(scope='session', autouse=True)
def global_test_setup():
    logging.getLogger('glint.core').setLevel(logging.CRITICAL)
    # Mock platform.system so it always returns a windows environment.
    patch('platform.system', return_value='Windows').start()
    yield

    logging.disable(logging.NOTSET)
    patch.stopall()
    try:
        shutil.rmtree(str(test_glint_root))
    except WindowsError:
        pass


class Test1_Component:
    location = test_glint_root/'plugins/test-plugin'

    @pytest.fixture(scope='class', autouse=True)
    def set_up(self, request):
        os.environ['LOG_ROOT'] = '/glint'
        os.environ['TESTVAR'] = 'glint'

        comp_mgr = glint.component.get_component_manager()
        comp = comp_mgr.load_component(self.location, COMPONENT_TYPE.Plugin,
                                       glint.PluginBase)

        # Add vendor folders cause lib function uses a vendor package.
        comp.register_component_paths()

        # Attach these to the test function class since class-scope fixtures
        # cannot use 'self' to make this available to test functions.
        request.cls.comp_mgr = comp_mgr
        request.cls.comp = comp
        yield

        comp_mgr.unload_component('test-plugin')

    def test1_readonly_properties(self):
        assert self.comp.name == 'test-plugin'
        assert self.comp.display_name == 'Test Plugin'
        assert self.comp.version == '0.0.1'
        assert self.comp.description == 'A test plugin'
        assert self.comp.location == self.location
        assert self.comp.type == COMPONENT_TYPE.Plugin
        assert self.comp.python_folder == self.location/'python'
        assert self.comp.logger.name == 'glint.test-plugin.main'

    def test2_vendor_folder_order(self):
        """ Version-specific plugin vendor folder has higher priority over common folder """
        root = test_glint_root/'plugins/test-plugin'
        items = (str(root/'python'/vendor_dirname/'lib'),
                 str(root/'python'/'vendor_common_lib'))
        self.longMessage = False
        msg = "Vendor folder paths were not inserted in expected order in sys.path:\n{0}\n{1}".format(*items)
        assert items in zip(sys.path, sys.path[1:]), msg

    def test3_lib_modules(self):
        """
        * Component's python folder is not added to python path
        * Component modules & functions accessible via component.lib namespace
        """
        assert self.comp.lib.call_vendor_func() == 'vendor_ok'
        assert self.comp.lib.module.call_vendor_func() == 'vendor_ok'
        assert self.location/'python' not in sys.path

    def test4_settings(self):
        """
        * Override global log level (logging.ERROR)
        * Environment variables in setting value is resolved
        """
        assert self.comp.get_setting('log_dir') == Path('/glint/path/to/log_files')
        assert self.comp.get_setting('with_var') == 'glint_framework'
        assert self.comp.logger.getEffectiveLevel() == logging.DEBUG

    def test5_get_logger(self):
        """ Return correct logger for component's python modules """
        assert self.comp.get_logger_test() == self.comp.logger
        assert self.comp.lib.get_logger_test(self.comp).name == 'glint.test-plugin.lib'
        mod = glint.import_file(test_glint_root/'plugins/test-plugin/startup/startup.py')
        assert mod.get_logger_test(self.comp).name == 'glint.test-plugin.startup.startup'

    def test6_registered_hooks(self):
        assert glint.run_hook('test_hook') == 'custom_hook_ok'

    def test7_unregistered_hooks(self):
        with pytest.raises(glint.errors.GlintError):
            glint.run_hook('invalid_hook')

    def test8_get_main_module(self):
        actual_mod = self.comp_mgr.get_main_module(self.comp.name)
        assert actual_mod.__file__ == str(self.location/'main.py')
        actual_mod = self.comp_mgr.get_main_module('invalid_name')
        assert actual_mod is None

    def test9_initializer(self):
        """ Component's python folder is not added to python path """
        assert self.comp.initialized is False
        assert self.location/'python' not in sys.path

        self.comp_mgr.run_initializer(self.comp)
        assert self.comp.initialized is True
        assert self.location/'python' not in sys.path


class Test2_Plugin:
    loaded_plugins = []

    @pytest.fixture(scope='class', autouse=True)
    def set_up(self, request):
        # Make sure we're starting from a clean slate
        msg = "Component loaded from previous test run is not unloaded. Aborting..."
        assert glint.get_main('test-plugin') is None, msg

        def _wrapper(*args):
            # Record the order in which plugins are loaded.
            # NOTE: Tests will fail in python2 if this is made a class method.
            p = load_plugin_orig(*args)
            self.loaded_plugins.append(p.name)
            return p

        # Patch load_plugin function so we can record the order in which the
        # plugin and its dependencies are loaded.
        comp_mgr = glint.component.get_component_manager()
        load_plugin_orig = glint.component.plugin.load_plugin
        patcher1 = patch('glint.component.plugin.load_plugin', side_effect=_wrapper)
        patcher1.start()
        patcher2 = patch('glint.load_plugin', side_effect=_wrapper)
        patcher2.start()

        request.cls.plugin = glint.load_plugin('test-plugin')
        yield

        comp_mgr.unload_component('test-depend-plugin-1')
        comp_mgr.unload_component('test-depend-plugin-2')
        comp_mgr.unload_component('test-plugin')
        patcher1.stop()
        patcher2.stop()

    def test1_component_type(self):
        assert self.plugin.type == COMPONENT_TYPE.Plugin

    def test2_dependencies(self):
        # Load any required plugins in correct order as specified in manifest
        expected = ['test-depend-plugin-1', 'test-depend-plugin-2', 'test-plugin']
        assert self.loaded_plugins == expected

        # Loaded dependencies are not loaded again
        del self.loaded_plugins[:]
        glint.load_plugin('test-plugin')
        assert self.loaded_plugins == ['test-plugin']

    def test3_plugin_initializer(self):
        """ Loading a plugin will automatically run its initializer """
        assert self.plugin.initialized is True

    def test4_reload_plugin(self):
        # Guard to indicate pre-reload state
        assert self.plugin.lib.changed is False

        # Modify plugin code
        code = plugin_lib_code.replace('changed = False', 'changed = True')
        (test_glint_root/'plugins/test-plugin/python/__init__.py').write_text(code)
        plugin = glint.reload_plugin('test-plugin')

        # Assert that the plugin variable state is changed
        assert plugin.lib.changed is True


class Test3_EngineExceptions:
    engine_root = test_glint_root/'engines/failed-engine'

    @pytest.fixture(scope='class', autouse=True)
    def set_up(self):
        # Make sure we're starting from a clean slate
        msg = "Plugins loaded from previous test runs were not unloaded. Aborting..."
        assert glint.get_main('test-depend-plugin-1') is None, msg
        assert glint.get_main('test-depend-plugin-2') is None, msg
        assert glint.get_main('test-plugin') is None, msg

        (self.engine_root/'python').mkdir(parents=True)
        (test_glint_root/'plugins/test-plugin-2').mkdir(parents=True)

    @pytest.fixture(autouse=True)
    def set_initial_content(self):
        (self.engine_root/'.meta').write_text(textwrap.dedent("""{
            "name": "test-engine",
            "version": "0.0.1",
            "requires": {"core": "0.0.1", "test-plugin": "0.0.1",
                         "test-plugin-2": "0.0.1"}
        }"""))
        (self.engine_root/'main.py').write_text(textwrap.dedent("""
        import glint
        class TestEngine(glint.EngineBase): pass
        """))

        p = test_glint_root/'plugins/test-plugin-2'
        (p/'main.py').write_text("import glint\nclass Main(glint.PluginBase): pass")
        (p/'.meta').write_text(textwrap.dedent("""{
        "name": "test-plugin-2", "version": "0.0.1", "requires": {"core": "0.0.1"}
        }"""))

        (test_glint_root/'plugins/test-additional-plugin/main.py').write_text(
            "import glint\nclass Main(glint.PluginBase): pass")
        yield

        comp_mgr = glint.component.get_component_manager()
        unload_list = ('test-engine', 'test-plugin', 'test-plugin-2',
                       'test-additional-plugin', 'test-tool-1', 'test-tool-2')
        for c in unload_list:
            comp_mgr.unload_component(c)

    def test1_load_component_exception(self):
        (self.engine_root/'main.py').write_text('')

        # Exception raised due to missing entrypoint class
        with pytest.raises(Exception):
            glint.load_engine(self.engine_root)
        # No engine loaded
        assert glint.get_current_engine() is None
        # No plugins loaded
        assert glint.get_main('test-plugin') is None
        assert glint.get_main('test-plugin-2') is None
        assert glint.get_main('test-additional-plugin') is None
        # No tools loaded
        assert glint.get_main('test-tool-1') is None
        assert glint.get_main('test-tool-2') is None

    def test2_load_required_plugin_exception(self):
        (test_glint_root/'plugins/test-plugin-2/main.py').write_text(textwrap.dedent("""
        import glint
        class Main(glint.PluginBase): pass
        def initializer(plugin):
            raise Exception("initialize error")
        """))

        # Exception raised when loading one of the required plugins
        with pytest.raises(Exception):
            glint.load_engine(self.engine_root)
        # No engine loaded
        assert glint.get_current_engine() is None
        # No plugins loaded
        assert glint.get_main('test-plugin') is None
        assert glint.get_main('test-plugin-2') is None
        assert glint.get_main('test-additional-plugin') is None
        # No tools loaded
        assert glint.get_main('test-tool-1') is None
        assert glint.get_main('test-tool-2') is None

    def test3_load_additional_plugin_exception(self):
        (test_glint_root/'plugins/test-additional-plugin/main.py').write_text(textwrap.dedent("""
        import glint
        class Main(glint.PluginBase): pass
        def initializer(plugin):
            raise Exception("initialize error")
        """))

        # Engine loading not affected by additional plugin
        # not being loaded.
        engine = glint.load_engine(self.engine_root)
        # Engine loaded
        assert glint.get_current_engine() is engine
        # Plugins loaded except additional plugin
        assert glint.get_main('test-plugin') is not None
        assert glint.get_main('test-plugin-2') is not None
        assert glint.get_main('test-additional-plugin') is None
        # Tools loaded
        assert glint.get_main('test-tool-1') is not None
        assert glint.get_main('test-tool-2') is not None

    def test4_run_initializer_exception(self):
        (self.engine_root/'main.py').write_text(textwrap.dedent("""
        import glint
        class TestEngine(glint.EngineBase):
            initialized = False

        def initializer(engine):
            raise Exception("initialize error")
        """))

        # Exception raised when running initializer
        with pytest.raises(Exception):
            glint.load_engine(self.engine_root)
        # No engine loaded
        assert glint.get_current_engine() is None
        # No plugins loaded
        assert glint.get_main('test-plugin') is None
        assert glint.get_main('test-plugin-2') is None
        assert glint.get_main('test-additional-plugin') is None
        # No tools loaded
        assert glint.get_main('test-tool-1') is None
        assert glint.get_main('test-tool-2') is None


class Test4_Engine:
    @pytest.fixture(scope='class', autouse=True)
    def set_up(self, request):
        comp_mgr = glint.component.get_component_manager()
        request.cls.engine = glint.load_engine(engine_root)
        yield

        comp_mgr.unload_component('test-depend-plugin-1')
        comp_mgr.unload_component('test-depend-plugin-2')
        comp_mgr.unload_component('test-plugin')
        comp_mgr.unload_component('test-additional-plugin')
        unload_list = ('test-engine', 'test-plugin', 'test-depend-plugin-1',
                       'test-depend-plugin-2', 'test-additional-plugin',
                       'test-tool-1', 'test-tool-2')
        for c in unload_list:
            comp_mgr.unload_component(c)

    def test1_component_type(self):
        assert self.engine.type == COMPONENT_TYPE.Engine

    def test2_raise_GlintError_if_loading_more_than_one_engine(self):
        """ It is an error loading more than one engine at the same time """
        with pytest.raises(glint.errors.GlintError):
            glint.load_engine(engine_root)

    def test3_engine_component_paths_are_in_python_search_path(self):
        """ Enable plugins and tools to access any 3rd-party packages
        that the engine uses """
        root = test_glint_root/'plugins/test-plugin'
        assert str(root/'python/vendor_common_lib') in sys.path
        assert str(root/'python'/vendor_dirname/'lib') in sys.path
        assert str(root/'python'/vendor_dirname/'bin') in os.environ['PATH'].split(os.pathsep)

    def test4_get_current_engine(self):
        assert glint.get_current_engine() is self.engine

    def test5_required_plugins_are_loaded_in_the_order_specified_in_manifest(self):
        assert isinstance(glint.get_main('test-depend-plugin-1'), glint.PluginBase)
        assert isinstance(glint.get_main('test-depend-plugin-2'), glint.PluginBase)
        assert isinstance(glint.get_main('test-plugin'), glint.PluginBase)
        assert isinstance(glint.get_main('test-additional-plugin'), glint.PluginBase)

    def test6_tools_supported_by_the_engine_are_loaded(self):
        assert isinstance(glint.get_main('test-tool-1'), glint.ToolBase)
        assert isinstance(glint.get_main('test-tool-2'), glint.ToolBase)
        assert glint.get_main('test-tool-1').type == COMPONENT_TYPE.Tool
        # Plugin dependency of test-tool-1 is also loaded
        assert isinstance(glint.get_main('tool-plugin'), glint.PluginBase)
        # Get list of loaded tools
        actual = [t.name for t in self.engine.get_loaded_tools()]
        assert actual == ['test-tool-1', 'test-tool-2']

    def test7_tools_not_supported_by_the_engine_not_loaded(self):
        assert glint.get_main('test-tool-3') is None

    def test8_run_initializers(self):
        # Loading an engine will automatically run its initializer
        assert self.engine.initialized is True
        # Also run initializers of loaded tools
        assert glint.get_main('test-tool-1').initialized is True


class Test5_RPC_and_Commands:
    @pytest.fixture(scope='class', autouse=True)
    def set_up(self, request):
        # Start test server
        subprocess.Popen(['python', __file__, 'start-server']).pid
        time.sleep(0.5)
        sys.stdout.flush()

        comp_mgr = glint.component.get_component_manager()
        request.cls.plugin = glint.load_plugin('test-plugin')
        yield

        # Shut down test server
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        rpc_url = 'http://localhost'
        requests.post(rpc_url, data='shutdown', headers=headers)
        # Unload plugin
        comp_mgr.unload_component('test-plugin')

    def test1_date_type_support(self):
        data = date(2018, 4, 27)
        assert glint.commands.data_test(data) == data

    def test2_datetime_type_support(self):
        data = datetime(2018, 4, 27)
        assert glint.commands.data_test(data) == data

    def test3_path_type_support(self):
        data = Path('C:/temp')
        assert glint.commands.data_test(data) == data

    def test4_custom_data_type_support(self):
        data = self.plugin.lib.CustomDataClass([1, 2, 3])
        assert glint.commands.data_test(data) == data

    def test5_server_exceptions(self):
        """ Exceptions generated by the remote command in the server
        "bubble up" to client code """
        # Custom exception
        with pytest.raises(glint.errors.GlintError, match="Raising GlintError in server"):
            glint.remote.exception_test('glint')
        # Builtin exception
        with pytest.raises(KeyError, match="Raising KeyError in server"):
            glint.remote.exception_test('key_error')

    def test6_server_only_command(self):
        """ Call command that is defined only in the server """
        assert glint.remote.server_only_command() == 'command_ok'


def prep_test_env():
    os.environ['GLINT_PLUGIN_ROOT'] = str(test_glint_root/'plugins')
    os.environ['GLINT_TOOL_ROOT'] = str(test_glint_root/'tools')
    os.environ['GLINT_SETTINGS_PATH'] = str(test_glint_root/'glint_settings.ini')
    os.environ['GLINT_HOOK_PATH'] = str(test_glint_root/'hooks')

    try:
        shutil.rmtree(str(test_glint_root))
    except WindowsError:
        pass
    create_test_files(test_glint_root)
    create_server_files(test_glint_root)

    # Restart test script so it uses the modified environment. Pass
    # verbose argument to pytest if specified.
    if sys.argv[-1] in ('-v', '--verbose'):
        subprocess.run(['python', __file__, '-v', 'start-test'])
    else:
        subprocess.run(['python', __file__, 'start-test'])


if __name__ == '__main__':
    if sys.argv[-1] == 'start-server':
        start_server()
    elif sys.argv[-1] == 'start-test':
        args = [__file__, '-s', '-rsf']
        if '-v' in sys.argv or '--verbose' in sys.argv:
            args.append('-v')
        pytest.main(args)
    else:
        # Initialize test environment and restart test script
        prep_test_env()
