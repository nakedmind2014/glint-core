###v0.10.0 (2019-07-24)
- Initial implementation of caching
- Updated license
- Fixed a few bugs

###v0.9.0  (2019-03-12)
- Improvements in the engine implemention
  + Added new engine API methods for starting and stopping. These are intended to be overridden by engine subclasses.
  + Added a new engine API method `engine.get_loaded_tools` that returns a list of tools that have been loaded by the engine.
  + Automatically register/unregister the engine component during engine loading
  + Abort engine loading if a required plugin fails to load
  + Add option for engines to load additional plugins specified in engine settings
  + Modified engine loading sequence so that the engine's initializer is called first before loading the tools that run on the engine. This is to make sure that functionality provided by the engine such as glint commands are available to the tools when they are loaded.
- Added a new namespace `glint.errors` where a component can expose their custom exceptions to other components. Added a new framework command `glint.register_exception` for this.
- The `component.type` attribute is now usable, meaning it now indicates the component type. Previously it was always set to None, making it useless.
- Upgraded vendored packages and added python 3.6 linux packages.
- Removed:
  + `ComponentManager.register_engine` API method
  + `glint.modules` namespace
- Various code fixes and cleanups.

###v0.8.0  (2018-07-10)
- Added support for python 3,specifically 3.6.
- Removed support for managing site data.
- Changed settings implementation so that, instead of one setting file for each component located in the component folder, only one settings file is used for all components. 
- Re-worked again the hook implementation. Removed the module hooks introduced in previous version and switched to file-based hooks similar to git hooks.
- Moved to pytest as the testing framework, improved unittests and added integration tests.
- Changes to glint API: 
  + added: *glint.get_plugin_root*, *glint.get_tool_root*, *glint.load_all_hooks*
  + removed: *glint.Hook*
  + renamed: *glint.execute_hook* to *glint.run_hook*
- Added new environment variables *GLINT_PLUGIN_ROOT*, *GLINT_TOOL_ROOT* and *GLINT_SETTINGS_PATH* for use in overriding default locations for plugins, tools and the settings file.
- Upgraded vendor packages to latest versions and replaced existing path and enum packages with corresponding packages included with python 3 stdlib and backported to python 2.
- Various code fixes and cleanups.

###v0.7.0  (2018-04-21)
- Implemented major changes in components functionality:
  + introduced the component manager which will handle the loading and instantiation of all components,
  + new read-only public properties for components such as name, display_name, version, and description,
  + a component (usually tools) can now specify in its manifest the engines it supports
- Modified the rpc implementation so that: 
  + it now supports multiple rpc servers, enabling a command or function to be remotely executed in a server other than the default rpc server, 
  + it now provides improved support for custom data objects, and 
  + any function can now be defined as "remotely executable".
- Replaced the existing basic setup with a new implementation of hooks which are defined in a component's main module. Component initialization is now done via an optional initializer hook defined in the component main module and called by the component manager.
- Added support for managing site-specific data stored in an sqlite database located in the glint data folder by default.
- Added an initial pass of event signaling implementation based on the blinker package.
- Added improvements in logging functionality such as file logging support
- Removed the option to disable the rpc subsystem.
- Removed the *server* component type, leaving only 3 component types, namely: *engine*, *plugin*, and *tool*.
- Added new environment variable *GLINT_HOME* which will point to the location where user-specific glint data such as preferences, temp and log files are stored.
- Added new namespace *glint.packages* for providing access to a component's vendor package that it wants to share access to other components. Deprecated the *glint.modules* namespace.
- Added *glint.get_setting* API command.
- Added support for *floating point* and *list* setting values.
- Added utility function *whitespace_to_camelcase*.
- Added new 3rd-party dependencies: *blinker*, *colour*, *sqlalchemy-utils*
- Fixed bugs related to component loading, logging, and remote command execution.

###v0.6.1  (2016-03-01)
- Changed how the 3rd-party dependencies are set up and tracked.
- Added and removed 3rd-party packages.
- Fixed bugs related to component loading and logging.

###v0.6.0  (2015-09-02)
 - Changed the logging setup to add more control in the verbosity of logging messages. Instead of just being able to specify whether to output debug messages or not, it is now possible to specify different levels of verbosity to log output via the **log_level** setting which replaced **enable_debug_logging** boolean setting.
 - Changed the settings implementation in order to require settings recognized and used by a component to be defined in the component's manifest. A setting definition includes the value's data type, the default value to used, a brief description and a boolean flag that indicates whether the setting is accessible by all components or is just for a component's private use.
 - Added support for a global data folder where components can save their data files and global temp folder where temporary files generated by components can be saved. The path to these folders are controlled by the **global_data_dir** and **global_temp_dir** settings.

###v0.5.0  (2015-08-11)
 - improved: component loading and reloading; plugin and tool reloading more reliable
 - new: keyboard shortcut property for tool launchers

###v0.4.0  (2015-06-22)
 - new: basic implementation of hook system
 - new: 3rd-party library dependency: comtypes v1.1.1

###v0.3.0  (2015-06-11)
 - significant changes to logging setup:
    - improved: glint core and glint components now uses the same parent namespace "glint"
    - new: component logger property for use by component entry points
    - new: component get_logger method that can provide loggers with correct namespaces to component modules
    - deprecated: glint.get_logger function
    - fixed: component-specific logging settings is now working as expected
 - new: API command glint.get_core_root that returns the location of glint's core folder
 - improved: API command glint.get_glint_root - computed value is cached
 - signicant changes in implementation of component framework
    - removed: import access to component entry point
    - improved: glint.get_main now optionally accepts name of component to return
    - new: API commands for reloading components
    - new: API command glint.run for launching "runnable" components like tools
    - new: auto-loading of plugin dependencies specified in the manifest
    - fixed: location property sometimes does not return the full path to the component folder
  - significant changes in the engine implementation
    - improved: tool registration
    - new: get_launchers method which returns a list of registered tool launchers
    - new: abstract methods stop_engine and get_host_application
    - removed: unused code
  - refactored utils module
    - removed unused functions and renamed existing functions

###v0.2.0  (2015-04-30)
  - new: API command glint.get_main that returns a component entry point
  - new: API command glint.get_current_engine that returns the currently-loaded engine
  ...

###v0.1.0  (2015-04-21)
  ...