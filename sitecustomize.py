"""
if environment variable GLINT_ROOT is defined, use it.
Otherwise, use the following default paths:
  windows:  C:\glint
  linux:    /opt/glint
  linux:    /usr/local/glint
"""
import os
import site
import platform

os_name = platform.system()
glint_root = os.environ.get("GLINT_ROOT")
if glint_root is None:
    default_paths = {
        "Windows": ["C:/glint"],
        "Linux": ["/opt/glint", "/usr/local/glint"]
    }

    for fp in default_paths[os_name]:
        if os.path.exists(fp):
            glint_root = fp
            break

if glint_root:
    # suffix to indicate operating system
    suffix = {"Windows": "_win64", "Linux": "_linux"}[os_name]

    # python version-specific infix which uses only the first 2 components of
    # the python version. e.g. 2.6 for 2.6.6 or 2.7 for 2.7.8
    infix = "_py" + "".join( platform.python_version_tuple()[:2] )

    # version-specific and os-specific
    vendor_dirname = "vendor" + infix + suffix

    # update system path
    bin_dir = os.path.join(glint_root, "core", "python", vendor_dirname, "bin")
    os.environ["PATH"] = bin_dir + os.pathsep + os.environ["PATH"]

    # Update python path. Version-specific modules have higher priority.
    # https://docs.python.org/2/library/site.html#site.addsitedir
    site.addsitedir(glint_root+"/core/python")
    site.addsitedir(glint_root+"/core/python/"+vendor_dirname+"/lib")
    site.addsitedir(glint_root+"/core/python/vendor_common_lib")
